# -*- coding: utf-8 -*-
#
#  IPTV InputBox window
#
#  $Id$
#
#
from Plugins.Extensions.IPTVPlayer.components.cover import Cover2
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printExc
from Screens.InputBox import InputBox


class IPTVInputBoxWidget(InputBox):

    def __init__(self, session, icon={}, size=None, title="", windowTitle=_("Input"), useableChars=None, **kwargs):
        self.session = session
        InputBox.__init__(self, session, title, windowTitle, useableChars, **kwargs)
        width = 300
        height = 260
        if None != size:
            width = size[0]
            height = size[1]
        if 'size' not in icon:
            icon['size'] = [width - 10, height - 70]
        skin = f"""
            <screen name="IPTVInputBoxWidget" position="center,center" title="Input" size="{width},{height}">
            <widget name="text" position="center,10" size="{width - 20},30" font="Regular;24" valign="center" halign="center" />
            <widget name="input" position="center,60" size="{width - 20},50" font="Regular;40" valign="center" halign="center" />
            <widget name="cover" zPosition="4" position="center,{85 + (height - 85 - icon["size"][1]) / 2}" size="{icon["size"][0]},{icon["size"][1]}" transparent="1" alphatest="on" />
            </screen>"""
        self.skin = skin
        self.icon = icon
        self["cover"] = Cover2()
        self.onShown.append(self.setIcon)
    #end def __init__(self, session):

    def setIcon(self):
        if 0 < len(self.icon.get('icon_path', '')):
            try:
                self["cover"].updateIcon(self.icon['icon_path'])
            except Exception:
                printExc()
#class IPTVInputBoxWidget
