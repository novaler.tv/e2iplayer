# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Aflaam'


class Aflaam(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'aflaam', 'cookie': 'aflaam.cookie'})

        self.MAIN_URL = 'https://aflaam.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/GHLpW13/aflaam.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Aflaam.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, },
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=2')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"Aflaam.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=2')},
                {'category': 'list_items', 'title': _('أفــلام عـــربـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=1')},
                {'category': 'list_items', 'title': _('أفــلام هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=3')},
                {'category': 'list_items', 'title': _('أفــلام كــرتـون'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('movies?section=2&category=30')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Aflaam.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(self.ph.decodeHtml(nextPage), f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'widget-body row flex-wrap'), ('<footer', '>', 'main-footer'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'col-lg-auto'), ('</div>', '</a>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''quality['"].+?([^>]+?)[$<]'''), ('rating', '''rating['"].+?([^>]+?)[$<]''')])

            if icon == '':
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            if title == '':
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>', 'entry-title'), ('</h3', '>'), False)[1])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Aflaam.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movie-tab-3'), ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': f"[{E2ColoR('yellow')}Trailer{E2ColoR('white')}]", 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<p', '>', 'text-justify'), ('</p', '>'), False)[1])

        if '/series/' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movie-tab-1'), ('<div', '>', 'tab-content pt-5'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<a', ('</a', '>'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                url = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Aflaam.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/search?q={urllib_quote(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Aflaam.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movie-tab-1'), ('link-show', '>'), True)[1]
        tmpUrl = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''<a href=['"]([^"^']+?)['"]''')[0])

        sts, sHtmlContent = self.getPage(tmpUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('<video', '>', 'player'), ('</video', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<source', '/>')
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''size=['"]([^"^']+?)['"]''')[0])

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': url, 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Aflaam.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Aflaam.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movie-container'), ('<div', '>', 'movie-tabs'), True)[1]

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<span', '>', 'font-size-24'), ('</span', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('اللغة', ':'), ('</div', '>'), False)[1]):
            otherInfo['language'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الترجمة', ':'), ('</div', '>'), False)[1]):
            otherInfo['translation'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('جودة', ':'), ('</div', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('انتاج', ':'), ('</div', '>'), False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('سنة', ':'), ('</div', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('مدة', ':'), ('</div', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('movie-category', '>'), ('</div', '>'), False)[1]):
            otherInfo['genre'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'serie':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Aflaam(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
