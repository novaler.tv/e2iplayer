# -*- coding: utf-8 -*-
# typical import for a standard host

from base64 import b64decode
from re import compile

from Components.config import ConfigYesNo, config, getConfigListEntry
from Plugins.Extensions.IPTVPlayer.compat import urljoin, urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_str

config.plugins.iptvplayer.animeni_getDesc = ConfigYesNo(default=True)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry("Preload movie/series info:", config.plugins.iptvplayer.animeni_getDesc))
    return optionList


def gettytul():
    return 'AnimeNi.pl'  # Nazwa serwisu


class animeni(CBaseHostClass):

    def __init__(self):

        CBaseHostClass.__init__(self, {'history': 'animeni', 'cookie': 'animeni.cookie'})

        self.MAIN_URL = 'https://animeni.pl/'
        self.DEFAULT_ICON_URL = self.getFullUrl("/wp-content/uploads/2023/09/logo-strona-dark.png")

        self.FILTERLIST_URL = self.getFullUrl("/anime/")
        self.POPUPINFO_URL = self.getFullUrl("/wp-admin/admin-ajax.php")

        self.HTTP_HEADER = self.cm.getDefaultHeader('chrome')
        self.defaultParams = {'header': self.HTTP_HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.cacheLinks = {}
        self.filterList = {}

        self.DATA_PARSER_1 = 1
        self.DATA_PARSER_2 = 2
        self.PAGINATION_PARSER_1 = 4
        self.PAGINATION_PARSER_2 = 8

    def getPage(self, url, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)
        return self.cm.getPage(url, addParams, post_data)

    def getLinksForVideo(self, cItem):
        printDBG("animeni.getLinksForVideo [%s]" % cItem)

        videoUrl = cItem['url']
        title = cItem['title']
        sType = cItem['adata']
        linksTab = []
        cacheTab = self.cacheLinks.get(videoUrl, [])
        self.cacheLinks = {}

        if self.up.checkHostSupport(videoUrl) == 1:
            return self.up.getVideoLinkExt(videoUrl)
        else:
            # znajdujemy linki do stron z odtwarzaczami
            sts, data = self.getPage(videoUrl)
            if not sts:
                return
            if sType == -1:
                info_section = self.cm.ph.getDataBeetwenMarkers(data, '<article', '</article>')[1]
                link = self.cm.ph.getSearchGroups(info_section, '<a href="(.*?)">\W?.*epl')[0]

                sts, data = self.getPage(link)
                if not sts:
                    return

            # znajdź linki
            links = compile(r'<option value="((?!").*\W?.*?)/option>').findall(data)

            for link in links:
                edata = self.cm.ph.getSearchGroups(link, '^(.*?)".d')[0]
                ddata = ensure_str(b64decode(edata))
                vurl = self.cm.ph.getSearchGroups(ddata, 'src="(.*?)"')[0]
                service = self.cm.ph.getSearchGroups(link, '\W?\t*(.*?)<')[0]
                linksTab.append({'name': '[%s] %s' % (service.strip(), title), 'url': strwithmeta(vurl, {'Referer': cItem['url']}), 'need_resolve': 1})

        if len(linksTab):
            self.cacheLinks[videoUrl] = linksTab

        return linksTab

    def getVideoLinks(self, baseUrl):
        printDBG("animeni.getVideoLinks [%s]" % baseUrl)
        baseUrl = strwithmeta(baseUrl)

        # mark requested link as used one
        if len(self.cacheLinks.keys()):
            for key in self.cacheLinks:
                for idx in range(len(self.cacheLinks[key])):
                    if baseUrl in self.cacheLinks[key][idx]['url']:
                        if not self.cacheLinks[key][idx]['name'].startswith('*'):
                            self.cacheLinks[key][idx]['name'] = '*' + self.cacheLinks[key][idx]['name'] + '*'
                        break

        return self.up.getVideoLinkExt(baseUrl)

    def listMainMenu(self, cItem):
        # items of main menu
        printDBG('animeni.listMainMenu')
        MAIN_CAT_TAB = [{'category': 'list_items', 'title': _('Latest movies'), 'url': self.getFullUrl('/anime/?status=&type=Movie&order=update'), 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_1},
                        {'category': 'list_items', 'title': _('Newest Episodes'), 'url': self.getFullUrl('/anime/?status=&type=TV&order=update'), 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_1},
                        {'category': 'list_items', 'title': _('Latest update'), 'url': self.getFullUrl('/anime/?status=&type=&order=update'), 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_1},
                        {'category': 'list_az', 'title': _('A-Z List'), 'url': self.getFullUrl('/a-z-list/?show='), 'adata': self.DATA_PARSER_2 + self.PAGINATION_PARSER_2},
                        {'category': 'list_cats', 'title': _('Movies genres'), 'url': self.FILTERLIST_URL, 'adata': 0},
                        {'category': 'search', 'title': _('Search'), 'search_item': True, 'adata': self.DATA_PARSER_2 + self.PAGINATION_PARSER_2},
                        {'category': 'search_history', 'title': _('Search history'), 'adata': self.DATA_PARSER_2 + self.PAGINATION_PARSER_2}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("animeni.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))

        url = self.getFullUrl('/?s=%s') % urllib_quote_plus(searchPattern)
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url, 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_2}
        self.listItems(params)

    def listSeries(self, cItem):
        printDBG('animeni.listSeries')

        # load url
        url = cItem['url']
        sts, data = self.getPage(url)
        if not sts:
            return
        info_section = self.cm.ph.getDataBeetwenMarkers(data, '<article', '</article>')[1]
        icon = self.cm.ph.getSearchGroups(info_section, '<img.*src="(.*?)\?')[0]
        rating = self.cm.ph.getSearchGroups(info_section, 'Ocena (.*?)</strong>')[0]
        title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(info_section, '<h1 class="entry-title".*>(.*?)</h1>')[0])
        status = self.cm.ph.getSearchGroups(info_section, 'Status:.*> (.*?)</span>.*Studio')[0]
        studio = self.cleanHtmlStr(self.cm.ph.getSearchGroups(info_section, 'Studio:.*> (.*?)</span>.*Rok')[0])
        year = self.cm.ph.getSearchGroups(info_section, 'Wydania:.*> (.*?)</span>.*Czas')[0]
        duration = self.cm.ph.getSearchGroups(info_section, 'Trwania:.*> (.*?)</span>.*Sezon')[0]
        added = self.cm.ph.getSearchGroups(info_section, 'Dodano:.*>(.*?)</time>')[0]
        genres = self.cleanHtmlStr(self.cm.ph.getSearchGroups(info_section, '<div class="genxed">(.*?)</a></div>')[0]).strip().replace(' ', ',')
        desc = self.cleanHtmlStr(self.cleanHtmlStr(self.cm.ph.getSearchGroups(info_section, 'itemprop="description">([\s\S]*?)</div>')[0]))
        items = compile('<li.*\W?.*href="(.*?)">\W.*num">(\d+?)</div>\W.*title">(.*?)</div>\W.*date">(.*?)</div>').findall(info_section)

        for item in items:
            epurl = item[0]
            epnum = item[1]
            eptitle = self.cleanHtmlStr(item[2])
            epdate = item[3]
            epdesc = '%s [%s] | STATUS: %s | STUDIO: %s | GATUNKI: %s\n%s %s | DODANY: %s\n%s' % (title, rating, status, studio, genres, eptitle.replace(title, '').strip(), duration, epdate, desc)
            params = {'good_for_fav': True, 'category': 'list_series', 'url': epurl, 'title': '[%s] %s' % (epnum, eptitle), 'desc': epdesc, 'icon': icon, 'adata': 0}
            self.addVideo(params)

    def listItems(self, cItem):
        printDBG('animeni.listItems')

        # load url
        url = cItem['url']
        try:
            sType = cItem['adata']
        except:
            sType = 0
        pattern_d = '<a href="(.*?)".*title="(.*?)".*rel="(.*?)">(?:\W.*){2}typez.*">(.*?)</div>(?:\W.*){5}<img.*src="(.*?)[\?|"]'
        pattern_np = '<div class="hpage">[\S\s]*?href="(.*?)".*Nast'
        if sType & self.DATA_PARSER_2 > 0:
            pattern_d = '<article.*\s.*\s.*href="(.*?)".*rel="(.*?)".*\s.*\s.*typez.*">(.*?)</d.*\s?.*<i.*\s.*/i[\s\S]+?<img.*src="(.*?)[\?|"].*title="(.*?)"'
        if sType & self.PAGINATION_PARSER_2 > 0:
            pattern_np = '<div class="pagination">[\S\s]*?href="(.*?)".*Nast'

        sts, data = self.getPage(url)
        if not sts:
            return
        items = compile('<article([\s\S]*?)(?:</article){1}').findall(data)

        for item in items:
            p = compile(pattern_d).findall(item)[0]
            vurl = p[0]
            vid = p[2]
            vtype = p[3]
            vicon = p[4]
            vtitle = self.cleanHtmlStr(p[1]) + ' ['+vtype+']'
            if config.plugins.iptvplayer.animeni_getDesc.value:
                # get popup for desc
                temp = self.loadPopupData(url, vid)
                desc = self.cleanHtmlStr(self.cm.ph.getSearchGroups(temp, '<div class="contexcerpt">(.*?)</div>')[0])
                score = self.cm.ph.getSearchGroups(temp, '<span class="l">.*/i> (.*?)</span>')[0]
                length = self.cm.ph.getSearchGroups(temp, '<span class="l">(?!<i.*/i>)(.*?)</span>')[0]
                status = self.cm.ph.getSearchGroups(temp, '<span><b>Status:</b> (.*?)</span>')[0]
                genres = self.cleanHtmlStr(self.cm.ph.getSearchGroups(temp, '<span><b>Gatunki:</b> (.*?)</span>')[0])
                studio = self.cleanHtmlStr(self.cm.ph.getSearchGroups(temp, '<span><b>Studio:</b> (.*?)</span>')[0])
                vdesc = '%s[%s]  %s| STATUS: %s | STUDIO: %s | GATUNKI: %s\n%s' % (vtitle, score, length, status, studio, genres, desc)
            else:
                vdesc = vtitle
            sT = sType
            if vtype == "Film":
                sT = -1
            params = {'good_for_fav': True, 'category': 'list_series', 'url': vurl, 'title': vtitle, 'desc': vdesc, 'icon': vicon, 'adata': sT}
            if vtype == "Film":
                self.addVideo(params)
            else:
                self.addDir(params)

        temp = self.cm.ph.getSearchGroups(data, pattern_np)[0]
        if temp != "":
            vurl = urljoin(url, temp)
            params = {'good_for_fav': False, 'category': 'list_items', 'url': vurl, 'title': _('Next Page'), 'desc': _('Next Page'), 'adata': sType}
            self.addDir(params)

    def listAZ(self, cItem):
        printDBG('animeni.listAZ')
        # load url
        url = cItem['url']

        sts, data = self.getPage(url)
        if not sts:
            return

        temp = self.cm.ph.getDataBeetwenMarkers(data, '<div class="lista">', '<div id="sidebar">', False)[1]
        items = compile('<a href="(.*?)">(.*?)</a>').findall(temp)
        for item in items:
            curl = item[0]
            cname = item[1]
            params = {'good_for_fav': False, 'category': 'list_items', 'url': curl, 'title': cname, 'desc': _('Browse'), 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_2}
            self.addDir(params)

    def loadPopupData(self, baseUrl, vId):
        printDBG('animeni.loadPopupData VID:%s' % vId)

        post_data = {'id': vId, 'action': 'dynamic_view_ajax'}

        httpParams = dict(self.defaultParams)
        httpParams['header'] = dict(httpParams['header'])
        httpParams['header']['Referer'] = baseUrl
        httpParams['content-type'] = 'text/html; charset=UTF-8'
        sts, data = self.getPage(self.POPUPINFO_URL, httpParams, post_data)
        if not sts:
            return ''
        return data

    def getList(self, data, key, default=[]):
        try:
            if isinstance(data[key], list):
                return data[key]
        except Exception:
            printExc()
        return default

    def getFilterList(self, cItem):
        printDBG('animeni.getFilterList')
        self.filterList = {}

        sts, data = self.getPage(self.FILTERLIST_URL)
        if not sts:
            return
        temp = self.cm.ph.getDataBeetwenMarkers(data, '<div class="advancedsearch">', '<div class="other-opts">', False)[1]
        items = self.cm.ph.getAllItemsBeetwenMarkers(temp, '<button', '/ul>')
        for item in items:
            filter_name = self.cm.ph.getSearchGroups(item, '<button.*dropdown"> (.*?) <span id.*/button')[0]
            self.filterList[filter_name] = {}
            tab = []
            elements = self.cm.ph.getAllItemsBeetwenMarkers(item, '<li', '/li>')
            for element in elements:
                filter = compile('<li>.*type="(.*?)".*name="(.*?)" value="(.*?)?".*>.*">(.*?)</label').findall(element)[0]
                tab.append({'group': filter[1], 'name': filter[2], 'label': self.cleanHtmlStr(filter[3]), 'ltype': filter[0]})
            self.filterList[filter_name] = tab
        self.listFilters(cItem)

    def listFilters(self, cItem):
        printDBG('animeni.listFilters')
        adata = cItem['adata']

        if adata > 1:
            prev_cat_name = list(self.filterList.keys())[adata-1]
            tlist = self.getList(self.filterList, prev_cat_name)
            for element in tlist:
                if element['label'] in ['Wszystkie', 'Domyślne']:
                    continue
                idx = self.filterList[prev_cat_name].index({'group': element['group'], 'name': element['name'], 'label': element['label'], 'ltype': element['ltype']})
                if element['label'].startswith('* '):
                    element['label'] = element['label'][2:]
                    self.filterList[prev_cat_name][idx] = {'group': element['group'], 'name': element['name'], 'label': element['label'], 'ltype': element['ltype']}
                if cItem['title'] == element['label']:
                    element['label'] = '* ' + element['label']
                    self.filterList[prev_cat_name][idx] = {'group': element['group'], 'name': element['name'], 'label': element['label'], 'ltype': element['ltype']}

        # show current category
        cat_name = list(self.filterList.keys())[adata]
        tlist = self.getList(self.filterList, cat_name)
        for element in tlist:
            ltype = element['ltype']
            if element['label'] in ['Wszystkie', 'Domyślne']:
                continue
            if cItem['title'] == element['label']:
                idx = self.filterList[cat_name].index({'group': element['group'], 'name': element['name'], 'label': element['label'], 'ltype': ltype})
                if element['label'].startswith('* '):
                    element['label'] = element['label'][2:]
                else:
                    element['label'] = '* ' + element['label']
                self.filterList[cat_name][idx] = {'group': element['group'], 'name': element['name'], 'label': element['label'], 'ltype': ltype}
            if adata + 1 == len(self.filterList):
                # last category
                ndata = self.DATA_PARSER_1 + self.PAGINATION_PARSER_1
                url = self.genUrlWithFilters()
                category = 'list_items'
            else:
                url = ''
                category = 'select_cats'
                ndata = adata
                if adata > 0:
                    ndata += 1
            params = {'good_for_fav': False, 'category': category, 'url': url, 'title': element['label'], 'desc': 'KATEGORIA: ' + cat_name + ' | ' + element['label'], 'adata': ndata}
            self.addDir(params)
        if adata + 1 < len(self.filterList):
            self.addDir({'good_for_fav': False, 'category': 'select_cats', 'url': '', 'title': 'Następna kategoria', 'desc': 'Przejdź do nastepnej kategorii', 'adata': adata+1})
        url = self.genUrlWithFilters()
        self.addDir({'good_for_fav': False, 'category': 'list_items', 'url': url, 'title': 'Wyszukaj', 'desc': 'Zakończy wybór i rozpocznij wyszukiwanie', 'adata': self.DATA_PARSER_1 + self.PAGINATION_PARSER_1})

    def genUrlWithFilters(self):
        printDBG('animeni.genUrlWithFilters')
        output = []
        for cat_name in self.filterList:
            tlist = self.getList(self.filterList, cat_name)
            for element in tlist:
                group = element['group']
                if element['label'].startswith('* '):
                    output.append(group + '=' + element['name'])
        url = self.FILTERLIST_URL + '?'+'&'.join(output)
        return url

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        # main cycle to move through items.
        printDBG('animeni.handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        url = self.currItem.get("url", '')

        self.currList = []
        self.cacheLinks = {}

        # MAIN MENU
        if name == None:
            self.listMainMenu({'name': 'category'})
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'list_az':
            self.listAZ(self.currItem)
        elif category == 'list_series':
            self.listSeries(self.currItem)
        elif category == 'list_cats':
            self.getFilterList(self.currItem)
        elif category == 'select_cats':
            self.listFilters(self.currItem)
        # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
        # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, animeni(), True, [])
