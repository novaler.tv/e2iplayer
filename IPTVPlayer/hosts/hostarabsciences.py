# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'ArabSciences'


class ArabSciences(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'arabsciences', 'cookie': 'asharq.com.cookie'})

        self.MAIN_URL = 'https://arabsciences.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/cDdx7JD/arabsciences.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("ArabSciences.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'part1', 'title': _('الصـنفـ الاول'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part2', 'title': _('الصـنـفـ الثـانـي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part3', 'title': _('الصـنفـ الـثالـث'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("ArabSciences.listCatItems")
        category = self.currItem.get("category", '')

        if category == 'part1':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('ثـقـافـات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/cultures/')},
                {'category': 'list_items', 'title': _('الـبلـدان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/countries/')},
                {'category': 'list_items', 'title': _('الإســلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/islam/')},
                {'category': 'list_items', 'title': _('الـطـبيـعة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/nature/')},
                {'category': 'list_items', 'title': _('سيـاســة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/politics/')},
                {'category': 'list_items', 'title': _('هـنـدسـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/engineering/')},
                {'category': 'list_items', 'title': _('كــوارــث'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/catastrophe/')}]
        elif category == 'part2':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('ناشونال جيوغرافيك ابو ظبي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/tv-channels/natgeoad/')},
                {'category': 'list_items', 'title': _('DW (عربية)'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/tv-channels/dw-arabic/')},
                {'category': 'list_items', 'title': _('الجزيرة الوثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/tv-channels/jazeeradoc-tv-channels/')},
                {'category': 'list_items', 'title': _('قناة العربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/tv-channels/alarabyatv/')},
                {'category': 'list_items', 'title': _('BBC Arabic'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/tv-channels/bbc-arabic/')}]
        elif category == 'part3':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('الحيوانات و الحياة البريّة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/animals-categories/')},
                {'category': 'list_items', 'title': _('تاريخ'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/history/')},
                {'category': 'list_items', 'title': _('الفضاء'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/space/')},
                {'category': 'list_items', 'title': _('علوم وتكنولوجيا'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/technology/')},
                {'category': 'list_items', 'title': _('غموض و ألغاز'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/mystery/')},
                {'category': 'list_items', 'title': _('مغامرات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/adventure/')},
                {'category': 'list_items', 'title': _('أسلحة و فنون قتال'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/weapons-fight/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"ArabSciences.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pages-nav'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^'^"]+?/{page + 1}/)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', 'posts-container'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('post-item', '>'), ('<a', 'more-link'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-breeze=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''aria-label=['"]([^"^']+?)['"]''')[0])
            other = self.ph.extract_desc(item, [('year', '''meta-item tie-icon['"].+?([0-9]{4})[$<]'''), ('plot', '''post-excerpt['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=other, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"ArabSciences.exploreItems cItem[{cItem}]")

        params = dict(cItem)
        params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
        self.addVideo(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"ArabSciences.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<p>', '<iframe'), ('</iframe>', '</p>'), True)[1]
        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''src=['"]([^"^']+?)['"]''')[0])

        urlTab.append({'name': '', 'url': url, 'need_resolve': 1})
        return urlTab

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"ArabSciences.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/?s={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getVideoLinks(self, videoUrl):
        printDBG(f"ArabSciences.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"ArabSciences.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', 'style'), ('</span', '</p'), True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('justify', '>'), ('</p', '<p'), False)[1])
        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'part1' or category == 'part2' or category == 'part3':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, ArabSciences(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
