# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'ArabSeed'


class ArabSeed(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'arabseed', 'cookie': 'arabseed.cookie'})

        self.MAIN_URL = 'https://m5.asd.cam/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/WtH2Wp2/arabseed.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("ArabSeed.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies/')},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series/')},
            {'category': 'ramadan', 'title': _('مســلـســلات رمـضـان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-رمضان/')},
            {'category': 'netfilx', 'title': _('Netfilx'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/netfilx/')},
            {'category': 'wwe', 'title': _('المــصـارعــة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/wwe-shows/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"ArabSeed.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if category == 'movei':
            sStart = '>افلام<'
        elif category == 'serie':
            sStart = '>مسلسلات<'
        elif category == 'netfilx':
            sStart = '>Netfilx<'
        elif category == 'ramadan':
            sStart = '>مسلسلات رمضان<'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG(f"ArabSeed.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'Blocks-UL'), ('<script', '>', 'text/javascript'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'MovieBlock'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h4', '>'), ('</h4', '>'), False)[1])
            desc = self.ph.extract_desc(item, [('rating', '''RateNumber['"].+?([^>]+?)[$<]'''), ('quality', '''Ribbon['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title.split('الحلقة')[0], desc=desc)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"ArabSeed.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''descrip['"][>]([^>]+?)[$<]''')[0])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'SeasonsListHolder'), ('</ul', '>'), True)[1]
        if Season:
            self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                sId = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-id=['"]([^"^']+?)['"]''')[0])
                sSeason = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-season=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('fa-folder', '>'), ('</span', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'category': 'listSeason', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': cItem['url'], 'icon': cItem['icon'], 'desc': otherInfo, 'sSeason': sSeason, 'sId': sId})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ContainerEpisodesList'), ('</div', '>'), True)[1]
        if Episod:
            self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, '<a', ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</em', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSeason(self, cItem):
        printDBG(f"ArabSeed.listSeason cItem[{cItem}]")

        tmpUrl = self.getFullUrl('/wp-content/themes/Elshaikh2021/Ajaxat/Single/Episodes.php')
        sts, data = self.getPage(tmpUrl, post_data={'post_id': cItem['sId'], 'season': cItem['sSeason']})
        if not sts:
            return

        tmp = self.cm.ph.getAllItemsBeetwenMarkers(data, '<a', ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</em', '>'), False)[1])

            info = self.ph.std_title(title, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"ArabSeed.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/find/?find={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"ArabSeed.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        sPost = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''data-post=['"]([^"^']+?)['"]''')[0])
        cUrl = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''HomeURL = ['"]([^"^']+?)['"]''')[0])
        self.setMainUrl(cUrl)

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Origin'] = self.up.getDomain(self.cm.meta['url'], False)
        params['header']['Referer'] = cUrl

        tmpUrl = self.getFullUrl('/wp-content/themes/Elshaikh2021/Ajaxat/Single/Server.php')

        for server in ('0', '1', '2', '3', '4', '5'):
            if server == '0':
                sName = 'Arabseed'
            if server == '1':
                sName = 'Server 1'
            if server == '2':
                sName = 'Server 2'
            if server == '3':
                sName = 'Server 3'
            if server == '4':
                sName = 'Server 4'
            if server == '5':
                sName = 'Server 5'

            for sQual in ('1080', '720', '480'):
                post_data = {'post_id': sPost, 'server': server, 'qu': sQual}
                sts, sHtmlContent = self.getPage(tmpUrl, params, post_data)
                if not sts:
                    return

                url = self.getFullUrl(self.cm.ph.getSearchGroups(sHtmlContent, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{sName} - {sQual}P]{E2ColoR('white')} - {E2ColoR('yellow')}{self.up.getHostName(url, True)}{E2ColoR('white')}")

                if url != '':
                    urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"ArabSeed.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"ArabSeed.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'MetaTermsInfo'), ('<div', '>', 'topBarSingle'), True)[1]

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('RatingImdb', 'IMDB'), ('</em', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('مدة العرض', '>'), ('</li', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('Quality', '>'), ('</a', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('release-year', '>'), ('</a', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع', '>'), ('</li', '>'), False)[1]):
            otherInfo['genre'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('language', '>'), ('</a', '>'), False)[1]):
            otherInfo['language'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('nation', '>'), ('</a', '>'), False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1]):
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'netfilx' or category == 'ramadan':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'wwe':
            self.listItems(self.currItem)
        elif category == 'listSeason':
            self.listSeason(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, ArabSeed(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
