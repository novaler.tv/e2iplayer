# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import \
    getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Asharq'


class Asharq(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'asharq.com.cookie'})

        self.MAIN_URL = 'https://now.asharq.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/xsbdc1f/asharq.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Asharq.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title': _('افـلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/doc/')},
            {'category': 'show_movies', 'title': _('أفـلام الاكــتشاف'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/discovery/')},
            {'category': 'show_list', 'title': _('بـودكــاسـت'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/الشرق-بودكاست/')},
            {'category': 'show_list', 'title': _('QuickTake'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/الشرق-quicktake/')},
            {'category': 'show_list', 'title': _('اقـتصـاد'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/اقتصاد-الشرق/')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Asharq.listItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<main', '>'), '</main>', True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'card card-genre'), ('<div', '>', 'swiper-slide is-column'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<p', '>', 'card-description'), ('</p', '>'), False)[1])

            info = self.ph.std_title(title, with_ep=True)
            if title != '':
                title = info['title_display']
            otherInfo = f"{info['desc']}\n{desc}"

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Asharq.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'cards-list'), ('</section', '>'), True)[1]
        if tmp:
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'card-genre-episode'), ('<div', '>', 'card-item'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
                desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('card-shortdescription', '>'), ('</p', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Asharq.getLinksForVideo [{cItem}]")
        urlTabs = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = data.meta['url']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, 'data-ssr', ('</script', '>'), True)[1]
        Url = self.cm.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''['"]High['"].+?['"]([^"^']+?m3u8)['"]''', ignoreCase=True)[0])
        hlsUrl = strwithmeta(Url, {'Origin': self.getMainUrl(), 'Referer': cUrl})

        urlTabs.extend(getDirectM3U8Playlist(hlsUrl, checkExt=False, variantCheck=True, checkContent=True, sortWithMaxBitrate=99999999))
        return urlTabs

    def getVideoLinks(self, videoUrl):
        printDBG(f"Asharq.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Asharq.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('card-content-hover', '>'), ('</span', '</a'), True)[1]
        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('card-description', '>'), ('</p', '</div>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Asharq(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
