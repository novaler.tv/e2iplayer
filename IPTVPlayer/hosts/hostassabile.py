# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Assabile'


class Assabile(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'assabile.cookie'})

        self.MAIN_URL = 'https://ar.assabile.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/M9m0PHK/Assabile.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': False}

    def listMainMenu(self, cItem):
        printDBG("Assabile.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('الـقــرآن'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/quran')},
            {'category': 'show_list', 'title': _('الــدروس'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/lesson')},
            {'category': 'show_list', 'title': _('الأنــاشــيد'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anasheed')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Assabile.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('pagination', '>'), ('</ul>', '</div>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''<a href=['"]([^"^']+?)['"] rel=['"]next['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('portfolio', '>'), ('pagination', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('portfolio-image', '>'), ('style', '</div>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

            info = self.ph.std_title(title, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def listNewCategory(self, cItem):
        printDBG(f"Assabile.listNewCategory cItem[{cItem}]")

        sts, data = self.cm.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('active', '>'),  ('</li>', '</ul>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), '</a></li>')
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a>', '</li>'), False)[1])

            if url.endswith('photos'):
                continue

            info = self.ph.std_title(title.split('(')[0], with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listNewCategory', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Assabile.exploreItems cItem[{cItem}]")
        baseUrl = cItem['url']

        sts, data = self.cm.getPage(baseUrl)
        if not sts:
            return

        if baseUrl.endswith('quran') or 'quran' in baseUrl or 'collection' in baseUrl or 'album' in baseUrl or '/series-audio/' in baseUrl:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', 'ul-play-list', '>'), ('<div', 'modal hide fade'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('link-media', 'never'), ('</a>', '</div>'))

            for item in tmp:
                url = self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0]
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('sorting', '>'), '</span>', False)[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addAudio(params)

        if baseUrl.endswith('collection'):
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('myoptions', '>'), ('text/javascript', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('h2_sourat', '>'), ('</a>', '</h2>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-name=['"]([^"^']+?)['"]''')[0])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

        if baseUrl.endswith('album') or baseUrl.endswith('series'):
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('clearfix portfolio', '>'), ('<div', 'widget clearfix', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('title-ptf-lstpp', '>'), ('</p>', '</div>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a>', '</p>'), False)[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

        if baseUrl.endswith('series-audio'):
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('clearfix portfolio', '>'), ('<div', 'widget clearfix', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('audioStextA', '>'), ('<td', 'audioStextAalt'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a>', '</td>'), False)[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

        if 'episode' in baseUrl:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': ''})
            self.addVideo(params)

        if '/series/' in baseUrl:
            page = cItem.get('page', 1)

            nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('pagination', '>'), ('</ul>', '</div>'), True)[1]
            nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''<a href=['"]([^"^']+?)['"] rel=['"]next['"]''')[0])

            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', 'posts'), ('<div', 'widget clearfix', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('entry_title2', '>'), ('</h2>', '</div>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a>', '</h2>'), False)[1])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

            if nextPage != '':
                params = dict(cItem)
                params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
                self.addDir(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Assabile.getLinksForVideo [{cItem}]")
        urlTab = []
        baseUrl = cItem['url']

        if baseUrl.endswith('.mp3'):
            urlTab.append({'name': cItem['title'], 'url': baseUrl, 'need_resolve': 0})

        if baseUrl.startswith('#'):
            tmpUrl = self.getFullUrl(f"/ajax/getrcita-link-{baseUrl}").replace('#', '')

            sts, data = self.cm.getPage(tmpUrl, self.defaultParams)
            if not sts:
                return

            urlTab.append({'name': cItem['title'], 'url': data, 'need_resolve': 0})
        elif baseUrl.startswith('@'):
            tmpUrl = self.getFullUrl(f'/ajax/getsnng-link-{baseUrl}').replace('@', '')

            sts, data = self.cm.getPage(tmpUrl, self.defaultParams)
            if not sts:
                return

            urlTab.append({'name': cItem['title'], 'url': data, 'need_resolve': 0})

        elif baseUrl.endswith('.htm'):
            sts, data = self.cm.getPage(baseUrl)
            if not sts:
                return

            url = self.cm.ph.getSearchGroups(data, '''file: ['"]([^"^']+?)['"]''')[0]
            if url == '':
                tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<video', 'style'), ('</video>', '<script'), True)[1]
                url = self.cm.ph.getSearchGroups(tmp, '''src=['"]([^"^']+?)['"]''')[0]
            urlTab.append({'name': cItem['title'], 'url': url, 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Assabile.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Assabile.getArticleContent [{cItem}]")

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': ''}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.listNewCategory(self.currItem)
        elif category == 'listNewCategory':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Assabile(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
