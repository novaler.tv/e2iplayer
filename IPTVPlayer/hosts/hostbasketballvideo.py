# -*- coding: utf-8 -*-

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'basketball-video'


class BasketballVideo(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'basketball-video', 'cookie': 'basketball-video.cookie'})

        self.MAIN_URL = 'https://basketball-video.com/'
        self.DEFAULT_ICON_URL = 'https://cdn.nba.com/manage/2023/04/nba-app-pregame.jpg'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG(f"BasketballVideo.listMainMenu cItem [{cItem}]")

        MAIN_CAT_TAB = [{'category': 'sub_menu', 'title': 'Basketball-Video', 'url': self.getMainUrl(), 'desc': self.getMainUrl()},
                        {'category': 'sub_menu', 'title': 'MLBLive', 'url': 'https://mlblive.net/', 'desc': 'https://mlblive.net/'},
                        {'category': 'sub_menu', 'title': 'NFL-Video', 'url': 'https://nfl-video.com/', 'desc': 'https://nfl-video.com/'},
                        {'category': 'sub_menu', 'title': 'Tennis Replays', 'url': 'https://tennisreplays.com/', 'desc': 'https://tennisreplays.com/'},
                        {'category': 'sub_menu', 'title': 'FullRaces', 'url': 'https://fullraces.com/', 'desc': 'https://fullraces.com/'},]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listSubMenu(self, cItem, nextCategory):
        printDBG(f"BasketballVideo.listSubMenu [{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = data.meta['url']
        self.setMainUrl(cUrl)

        tmp = self.cm.ph.getDataBeetwenNodes(data, ('<nav', '>', 'nav block_elem'), ('</nav', '>'), False)[1]
        sub = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'submenu'), '</ul>')
        for item in sub:
            tmp = tmp.replace(item, '')
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        nsub = 0
        for item in tmp:
            if '>DCMA<' in item:
                continue
            if '<ul' in item:
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<li', '>'), ('</a', '>'), False)[1])
                self.addMarker({'title': title, 'desc': ''})
                item = self.cm.ph.getDataBeetwenNodes(item, ('<ul', '>'), ('</li', '>'), False)[1]
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            if url == '':
                continue
            title = self.cleanHtmlStr(item)
            params = dict(cItem)
            params.update({'category': nextCategory, 'title': title, 'url': url})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("BasketballVideo.listItems")

        page = cItem.get('page', 1)
        url = f"{cItem['url']}?page{page}"
        sts, data = self.getPage(url)
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'paging'), '</div>')[1]
        if '' != self.cm.ph.getSearchGroups(nextPage, f'>({page + 1})<')[0]:
            nextPage = True
        else:
            nextPage = False

        data = data.split('<div class="poster">')[1:]
        for item in data:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''\shref=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])
            if not self.cm.isValidUrl(url):
                continue
            icon = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''\ssrc=['"]([^"^']+?)['"]''')[0])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<div', '>', 'short_descr'), ('</div', '>'), False)[1]).replace('&nbsp;', ' ')
            params = dict(cItem)
            params = {'good_for_fav': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc}
            self.addVideo(params)

        if nextPage:
            page += 1
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': cItem['url'], 'page': page})
            self.addDir(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"BasketballVideo.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']

        tmp = self.cm.ph.getAllItemsBeetwenNodes(data, ('<p', '<br', 'strong'), ('</p', '>'))
        if len(tmp):
            for item in tmp:
                url = self.cm.ph.getSearchGroups(item, '''\shref=['"]([^'^"]+?)['"]''')[0]
                if url.startswith('//'):
                    url = f'http:{url}'

                name = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<strong', '>'), ('</strong', '>'), False)[1])
                if self.cm.isValidUrl(url):
                    urlTab.append({'name': name, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})

        tmp = self.cm.ph.getAllItemsBeetwenNodes(data, ('<iframe', '>'), ('</iframe', '>'))
        if len(tmp):
            for item in tmp:
                url = self.cm.ph.getSearchGroups(item, '''\ssrc=['"]([^'^"]+?)['"]''')[0]
                if url.startswith('//'):
                    url = f'http:{url}'

                if self.cm.isValidUrl(url):
                    urlTab.append({'name': self.up.getDomain(url), 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"BasketballVideo.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None:
            self.listMainMenu({'name': 'category'})
        elif category == 'sub_menu':
            self.listSubMenu(self.currItem, 'list_items')
        elif category == 'list_items':
            self.listItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, BasketballVideo(), True, [])
