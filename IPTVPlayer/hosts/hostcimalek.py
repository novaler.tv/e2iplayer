# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

import random

from Plugins.Extensions.IPTVPlayer.compat import (urllib_quote,
                                                  urllib_quote_plus)
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Cimalek'


class Cimalek(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cimalek', 'cookie': 'cimalek.cookie'})

        self.MAIN_URL = 'https://cimalek.art/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/qR8xY8G/cimalek.png'

        self.HEADER = self.cm.getDefaultHeader('safari')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def getRandomString(self, length=16):
        allowedChars = list('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
        return ''.join(random.choice(allowedChars) for _ in range(length))

    def listMainMenu(self, cItem):
        printDBG("Cimalek.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'home', 'title': _('الـرئـيـسية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/b4/')},
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies/')},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series/')},
            {'category': 'season', 'title': _('الـمـواسـم'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/seasons/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"Cimalek.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if category == 'movei':
            sStart = '> افلام'
        elif category == 'serie':
            sStart = '> المسلسلات'
        elif category == 'home':
            sStart = '>الرئيسية'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG(f"Cimalek.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('<footer', '>', 'main'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'tab-content'), ('<footer', '>', 'main'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'item'), ('desc', '</div>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('rating', '''rating['"].+?([^>]+?)[$<]'''), ('quality', '''quality['"].+?([^>]+?)[$<]'''), ('year', '''desc['"].+?([0-9]{4})[$<]''')])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Cimalek.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'film-description'), ('</div', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'seasonse'), ('</div', '>'), True)[1]
        if Season:
            self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<li', '>', 'sealist'), ('</li', '>'))
            for item in tmp:
                url = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'episodios'), ('</ul', '>'), True)[1]
        if Episod:
            self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>', 'episodesList'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Cimalek.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Cimalek.getLinksForVideo [{cItem}]")
        urlTab = []
        baseUrl = cItem['url']
        if not baseUrl.endswith("watch/"):
            baseUrl += 'watch/'

        sts, sHtmlContent = self.getPage(baseUrl)
        if not sts:
            return
        cUrl = self.cm.meta['url']

        sVer = self.cleanHtmlStr(self.cm.ph.getSearchGroups(sHtmlContent, '''ver['"]:['"]([^"^']+?)['"]''')[0])
        sHost = self.cleanHtmlStr(self.cm.ph.getSearchGroups(sHtmlContent, '''site_url['"]:['"]([^"^']+?)['"]''')[0]).replace('\\', '')

        tmp = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('<div', '>', 'ps_-block ajax_mode'), ('<div', '>', 'block_area'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'item server-item'), ('</ul', '>'))
        for item in tmp:
            sType = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-type=['"]([^"^']+?)['"]''')[0])
            sPost = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-post=['"]([^"^']+?)['"]''')[0])
            sNum = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-nume=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<li', '>'), ('</li', '>'), False)[1])

            siteUrl = f'{sHost}/wp-json/lalaplayer/v2/?p={sPost}&t={sType}&n={sNum}&ver={sVer}&rand={self.getRandomString()}'

            params = dict(self.defaultParams)
            params['header'] = dict(self.AJAX_HEADER)
            params['header']['Referer'] = urllib_quote(cUrl)
            params['header']['host'] = sHost.split('//')[1]

            sts, data = self.getPage(siteUrl, params)
            if not sts:
                return

            tmpUrl = self.getFullUrl(self.cm.ph.getSearchGroups(self.ph.decodeHtml(data), '''embed_url['"]:['"]([^"^']+?)['"]''')[0])

            params = dict(self.defaultParams)
            params['header'] = dict(self.AJAX_HEADER)
            params['header']['Referer'] = urllib_quote(cUrl)
            params['header']['Sec-Fetch-Dest'] = 'iframe'

            sts, data = self.getPage(tmpUrl, params)
            if not sts:
                return
            tmp = self.cm.ph.getDataBeetwenMarkers(self.ph.decodeHtml(data), '<iframe', ('</iframe', '>'), True)[1]
            url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''src=['"]([^"^']+?)['"]''')[0])

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            if url != '':
                urlTab.append({'name': title, 'url': strwithmeta(url, {'User-Agent': self.cm.getDefaultUserAgent(browser='mobile'), 'Referer': baseUrl}), 'need_resolve': 1})

        tmp = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('<div', '>', 'block_area-comment downlo'), ('<footer', '>', 'main'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<a', 'ssl-item ep-item'), ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<em', '>'), ('</em', '>'), False)[1])

            if 'megamax.me' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                tmp = cMegamax(sHosterUrl)
                if tmp != False:
                    for item in tmp:
                        url = item.split(',')[0].split('=')[1]
                        sQual = item.split(',')[1].split('=')[1]
                        sLabel = item.split(',')[2].split('=')[1]

                        title_ = (f"{cItem['title']} {E2ColoR('lightred')} [{sQual}]{E2ColoR('white')}{E2ColoR('yellow')} - {sLabel}{E2ColoR('white')}")
                        urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Cimalek.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem, data=None):
        printDBG(f"Cimalek.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'item-list'), ('<div', '>', 'block_area-header'), True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الاسم الاصلي', '>'), ('</span', '>'), False)[1])
        if title == '':
            title = cItem['title']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('المدة', '>'), ('</span', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('تاريخ العرض', '>'), ('</span', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</div', '>'), False)[1]):
            otherInfo['genres'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('البلد', '>'), ('</span', '>'), False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('اللغة', '>'), ('</span', '>'), False)[1]):
            otherInfo['language'] = Info

        return [{'title': title, 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'home':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'season':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Cimalek(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
