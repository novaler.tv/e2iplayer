# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Cinematy'


class Cinematy(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cinematy', 'cookie': 'cinematy.cookie'})

        self.MAIN_URL = 'https://cinematy.store/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/HqbxwN8/cinematy.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Cinematy.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'anime', 'title': _('أنــمــي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"Cinematy.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'movei':
            sStart = '>الأفلام<'
        elif category == 'serie':
            sStart = '>مسلسلات<'
        elif category == 'anime':
            sStart = '>انمي و كرتون<'
        elif category == 'tvshow':
            sStart = '>منوعات<'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

        if category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسلسلات تركية مترجمة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مترجمة/')},
                {'category': 'list_items', 'title': _('مسلسلات تركية مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مدبلجة/')},
                {'category': 'list_items', 'title': _('مسلسلات تركية نتفلكس'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-نتفلكس/')},
                {'category': 'list_items', 'title': _('أحدث المسلسلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series')},
                {'category': 'list_items', 'title': _('مسلسلات رمضان 2024'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-رمضان-2024/')},]
            self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Cinematy.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'page-numbers'), ('</ul', '>',), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(self.ph.decodeHtml(nextPage), f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'sec-line'), ('<div', '>', 'footer'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'block-post'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-img=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<li', '>', 'title'), ('<em', '>'), False)[1])
            if title == '':
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('year', '''year['"].+?([0-9]{4})[$<]''')])

            info = self.ph.std_title(title.split('الاخيرة')[0], desc=desc)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Cinematy.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('data-bpopup', '<iframe'), ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': f"[{E2ColoR('yellow')}Trailer{E2ColoR('white')}]", 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('story', '<p>'), ('</p', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'seasons'), ('<div', '>', 'otherseries'), True)[1]
        if Season:
            self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<article', '>', 'post'), ('</article', '>'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-img=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<li', '>', 'title'), ('<em', '>'), False)[1])
                year = self.ph.extract_desc(item, [('year', '''year['"].+?([0-9]{4})[$<]''')])

                info = self.ph.std_title(title, desc=year, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'episodes'), ('</ul', '>'), True)[1]
        if Episod:
            self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</em', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)

        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Cinematy.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/?s={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Cinematy.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        baseUrl = f"{cItem['url']}watch/"
        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        cUrl = self.cm.meta['url']

        postID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''vo_postID\s?=\s?['"]([^"^']+?)['"]''')[0])
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<h4', '>', 'servTitle'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            tmpID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''this.id.+?([^,]+?)[$,]''')[0])
            tmpServ = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''this.id,.+?,([^,]+?)[$)]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>', 'server'), ('</i', '>'), False)[1])

            tmpUrl = self.getFullUrl(f'wp-content/themes/vo2022/temp/ajax/iframe2.php?id={postID}&video={tmpID}&serverId={tmpServ}')

            params = dict(self.defaultParams)
            params['header']['Referer'] = cUrl

            sts, sHtmlContent = self.getPage(tmpUrl, params)
            if not sts:
                return

            url = self.getFullUrl(self.cm.ph.getSearchGroups(sHtmlContent, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if 'm3lomatik' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                tmp = cMegamax(sHosterUrl)
                if tmp != False:
                    for item in tmp:
                        url = item.split(',')[0].split('=')[1]
                        sQual = item.split(',')[1].split('=')[1]
                        sLabel = item.split(',')[2].split('=')[1]

                        title_ = (f"{cItem['title']} {E2ColoR('lightred')} [{sQual}]{E2ColoR('white')}{E2ColoR('yellow')} - {sLabel}{E2ColoR('white')}")
                        urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Cinematy.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Cinematy.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'singleInfo'), ('<div', '>', 'postLink'), True)[1]

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<i', '>', 'iconFont-star-full'), ('</span', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</ul', '>'), False)[1]):
            otherInfo['genre'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1]):
            otherInfo['category'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('country', '>'), ('</a', '>'), False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('language', '>'), ('</a', '>'), False)[1]):
            otherInfo['language'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('years', '>'), ('</a', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('حالة', '<a>'), ('</a', '>'), False)[1]):
            otherInfo['status'] = Info

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('story', '<p>'), ('</p', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<h1', '>'), ('</h1', '>'), False)[1])
        if title == '':
            title = cItem['title']

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'anime' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Cinematy(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
