# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from base64 import b64decode

from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_str


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Esheeq'


class Esheeq(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'eceeq', 'cookie': 'eceeq.cookie'})

        self.MAIN_URL = 'https://3sktr.tv/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/nBz6Rfz/esseq.png'

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Connection': 'keep-alive', 'Accept-Encoding': 'gzip', 'Content-Type': 'application/x-www-form-urlencoded', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Esheeq.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title': _('أفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/الأفلام-التركية/')},
            {'category': 'show_series', 'title': _('مســلســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/all-series/')},
            {'category': 'show_movies', 'title': _('آخــر الحـلقـات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/episodes/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Esheeq.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmpNext = self.cm.ph.getDataBeetwenMarkers(data, ('pagination', '>'), ('</div', '>'), True)[1]
        nextPage = self.cm.ph.getDataBeetwenMarkers(tmpNext, ('current', '>'), ('inactive', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('container-fluid', '>'), ('footer', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('block-post', '>'), ('</article', '>'))
        for item in tmp:
            icon = self.cm.ph.stdUrl(self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0]))
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.ph.decodeHtml(self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<div', '>', 'title', ), ('</div', '>'), True)[1])).replace('“', '').replace('"', '').replace('”', ' ').strip()

            info = self.ph.std_title(title, with_type=False, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            if '?url=' in url:
                url = ensure_str(b64decode(url.split('?url=')[-1].replace('%3D', '=')))

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Esheeq.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'story'), ('</div', '>'), False)[1])
        if "series" in cItem['url']:

            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('container-fluid', '>'), ('footer', '>'), True)[1]
            msg = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<div', '>', 'noResult alert alert-dismissable'), ('</div', '>'), False)[1])
            if msg != '':
                SetIPTVPlayerLastHostError(_(msg))
            else:
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('block-post', '>'), ('</article', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    if '?url=' in url:
                        url = ensure_str(b64decode(url.split('?url=')[-1].replace('%3D', '=')))

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info['title_display']
                    otherInfo = f"{info['desc']}\n{desc}"

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': self.getFullUrl(url), 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Esheeq.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/search/{urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Esheeq.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'skipAd'), ('</span', '>'), True)[1]
        tmp = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''href=['"]([^"^']+?)['"]''')[0])

        tmpUrl = json_loads(ensure_str(b64decode(tmp.split('?post=')[-1])))

        tmpServ = tmpUrl.get('servers')
        for aEntry in tmpServ:
            tmpID = aEntry["id"]
            tmpName = aEntry["name"].replace('facebook', 'face')
            if 'dailymotion' == tmpName:
                url = f'https://www.dailymotion.com/video/{tmpID}'
            if 'ok' == tmpName:
                url = f'https://ok.ru/videoembed/tmpID{tmpID}'
            if 'face' == tmpName:
                url = f'https://app.videas.fr/embed/media/{tmpID}'
            if 'tune' == tmpName:
                url = f'https://tune.pk/js/open/embed.js?vid={tmpID}&userid=827492&_=1601112672793'
            if 'estream' == tmpName:
                url = f'https://arabveturk.sbs/embed-{tmpID}.html'
            if 'now' == tmpName:
                url = f'https://extreamnow.org/embed-{tmpID}.html'
            if 'box' == tmpName:
                url = f'https://youdbox.site/embed-{tmpID}.html'
            if 'Pro HD' == tmpName:
                url = f'https://segavid.com/embed-{tmpID}.html'
            if 'Red HD' == tmpName:
                url = f'https://embedwish.com/e/{tmpID}'
            if 'online' == tmpName:
                url = f'https://player.vimeo.com/video/{tmpID}?title=0&byline=0'
            if 'youtube' == tmpName:
                url = f'https://www.youtube.com/watch?v={tmpID}'
            if 'plus' == tmpName:
                url = f'https://tuneplus.co/js/open/embed.js?vid={tmpID}&userid=958509&_=1627365111402'
            if 'express' == tmpName:
                url = tmpID

            title = (f"{cItem['title']} {E2ColoR('lightred')} [{tmpName}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Esheeq.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Esheeq.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'story'), ('</div', '>'), False)[1])

        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'show_series':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Esheeq(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
