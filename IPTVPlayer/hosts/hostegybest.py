# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'EgyBest'


class EgyBest(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'egybest', 'cookie': 'egybest.cookie'})

        self.MAIN_URL = 'https://egybest.homes/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/mRZ8WBx/egybest.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("EgyBest.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"EgyBest.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفلام أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?lang=الإنجليزية')},
                {'category': 'list_items', 'title': _('أفلام عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?lang=العربية')},
                {'category': 'list_items', 'title': _('أفلام أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?lang=الكورية')},
                {'category': 'list_items', 'title': _('أفلام تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?lang=التركية')},
                {'category': 'list_items', 'title': _('أفلام هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?lang=الهندية')},
                {'category': 'list_items', 'title': _('افلام انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?genre=40')},
                {'category': 'list_items', 'title': _('أفلام كرتون'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?genre=14')},
                {'category': 'list_items', 'title': _('الأفلام الرائجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/trending/movie?t=movie')},]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسلسلات أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?lang=الإنجليزية')},
                {'category': 'list_items', 'title': _('مسلسلات عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?lang=العربية')},
                {'category': 'list_items', 'title': _('مسلسلات أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?lang=الكورية')},
                {'category': 'list_items', 'title': _('مسلسلات تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?lang=التركية')},
                {'category': 'list_items', 'title': _('مسلسلات هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('series?lang=الهندية')},
                {'category': 'list_items', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?genre=40')},
                {'category': 'list_items', 'title': _('المسلسلات الرائجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/trending/serie?t=serie')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"EgyBest.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        cUrl = self.cm.meta['url']
        self.setMainUrl(cUrl)

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, 'الصفحة', ('</article', '>'), True)[1]
        nextPage = self.cm.ph.getSearchGroups(self.ph.decodeHtml(nextPage), f'''href=['"]([^'^"]+?p={page + 1})['"]''')[0]

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'load'), ('</article', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<a', '>', 'block'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''['"]ribbon.+?[>]([^>]+?)[$<]'''), ('rating', '''imdb-rating['"].+?([^>]+?)[$<]''')])

            if title == '':
                continue
            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': self.getFullUrl(nextPage, cUrl), 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"EgyBest.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, 'Trailer', ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': f"[{E2ColoR('yellow')}Trailer{E2ColoR('white')}]", 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<p', '>', 'description'), ('</p', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, '>المواسم', ('</article', '>'), True)[1]
        if Season:
            self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, '<a', ('</a', '>'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, '>الحلقات', ('</article', '>'), True)[1]
        if Episod:
            self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, '<a', ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(item, ('<h3', '>', 'title'), ('</h3', '>'), True)[1])
                if title == '':
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                info = self.ph.std_title(title.replace('-', ''), with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"EgyBest.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/search?query={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"EgyBest.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = self.cm.meta['url']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'WatchServers'), ('</div', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<button', '>'), ('</button', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-embed-url=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-mode', '>'), ('<i', '>'), False)[1])

            if title:
                title = f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}"

            if url != '':
                urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<article', '>', 'download-section'), ('</article', '>'), True)[1]
        tmpUrl = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''action=['"]([^"^']+?)['"]''')[0]).replace('download', 'script')

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Origin'] = self.up.getDomain(cUrl, False)
        params['header']['Referer'] = cUrl

        sts, data = self.cm.getPage(tmpUrl, params)
        if not sts:
            return

        playUrl = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''src=['"]([^'^"]+?)['"]''', ignoreCase=True)[0])
        title = (f"{cItem['title']} {E2ColoR('lightred')} [EgyBest]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(playUrl, True)}{E2ColoR('white')}")
        urlTab.append({'name': title, 'url': strwithmeta(playUrl, {'Referer': cUrl}), 'need_resolve': 1})
        self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"EgyBest.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem, data=None):
        printDBG(f"EgyBest.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'post-information'), ('</ul', '>'), True)[1]

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('year', '>'), ('</a', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('lang', '>'), ('</a', '>'), False)[1]):
            otherInfo['language'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('vueflags', '>'), ('</a', '>'), False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع', '>'), ('</div', '>'), False)[1]):
            otherInfo['genre'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<span', '>', 'ratingValue'), ('</span', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('المدة', '>'), ('</div', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الجودة', '<a>'), ('</div', '>'), False)[1]):
            otherInfo['quality'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, EgyBest(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
