# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'HalaCima'


class HalaCima(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'halacima', 'cookie': 'halacima.cookie'})

        self.MAIN_URL = 'https://halacima.media/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/0KNmGJh/halacima.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("HalaCima.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/برامج-وتلفزة')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"HalaCima.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أجنبية.html/')},
                {'category': 'list_items', 'title': _('أفــلام عــربـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-عربية.html/')},
                {'category': 'list_items', 'title': _('أفــلام هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-هندية.html/')},
                {'category': 'list_items', 'title': _('أفــلام آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-اسيوية.html/')},
                {'category': 'list_items', 'title': _('أفــلام تـركـيـة مـتـرجـمة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-تركي-مترجمة.html/')},
                {'category': 'list_items', 'title': _('أفــلام تـركـيـة مـدبـلـجـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-تركية-مدبلجة.html/')},
                {'category': 'list_items', 'title': _('أفــلام أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أنمي.html/')},
                {'category': 'list_items', 'title': _('ســلاسـلــ أفـلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/سلاسل-أفلام-كاملة.html/')}]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسـلـسـلات أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-أجنبية.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مترجمة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مدبلجة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات مـدبـلجـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-مدبلجة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة كـامـلـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-كاملة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-أسيوية.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات كــوريـــة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-كورية-مترجمة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-انمي.html/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"HalaCima.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^"^']+/page{page + 1}?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'main-content bg1'), ('<footer', '>', 'bg1'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'post'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-original=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''quality['"]>([^>]+?)[$<]'''), ('rating', '''icon-star-full2 f13['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"HalaCima.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<h3', '>', 'story'), ('</h3', '>'), False)[1])

        if 'series' in cItem['url']:
            Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'getSeasonsBySeries'), ('<div', '>', 'getPostRand'), True)[1]
            if Season:
                self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<div', '>', 'seasonNum'), ('</a', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>', 'content'), ('</h3', '>'), False)[1])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info['title_display']
                    otherInfo = f"{info['desc']}\n{desc}"

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addDir(params)

            Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'list-episodes'), ('</ul>', '</div>'), True)[1]
            if Episod:
                self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>'), ('</li', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</a', '>'), False)[1])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info['title_display']
                    otherInfo = f"{info['desc']}\n{desc}"

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'getMoviesCollection'), ('<div', '>', 'alert_message'), True)[1]
            if tmp:
                self.addMarker({'title': f"{E2ColoR('lime')} الــــفــلـم", 'icon': cItem['icon'], 'desc': ''})
                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
                self.addVideo(params)

                self.addMarker({'title': f"{E2ColoR('lime')} الــسلــسلـة", 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'block-post'), ('<div', '>', 'hover'))
                for item in tmp:
                    icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info['title_display']
                    otherInfo = f"{info['desc']}\n{desc}"

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': otherInfo})
                    self.addDir(params)
            else:
                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
                self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"HalaCima.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/search/{urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"HalaCima.getLinksForVideo [{cItem}]")
        baseUrl = cItem['url'].replace('movies', 'watch_movies').replace('episodes', 'watch_episodes')

        urlTab = []

        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        postID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''postID = ['"]([^"^']+?)['"]''')[0])
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'list-serv'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('</i', '>'), ('</a', '>'), False)[1])

            post_data = {'server': title, 'postID': postID, 'Ajax': '1'}

            sts, data = self.getPage(self.getFullUrl('/ajax/getPlayer'), post_data=post_data)
            if not sts:
                return
            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''SRC=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if 'megamax.me' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                tmp = cMegamax(sHosterUrl)
                if tmp != False:
                    for item in tmp:
                        url = item.split(',')[0].split('=')[1]
                        sQual = item.split(',')[1].split('=')[1]
                        sLabel = item.split(',')[2].split('=')[1]

                        title_ = (f"{cItem['title']} {E2ColoR('lightred')} [{sQual}]{E2ColoR('white')}{E2ColoR('yellow')} - {sLabel}{E2ColoR('white')}")
                        urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")
            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"HalaCima.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"HalaCima.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'singleInfo'), ('<div', '>', 'single-full'), True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<div', '>', 'the-story'), ('</h3', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, 'IMDB', ('</a', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('years', '>'), ('</a', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع', '>'), ('</li', '>'), False)[1]):
            otherInfo['genre'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الوقت', '>'), ('</a', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(tmp, ('اللغة', '>'), ('</a', '>'), False)[1]):
            otherInfo['language'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(tmp, ('دول الأنتاج', '>'), ('</a', '>'), False)[1]):
            otherInfo['country'] = Info

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'tvshow':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, HalaCima(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
