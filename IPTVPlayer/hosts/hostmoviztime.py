# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'MovizTime'


class MovizTime(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'moviztime', 'cookie': 'moviztime.cookie'})

        self.MAIN_URL = 'https://movtime94.homes/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/QNp7dsj/moviztime.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("MovizTime.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-أجنبية-مترجمة-d/')},
            {'category': 'anime', 'title': _('الأنـمـي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/برامج-تلفزيونية/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"MovizTime.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أجنبية/')},
                {'category': 'list_items', 'title': _('أفــلام أوروبـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أوروبية/')},
                {'category': 'list_items', 'title': _('أفــلام تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-تركية/')},
                {'category': 'list_items', 'title': _('أفــلام هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-هندية/')},
                {'category': 'list_items', 'title': _('أفــلام آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-آسيوية-مترجمة/')},
                {'category': 'list_items', 'title': _('أفــلام وثائـقـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-وثائقية/')},]
        elif category == 'anime':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/قائمة-الأنمي-b/أفلام-أنمي/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/قائمة-الأنمي-b/مسلسلات-أنمي/')},]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"MovizTime.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'posts-navigation'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^"^']+/{page + 1}/?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<section', '>', 'content'), ('</section', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'pinbox'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''_quality_tag['"].+?>([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"MovizTime.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('قصة الفيلم', '>'), ('</p', '>'), False)[1])
        if desc == '':
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('قصة المسلسل', '>'), ('</p', '>'), False)[1])

        if 'series' in cItem['url'] or 'anime' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'servers-block'), ('<style', '>', 'text/css'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<button', 'ep-item'), ('</button', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</button', '>'), False)[1])

                if 'kooora' in url:
                    continue

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"MovizTime.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/search/{urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"MovizTime.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'vidhls' in cItem['url']:
            return self.up.getVideoLinkExt(strwithmeta(cItem['url'], {'Referer': self.cm.meta['url']}))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'single_tab ifa'), ('</iframe', '>'), True)[1]
        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''data-src=['"]([^"^']+?)['"]''')[0])

        title = (f"{cItem['title']} {E2ColoR('lightred')} [{self.up.getHostName(url, True)}]{E2ColoR('white')}")
        urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.cm.meta['url']}), 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"MovizTime.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"MovizTime.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<p', '>', 'movie_details_section'), 'shorten_post_url', True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('قصة الفيلم', '>'), ('</p', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('تقييم الفيلم', '>'), ('</span', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('سنة الإنتاج', '>'), ('</span', '>'), False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('جودة الفيلم', '>'), ('</span', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('نوع الفيلم', '>'), ('</span', '>'), False)[1]):
            otherInfo['genre'] = Info

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'anime':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'serie' or category == 'tvshow':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, MovizTime(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
