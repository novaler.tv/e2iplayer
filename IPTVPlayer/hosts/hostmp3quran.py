# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'MP3Quran'


class MP3Quran(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'mp3quran', 'cookie': 'mp3quran.cookie'})

        self.MAIN_URL = 'https://www.mp3quran.net/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/CzNcfh4/MP3Quran.png'

        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Connection': 'keep-alive', 'Accept-Encoding': 'gzip', 'Content-Type': 'application/x-www-form-urlencoded', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("MP3Quran.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('القراء Ar'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/ar')},
            {'category': 'show_list', 'title': _('القراء En'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/eng')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"MP3Quran.listItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'home-reads letters'), ('<div', '>', 'side-letters'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'col-md-8'), ('</a>', '</div>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0].strip())
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            info = self.ph.std_title(title, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"MP3Quran.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'soar-list'), ('<div', '>', 'report_model'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'sora-info'), ('<div', '>', 'sora-btn more-btn'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('card-reciter-name', '>'), ('</a', '>'), False)[1])
            num = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<div', '>', 'sora-num'), ('</div', '>'), False)[1])

            if title != '':
                title = f"[{E2ColoR('yellow')}{num}{E2ColoR('white')}] {title}"

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addAudio(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"MP3Quran.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, 'download-btn', '>', True)[1]
        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''href=['"]([^"^']+?)['"]''')[0].replace('/download', ''))

        urlTab.append({'name': '', 'url': url, 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"MP3Quran.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"MP3Quran.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('sora-info', '>'), ('</div>', '</div>'), True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('sora-name', '>'), '</div>', False)[1])
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('sora-reciter', '>'), '</div>', False)[1])
        if desc == '':
            desc = cItem['desc']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('sora-rewaya', '>'), '</div>', False)[1]):
            otherInfo['type'] = Info

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'direct':
            self.liveItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, MP3Quran(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
