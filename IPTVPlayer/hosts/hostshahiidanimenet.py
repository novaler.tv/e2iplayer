# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Shahiid Anime'


class ShahiidAnime(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'shahiidanimenet', 'cookie': 'shahiidanime.cookie'})

        self.MAIN_URL = 'https://shahiid-anime.net/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/nfCx7PD/Shahiid-Anime.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("ShahiidAnime.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('الافــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anime/')},
            {'category': 'show_list', 'title': _('مســلــسـلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"ShahiidAnime.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</main', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^"^']+?page/{page + 1}/)['"]''')[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<header', '>', 'page-header'), ('<div', '>', 'foot-menu'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'wrap-poster'), ('<div', '>', 'one-poster'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+\.jpe?g)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(item, '<h2>', '</h2>', True)[1])
            desc = self.ph.extract_desc(item, [('rating', '''fa fa-star['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"ShahiidAnime.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if '/series/' in cItem['url']:
            tmpUrl = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''window.location = ['"]([^"^']+?)['"]''')[0])

            sts, data = self.getPage(tmpUrl)
            if not sts:
                return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'thumb-btn-trailer'), ('<i', '>', 'fab fa-youtube'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''href=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': f"[{E2ColoR('yellow')}Trailer{E2ColoR('white')}]", 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        siteUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'wrap-poster'), ('<span', '>', 'poster-views'))[1]
        siteUrl = self.cm.ph.getSearchGroups(siteUrl, '''href=['"]([^"^']+?)['"]''')[0]

        if siteUrl:
            params = {'good_for_fav': False, 'url': siteUrl, 'icon': cItem['icon'], 'desc': cItem['desc']}
            self.showEp(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)

    def showEp(self, cItem):
        printDBG(f"ShahiidAnime.showEp cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''window.location = ['"]([^"^']+?)['"]''')[0])

        sts, data = self.getPage(url)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'page-box'), ('</main', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<nav', '>', 'navbar'), ('</nav', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            info = self.ph.std_title(title, with_ep=True)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': self.getFullUrl(url), 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"ShahiidAnime.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/?s={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"ShahiidAnime.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        ajaxurl = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''var ajaxurl = ['"]([^"^']+?)['"]''')[0])
        action = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''['"]action['"]:['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('tabs-ul nav-justified clearfix', '>'), ('tabs-cntn', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            serv = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-serv=['"]([^"^']+?)['"]''')[0])
            frameserver = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-frameserver=['"]([^"^']+?)['"]''')[0])
            post = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-post=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '</i>', '</a>', False)[1])

            if 'iframe' in frameserver:
                post_data = {'action': action, 'post': post, 'frameserver': frameserver, 'is_film': 'yes', 'serv': serv}
            else:
                post_data = {'action': action, 'post': post, 'frameserver': frameserver, 'serv': serv}

            sts, data = self.getPage(ajaxurl, post_data=post_data)
            if not sts:
                return

            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''src=['"]([^"^']+?)['"]''')[0])
            if 'megamax' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                tmp = cMegamax(sHosterUrl)
                if tmp != False:
                    for item in tmp:
                        sHosterUrl = item.split(',')[0].split('=')[1]
                        sQual = item.split(',')[1].split('=')[1]
                        sLabel = item.split(',')[2].split('=')[1]

                        title_ = (f"{cItem['title']} {E2ColoR('lightred')} [{sQual}]{E2ColoR('white')}{E2ColoR('yellow')} - {sLabel}{E2ColoR('white')}")
                        urlTab.append({'name': title_, 'url': sHosterUrl, 'need_resolve': 1})

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} - {title}{E2ColoR('white')}")

            if 'megamax' in url:
                continue

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"ShahiidAnime.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"ShahiidAnime.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('head-s-movie-cntn', '>'), ('head-s-meta-end', '>'), True)[1]

        title = self.ph.std_title(self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, '<h1>', '</h1>', False)[1]), with_ep=True)['title_display']
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('head-s-story', '>'), ('</p>', '</div>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-star', '<span>'), '</span>', False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-clock-o', '<span>'), '</span>', False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-calendar-o', 'tag">'), '</span>', False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-low-vision', 'tag">'), '</span>', False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-umbrella', 'tag">'), '</span>', False)[1]):
            otherInfo['status'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('movies-cats', 'tag">'), '</span>', False)[1]):
            otherInfo['genre'] = Info

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, ShahiidAnime(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
