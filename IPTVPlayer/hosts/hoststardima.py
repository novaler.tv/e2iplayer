# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from base64 import b64decode

from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Stardima'


class Stardima(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'stardima', 'cookie': 'stardima.cookie'})

        self.MAIN_URL = 'https://stardima.vip/watch/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/c10TPqS/stardima.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Stardima.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('الأفــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('movies/')},
            {'category': 'show_list', 'title': _('مسلـسـلات مواسم'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('seasons/')},
            {'category': 'show_list', 'title': _('مسلـسـلات حلقات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('episodes/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Stardima.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'archive-content'), ('<div', '>', 'sidebar'), True)[1]
        if tmp == '':
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'search-page'), ('<div', '>', 'sidebar'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            if icon == '':
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('texto', '>'), '</div>', False)[1])
            other = self.ph.extract_desc(item, [('year', '''[<^]h3>.+?(\d{4})[$<]'''), ('rating', '''rating['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=other, with_ep=True)
            if title != '':
                title = info['title_display']
            otherInfo = f"{info['desc']}\n{desc}"

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Stardima.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if 'seasons' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('episodios', '>'), ('sbox', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('episodiotitle', '>'), '</span></div>')
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href.+?>([^>]+?)</''')[0])

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': cItem['desc']})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Stardima.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        url = self.getFullUrl(f'/watch/?s={urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Stardima.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        ajax_url = self.ph.decodeHtml(self.cm.ph.getSearchGroups(data, '''['"]ajax_url['"]:\s?['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('playeroptionsul', '>'), ('box_links', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            type_ = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-type=['"]([^"^']+?)['"]''')[0])
            post_ = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-post=['"]([^"^']+?)['"]''')[0])
            nume = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-nume=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''style.+?([^>]+?)</''')[0])

            post_data = {'action': 'doo_player_ajax', 'post': post_, 'nume': nume, 'type': type_}
            sts, data = self.getPage(ajax_url, post_data=post_data)
            if not sts:
                return

            url = self.getFullUrl(str(b64decode(self.cm.ph.getSearchGroups(data, '''['"]embed_url['"]:['"]([^"^']+?)['"]''')[0])))

            if '/embed2/?id=' in url:
                url = url.split('/embed2/?id=', 1)[1]
            if '?id=' in url:
                url = url.split('?id=', 1)[1]

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Stardima.getVideoLinks [{videoUrl}]")

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Stardima.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('sheader', '>'), ('single_relacionados', '>'), True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('title', '='), ('>', '<img'), False)[1])
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('description', '>'), ('<ul', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('repimdb', '>'), '</strong>', False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('duration', '>'), '</span>', False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('dateCreated', '>'), '</span>', False)[1]):
            otherInfo['year'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('country', '>'), '</span>', False)[1]):
            otherInfo['country'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-umbrella', 'tag">'), '</span>', False)[1]):
            otherInfo['status'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('movies-cats', 'tag">'), '</span>', False)[1]):
            otherInfo['genre'] = Info

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'list_items' or category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Stardima(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
