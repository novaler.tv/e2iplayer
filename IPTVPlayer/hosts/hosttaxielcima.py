# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.compat import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'TaxiElCima'


class TaxiElCima(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'taxielcima', 'cookie': 'taxielcima.cookie'})

        self.MAIN_URL = 'https://twoupon.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/dJSXSB4/taxielcima.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("TaxiElCima.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"TaxiElCima.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'other':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/عروض-تليفزيونية/')},
                {'category': 'list_items', 'title': _('مصــارعـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/عروض-المصارعة/')}]
            self.listsTab(NEW_CAT_TAB, cItem)
        else:
            if category == 'movei':
                sStart = '>افلام<'
            elif category == 'serie':
                sStart = '>مسلسلات<'

            tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                params = dict(cItem)
                params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

    def listItems(self, cItem):
        printDBG(f"TaxiElCima.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, f'''href=['"]([^'^"]+?)['"][^>]*?>{page + 1}<''')[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'CimaclubBlocks'), ('<div', '>', 'Hotlinks'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'SMallBloca'), ('<div', '>', 'BlockBar'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h2', '>'), ('</h2', '>'), False)[1])
            desc = self.ph.extract_desc(item, [('rating', '''ti-star['"^>]+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc)
            if title != '':
                title = info['title_display']
            desc = info['desc']

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"TaxiElCima.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('Description', '</span>'), ('<p', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'Seasons'), ('</ul', '>'), True)[1]
        if Season:
            self.addMarker({'title': f"{E2ColoR('lime')} مـــواســم", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': strwithmeta(url, {'Referer': cItem['url']}), 'icon': cItem['icon'], 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'Episodes'), ('</ul', '>'), True)[1]
        if Episod:
            self.addMarker({'title': f"{E2ColoR('lime')} الـحـلـقـات", 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info['title_display']
                otherInfo = f"{info['desc']}\n{desc}"

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"TaxiElCima.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        if searchType == 'all':
            url = self.getFullUrl(f'/?s={urllib_quote_plus(searchPattern)}')
        elif searchType == 'movies':
            url = self.getFullUrl(f'/?s=فيلم+{urllib_quote_plus(searchPattern)}')
        elif searchType == 'series':
            url = self.getFullUrl(f'/?s=مسلسل+{urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"TaxiElCima.getLinksForVideo [{cItem}]")
        urlTab = []

        sts, data = self.getPage(f"{cItem['url']}watch/")
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ServersTitle'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-url=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-url', '>'), ('</li', '>'), False)[1])

            if 'tuktukcimamulti' in url or 'megamax' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                tmp = cMegamax(sHosterUrl)
                if tmp != False:
                    for item in tmp:
                        url = item.split(',')[0].split('=')[1]
                        sQual = item.split(',')[1].split('=')[1]
                        sLabel = item.split(',')[2].split('=')[1]

                        title_ = (f"{cItem['title']} {E2ColoR('lightred')} [{sQual}]{E2ColoR('white')}{E2ColoR('yellow')} - {sLabel}{E2ColoR('white')}")
                        urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

            if title != '':
                title = (f"{cItem['title']} {E2ColoR('lightred')} [{title}]{E2ColoR('white')}{E2ColoR('yellow')} - {self.up.getHostName(url, True)}{E2ColoR('white')}")

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.cm.meta['url']}), 'need_resolve': 1})

            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"TaxiElCima.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"TaxiElCima.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'SingleContentTerms'), ('</ul', '>'), True)[1]

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('IMDBRS', '<span>'), ('</span', '>'), False)[1]):
            otherInfo['imdb_rating'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-md-calendar', '<span>'), ('</a', '>'), False)[1]):
            otherInfo['released'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-ios-tv', '<span>'), ('</span', '>'), False)[1]):
            otherInfo['quality'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-md-time', '<span>'), ('</span', '>'), False)[1]):
            otherInfo['duration'] = Info

        if Info := self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-md-stats', '<span>'), ('</span', '>'), False)[1]):
            otherInfo['views'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'other':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, TaxiElCima(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("All", "all"))
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("Tv Series", "series"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
