# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import traceback

from Plugins.Extensions.IPTVPlayer.compat import (urllib_urlopen,
                                                  urllib_urlretrieve)


def formatExceptionInfo(maxTBlevel=1):
    cla, exc, trbk = sys.exc_info()
    excName = cla.__name__
    try:
        excArgs = exc.__dict__["args"]
    except KeyError:
        excArgs = "<no args>"
    excTb = traceback.format_tb(trbk, maxTBlevel)
    cla = None
    exc = None
    trbk = None
    return f"{excName}\n{excArgs}\n{excTb}"


def CheckVer(params):
    url = f"http://iptvplayer.vline.pl/check.php?{params}"
    f = urllib_urlopen(url)
    data = f.read()
    print(f"CheckVer [{data}]\n")
    f.close()


def download(url, file):
    try:
        (tmpfile, headers) = urllib_urlretrieve(url, file)
        return 0, str(headers)
    except Exception:
        return 2, str(formatExceptionInfo())


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('Usage: python pwget url file', file=sys.stderr)
        sys.exit(1)

    try:
        params = sys.argv[1].split('?')[1]
        CheckVer(params)
    except Exception:
        pass

    sts, data = download(sys.argv[1], sys.argv[2])
    print(data, file=sys.stderr)
    sys.exit(sts)
