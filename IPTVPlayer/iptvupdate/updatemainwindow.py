# -*- coding: utf-8 -*-
#
#  Update iptv main window
#

from filecmp import cmp
from os import listdir as os_listdir
from os import path as os_path
from os import remove as os_remove

from Components.ActionMap import ActionMap
from Components.config import config
from Components.Label import Label
from Plugins.Extensions.IPTVPlayer.components.articleview import ArticleView
from Plugins.Extensions.IPTVPlayer.components.ihost import ArticleContent
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdh import DMHelper
from Plugins.Extensions.IPTVPlayer.iptvdm.iptvdownloadercreator import \
    UpdateDownloaderCreator
from Plugins.Extensions.IPTVPlayer.iptvupdate.iptvlist import IPTVUpdateList
from Plugins.Extensions.IPTVPlayer.libs.pCommon import CParsingHelper, common
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (
    FreeSpace, GetEnabledHostsList, GetGraphicsHash, GetHostsList,
    GetIconsHash, GetIPTVDMImgDir, GetIPTVPlayerVerstion, GetPyScriptCmd,
    GetShortPythonVersion, GetTmpDir, GetUpdateServerUri, SetGraphicsHash,
    SetIconsHash, WriteTextFile, formatBytes, iptv_system, mkdirs, printDBG,
    printExc, rm, rmtree)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import enum
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import iterDictKeys
from Screens.ChoiceBox import ChoiceBox
from Screens.MessageBox import MessageBox
from Screens.Screen import Screen
from Tools.BoundFunction import boundFunction
from Tools.Directories import SCOPE_PLUGINS, fileExists, resolveFilename

try:
    import json
except Exception:
    import simplejson as json
###################################################


class IPTVUpdateWindow(Screen):

    skin = """
    <screen name="IPTVUpdateMainWindow" position="center,center" size="620,440" title="" >
            <widget name="sub_title"    position="10,10" zPosition="2" size="600,35"  valign="center" halign="left"   font="Regular;22" transparent="1" foregroundColor="white" />
            <widget name="list"         position="10,50" zPosition="1" size="600,380" enableWrapAround="1" transparent="1" scrollbarMode="showOnDemand" />
            <widget name="console"      position="40,200"   zPosition="2" size="540,80" valign="center" halign="center"   font="Regular;34" transparent="0" foregroundColor="white" backgroundColor="black"/>
    </screen>"""

    ICONS = ['iconwait1.png', 'iconwait2.png', 'iconwait3.png', 'icondone.png', 'iconerror.png', 'iconwarning.png', 'iconcancelled.png']
    ICON = enum(WAITING=0, PROCESSING=1, PROCESSING_NOT_BREAK=2, PROCESSED=3, ERROR=4, WARNING=5, CANCELLED=6)

    def __init__(self, session, updateObjImpl, autoStart=True):
        printDBG("IPTVUpdateMainWindow.__init__ -------------------------------")
        Screen.__init__(self, session)
        self.autoStart = autoStart

        self.updateObjImpl = updateObjImpl
        self.updateObjImpl.setStepFinishedCallBack(self.stepFinished)

        self.setup_title = self.updateObjImpl.getSetupTitle()
        self["sub_title"] = Label(_(" "))
        self["console"] = Label(_("> Press OK to start <"))
        self["actions"] = ActionMap(["SetupActions", "ColorActions"],
                                    {
            "cancel": self.keyExit,
            "ok": self.keyOK,
        }, -2)
        self.list = []
        self["list"] = IPTVUpdateList([GetIPTVDMImgDir(x) for x in IPTVUpdateWindow.ICONS])
        self.currStep = 0
        self.onShow.append(self.onStart)
        self.onClose.append(self.__onClose)
        self.status = None

        self.messages = {}
        self.messages['not_interrupt'] = _("During processing, please do not interrupt.")
        self.messages['please_wait'] = _("During processing, please wait.")
        self.messages['not_aborted'] = _("Step [{0}] cannot be aborted. Please wait.")

    def __del__(self):
        printDBG("IPTVUpdateMainWindow.__del__ -------------------------------")

    def __onClose(self):
        printDBG("IPTVUpdateMainWindow.__onClose -----------------------------")
        self.updateObjImpl.setStepFinishedCallBack(None)
        self.updateObjImpl.terminate()
        self.onClose.remove(self.__onClose)
        self.updateObjImpl = None
        self.list = []
        self["list"].setList([])

    def onStart(self):
        self.onShow.remove(self.onStart)
        self.setTitle(self.updateObjImpl.getTitle())
        self["sub_title"].setText(self.updateObjImpl.getSubTitle())
        self["list"].setSelectionState(enabled=False)
        self.preparUpdateStepsList()
        if self.autoStart:
            self.doStart()
        else:
            self["console"].show()

    def doStart(self):
        if 0 < len(self.list):
            self.currStep = 0
            self.status = 'working'
            self.stepExecute()
            self["console"].hide()
        else:
            self["console"].setText(_("No steps to execute."))

    def reloadList(self):
        self["list"].hide()
        self["list"].setList([(x,) for x in self.list])
        self["list"].show()

    def keyOK(self):
        if not self.autoStart and None == self.status:
            self.doStart()
        else:
            currItem = self["list"].getCurrent()
            if self.status not in [None, 'working']:
                artItem = ArticleContent(title=currItem['title'], text=currItem['info'], images=[])
                self.session.open(ArticleView, artItem)

    def keyExit(self):
        if 'working' == self.status and not self.list[self.currStep].get('breakable', False):
            self.session.open(MessageBox, self.messages['not_aborted'], type=MessageBox.TYPE_INFO, timeout=5)
        else:
            self.close()

    def preparUpdateStepsList(self):
        self.list = self.updateObjImpl.getStepsList()
        self.reloadList()

    def stepExecute(self):
        self["list"].moveToIndex(self.currStep)
        if self.list[self.currStep].get('breakable', False):
            self.list[self.currStep].update({'info': self.messages['please_wait'], 'icon': self.ICON.PROCESSING})
        else:
            self.list[self.currStep].update({'info': self.messages['not_interrupt'], 'icon': self.ICON.PROCESSING_NOT_BREAK})
        self.reloadList()
        if self.updateObjImpl.isReadyToExecuteStep(self.currStep):
            self.list[self.currStep]['execFunction']()
        else:
            raise Exception("IPTVUpdateMainWindow.stepExecute seems that last step has not been finished.")

    def stepFinished(self, stsCode, msg):
        printDBG(f'IPTVUpdateMainWindow.stepFinished stsCode[{stsCode}], msg[{msg}]')
        nextStep = True
        if 0 != stsCode and self.list[self.currStep].get('repeatCount', 0) > 0:
            self.list[self.currStep]['repeatCount'] -= 1
            self.stepExecute()
            return

        if -1 == stsCode and not self.list[self.currStep]['ignoreError']:
            nextStep = False
            self.status = 'error'
            self.list[self.currStep].update({'info': msg, 'icon': self.ICON.ERROR})
            # cancel other steps
            currStep = self.currStep + 1
            while currStep < len(self.list):
                self.list[currStep].update({'info': _("Aborted"), 'icon': self.ICON.CANCELLED})
                currStep += 1
        elif 0 != stsCode:
            self.list[self.currStep].update({'info': msg, 'icon': self.ICON.WARNING})
        else:
            self.list[self.currStep].update({'info': msg, 'icon': self.ICON.PROCESSED})
        if nextStep:
            if self.currStep + 1 < len(self.list):
                self.currStep += 1
                self.stepExecute()
            else:
                self.status = 'done'
                if self.updateObjImpl.finalize():
                    self.close()
                    return
                else:
                    self["list"].setSelectionState(enabled=True)
        else:
            self.status = 'error'
            if self.updateObjImpl.finalize(False, msg):
                self.close()
                return
            else:
                self["list"].setSelectionState(enabled=True)
        self.reloadList()


class IUpdateObjectInterface():
    def __init__(self, session):
        printDBG("IUpdateObjectInterface.__init__ -------------------------------")

    def __del__(self):
        printDBG("IUpdateObjectInterface.__del__ -------------------------------")

    def getSetupTitle(self):
        return " "

    def getTitle(self):
        return _(" ")

    def getSubTitle(self):
        return _(" ")

    def finalize(self, success=True):
        return

    def terminate(self):
        return

    def getStepsList(self):
        return []

    def isReadyToExecuteStep(currStepIdx):
        return False

    def setStepFinishedCallBack(self, stepFinishedCallbackFun):
        self.stepFinishedHandler = stepFinishedCallbackFun

    def stepFinished(self, stsCode=-1, msg=''):
        if self.stepFinishedHandler:
            self.stepFinishedHandler(stsCode, msg)

    def createPath(self, path):
        sts = True
        msg = ''
        try:
            if not os_path.isdir(path) and not os_path.islink(path):
                if not mkdirs(path):
                    msg = _(f"The problem with creating a directory [{path}].")
                    sts = False
        except Exception:
            printExc()
            msg = _(f"Problem with the directory [{path}].")
            sts = False
        return sts, msg

    def checkForFreeSpace(self, dir, requairedSpace):
        sts = True
        msg = ''
        if not FreeSpace(dir, requairedSpace, 1):
            sts = False
            msg = _(f"There is no space in the directory [{dir}]\n Available[{formatBytes(FreeSpace(dir, None, 1))}], required [{formatBytes(requairedSpace)}].")
        return sts, msg


class UpdateMainAppImpl(IUpdateObjectInterface):
    VERSION_PATTERN = '''IPTV_VERSION\s?=\s?['"]([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)['"]'''

    def __init__(self, session, allowTheSameVersion=False):
        printDBG("UpdateMainAppImpl.__init__ -------------------------------")
        self.SERVERS_LIST_URLS = [GetUpdateServerUri('serwerslist.json')]

        self.session = session
        IUpdateObjectInterface.__init__(self, session)
        self.cm = common()

        self.allowTheSameVersion = allowTheSameVersion
        self.setup_title = _("IPTVPlayer - update")
        self.tmpDir = GetTmpDir('iptv_update')
        self.ExtensionPath = resolveFilename(SCOPE_PLUGINS, 'Extensions/')
        self.PluginPath = os_path.join(self.ExtensionPath, 'IPTVPlayer')
        self.ExtensionTmpPath = None

        self.terminating = False
        self.status = 'none'
        self.downloader = None
        self.cmd = None
        self.serversList = []
        self.gitlabList = {}

        self.serverGraphicsHash = ''
        self.serverIconsHash = ''
        self.localGraphicsHash = ''
        self.localIconsHash = ''
        self.currServIdx = 0

        self.sourceArchive = None
        self.graphicsSourceArchive = None
        self.iconsSourceArchive = None

        self.decKey = None
        self.decKeyFilePath = None

        self.destinationArchive = None
        self.serverIdx = 0

        self.messages = {}
        self.messages['completed'] = _("Completed.")
        self.messages['problem_removal'] = _("Problem with the removal of the previous version.\nStatus[{0}], outData[{1}].")
        self.messages['problem_install'] = _("Problem with installing the new version.\nStatus[{0}], outData[{1}]")
        self.messages['problem_copy'] = _("Problem with copy files.\nStatus[{0}], outData[{1}]")

        self.list = []
        self.user = config.plugins.iptvplayer.iptvplayer_login.value
        self.password = config.plugins.iptvplayer.iptvplayer_password.value

    def checkVersionFile(self, newVerPath):
        code = 0
        msg = _('Correct version.')

        newVerFile = os_path.join(newVerPath, 'version.py')
        if os_path.isfile(newVerFile):
            verPattern = self.VERSION_PATTERN
        else:
            newVerFile = os_path.join(newVerPath, 'version.pyo')
            verPattern = '([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)'

        try:
            # get new version
            with open(newVerFile, "r") as verFile:
                data = verFile.read()
            newVerNum = CParsingHelper.getSearchGroups(data, verPattern)[0]
            if newVerNum != self.serversList[self.currServIdx]['version']:
                code = -1
                msg = _(f"Wrong version. \n downloaded version [{newVerNum}] is different from the requested [{self.serversList[self.currServIdx]['version']}].")
        except Exception:
            printExc()
            code = -1
            msg = _(f"File [{newVerFile}] reading failed.")
        return code, msg

    def doRestart(self, *args):
        try:
            try:
                from Screens.Standby import TryQuitMainloop
                self.session.openWithCallback(self.doRestartFailed, TryQuitMainloop, retvalue=3)
            except Exception:
                printExc()
                msgtxt = _("Please remember that you use this plugin at your own risk.")
                self.session.open(MessageBox, _(f"E2 GUI restart after IPTVPlayer update to version[{self.serversList[self.currServIdx]['version']}].\n\n") + msgtxt, type=MessageBox.TYPE_INFO, timeout=5)
                from enigma import quitMainloop
                quitMainloop(3)
        except Exception:
            printExc()
            self.doRestartFailed()

    def doRestartFailed(self, *args):
        try:
            self.list[self.currStep].update({'info': _("Aborted"), 'icon': self.ICON.CANCELLED})
        except Exception:
            printExc()
        self.session.open(MessageBox, _("Restart GUI failed. \nPlease restart STB manually."), type=MessageBox.TYPE_INFO, timeout=5)

    #########################################################
    # INREFACE IMPLEMENTATION METHODS
    #########################################################
    def getSetupTitle(self):
        return _("IPTVPlayer - update")

    def getTitle(self):
        return _("IPTVPlayer - update")

    def getSubTitle(self):
        return _(f"Currently you have version [{GetIPTVPlayerVerstion()}].")

    def finalize(self, success=True, errorMsg=""):
        if success:
            self.session.openWithCallback(self.doRestart, MessageBox, _("Update completed successfully. For the moment, the system will reboot."), type=MessageBox.TYPE_INFO, timeout=10)
            return False
        else:
            message = errorMsg
            # Failed message:
            message += "\n\n" + _("Update failed.\nCheck the status by selecting interesting and pressing OK.")
            self.session.open(MessageBox, message, type=MessageBox.TYPE_ERROR, timeout=-1)
            return False

    def terminate(self):
        self.terminating = True
        if self.downloader:
            self.downloader.terminate()
            self.downloader = None
        if self.cmd:
            self.cmd.kill()
        self.clearTmpData()

    def __getStepDesc(self, title, execFunction, breakable=True, ignoreError=False, repeatCount=0):
        return {'title': title, 'execFunction': execFunction, 'breakable': breakable, 'ignoreError': ignoreError, 'info': _("Pending"), 'progressFun': None, 'repeatCount': repeatCount, 'icon': IPTVUpdateWindow.ICON.WAITING}

    def getStepsList(self):
        self.list = []
        if config.plugins.iptvplayer.gitlab_repo.value and config.plugins.iptvplayer.preferredupdateserver.value == '2':
            self.list.append(self.__getStepDesc(title=_("Add repository last version."), execFunction=self.stepGetGitlab, ignoreError=True))
        self.list.append(self.__getStepDesc(title=_("Obtaining server list."), execFunction=self.stepGetServerLists))
        self.list.append(self.__getStepDesc(title=_("Downloading an update packet."), execFunction=self.stepGetArchive))
        self.list.append(self.__getStepDesc(title=_("Extracting an update packet."), execFunction=self.stepUnpackArchive))
        self.list.append(self.__getStepDesc(title=_("Copy post installed binaries."), execFunction=self.stepCopyPostInatalledBinaries, breakable=True, ignoreError=True))
        self.list.append(self.__getStepDesc(title=_("Executing user scripts."), execFunction=self.stepExecuteUserScripts))
        self.list.append(self.__getStepDesc(title=_("Checking version."), execFunction=self.stepCheckFiles))
        self.list.append(self.__getStepDesc(title=_("Removing unnecessary files."), execFunction=self.stepRemoveUnnecessaryFiles, breakable=True, ignoreError=True))
        self.list.append(self.__getStepDesc(title=_("Confirmation of installation."), execFunction=self.stepConfirmInstalation))
        self.list.append(self.__getStepDesc(title=_("Removing the old version."), execFunction=self.stepRemoveOldVersion, breakable=False, ignoreError=True, repeatCount=2))
        self.list.append(self.__getStepDesc(title=_("Installing new version."), execFunction=self.stepInstallNewVersion, breakable=False, ignoreError=False, repeatCount=3))
        return self.list

    def isReadyToExecuteStep(self, currStepIdx):
        if not self.downloader and not self.cmd:
            return True
        else:
            return False

    #########################################################
    # STEPS IMPLEMENTATION METHODS
    #########################################################
    def stepGetServerLists(self):
        printDBG('UpdateMainAppImpl.stepGetServerLists')
        self.clearTmpData()
        sts, msg = self.createPath(self.tmpDir)
        if not sts:
            self.stepFinished(-1, msg)
            return
        serverUrl = self.SERVERS_LIST_URLS[self.serverIdx]
        self.downloader = UpdateDownloaderCreator(serverUrl)
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__serversListDownloadFinished, None))
        self.downloader.start(serverUrl, os_path.join(self.tmpDir, 'serwerslist2.json'))

    def stepGetGitlab(self):
        printDBG('UpdateMainAppImpl.stepGetGitlab')
        self.clearTmpData()
        sts, msg = self.createPath(self.tmpDir)
        if not sts:
            self.stepFinished(-1, msg)
            return
        serverUrl = "https://gitlab.com/{0}/e2iplayer/raw/main/IPTVPlayer/version.py".format(config.plugins.iptvplayer.gitlab_repo.value)
        self.downloader = UpdateDownloaderCreator(serverUrl)
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__serversListGitlabFinished, None))
        self.downloader.start(serverUrl, os_path.join(self.tmpDir, 'lastversion.py'))

    def stepGetArchive(self):
        self.downloader = UpdateDownloaderCreator(self.serversList[self.currServIdx]['url'])
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__archiveDownloadFinished, None))
        self.sourceArchive = os_path.join(self.tmpDir, 'iptvplayer_archive.tar.gz')
        url = self.serversList[self.currServIdx]['url']

        if self.decKey:
            from hashlib import sha256
            url += '&' if '?' in url else '?'
            url += f'hash={sha256(self.user).hexdigest()}'

        self.downloader.start(url, self.sourceArchive)

    def stepUnpackArchive(self):
        self.destinationArchive = os_path.join(self.tmpDir, 'iptv_archive')
        self.ExtensionTmpPath = os_path.join(self.destinationArchive, self.serversList[self.currServIdx]['subdir'])
        printDBG(f'UpdateMainAppImpl.stepUnpackArchive: sourceArchive[{self.sourceArchive}] -> destinationArchive[{self.destinationArchive}] -> ExtensionTmpPath[{self.ExtensionTmpPath}]')
        sts, msg = self.createPath(self.destinationArchive)
        if not sts:
            self.stepFinished(-1, msg)
            return

        cmd = f'rm -f "{self.destinationArchive}/*" > /dev/null 2>&1; tar -xzf "{self.sourceArchive}" -C "{self.destinationArchive}" 2>&1; PREV_RET=$?; rm -f "{self.sourceArchive}" > /dev/null 2>&1; (exit $PREV_RET)'
        self.cmd = iptv_system(cmd, self.__unpackCmdFinished)

    def stepGetEncKey(self):
        printDBG('UpdateMainAppImpl.stepGetEncKey')
        from hashlib import sha256
        nameHash = sha256(self.user).hexdigest()

        self.downloader = UpdateDownloaderCreator(self.serversList[self.currServIdx]['graphics_url'])
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__encKeyDownloadFinished, None))
        encKeyBin = os_path.join(self.tmpDir, 'iptvplayer_enc_key.bin')
        self.downloader.start(self.serversList[self.currServIdx]['enc_url_tmp'].format(nameHash), encKeyBin)

    def stepDecryptArchive(self):
        printDBG(f'UpdateMainAppImpl.stepDecryptArchive: sourceArchive[{self.sourceArchive}]')
        from hashlib import sha256
        try:
            self.decKeyFilePath = GetTmpDir(sha256(self.user).hexdigest()[:16])
            with open(self.decKeyFilePath, "wb") as f:
                f.write(self.decKey)
            cmd = f'{GetPyScriptCmd("decrypt")} "{resolveFilename(SCOPE_PLUGINS, "Extensions/IPTVPlayer/libs/")}" "{self.sourceArchive}" "{self.decKeyFilePath}" '
        except Exception:
            printExc()
            cmd = 'fake'

        self.cmd = iptv_system(cmd, self.__decryptionCmdFinished)

    def stepGetGraphicsArchive(self):
        if '' == self.serversList[self.currServIdx]['graphics_url']:
            self.stepFinished(0, _('Skipped.'))
            return

        packageName = 'graphics.tar.gz'
        self.downloader = UpdateDownloaderCreator(self.serversList[self.currServIdx]['graphics_url'])
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__archiveDownloadFinished, None))
        self.graphicsSourceArchive = os_path.join(self.tmpDir, 'iptvplayer_graphics_archive.tar.gz')
        self.downloader.start(self.serversList[self.currServIdx]['graphics_url'] + packageName, self.graphicsSourceArchive)

    def stepUnpackGraphicsArchive(self):
        if '' == self.serversList[self.currServIdx]['graphics_url']:
            self.stepFinished(0, _('Skipped.'))
            return

        cmd = f'tar -xzf "{self.graphicsSourceArchive}" -C "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer/")}" 2>&1; PREV_RET=$?; rm -f "{self.graphicsSourceArchive}" > /dev/null 2>&1; (exit $PREV_RET)'
        self.cmd = iptv_system(cmd, self.__unpackCmdFinished)

    def stepGetIconsArchive(self):
        if '' == self.serversList[self.currServIdx]['icons_url'] or \
                not config.plugins.iptvplayer.ListaGraficzna.value:
            self.stepFinished(0, _('Skipped.'))
            return

        packageName = f'icons{config.plugins.iptvplayer.IconsSize.value}.tar.gz'
        self.downloader = UpdateDownloaderCreator(self.serversList[self.currServIdx]['icons_url'])
        self.downloader.subscribersFor_Finish.append(boundFunction(self.downloadFinished, self.__archiveDownloadFinished, None))
        self.iconsSourceArchive = os_path.join(self.tmpDir, 'iptvplayer_icons_archive.tar.gz')
        self.downloader.start(self.serversList[self.currServIdx]['icons_url'] + packageName, self.iconsSourceArchive)

    def stepUnpackIconsArchive(self):
        if '' == self.serversList[self.currServIdx]['icons_url'] or \
                not config.plugins.iptvplayer.ListaGraficzna.value:
            self.stepFinished(0, _('Skipped.'))
            return

        cmd = f'tar -xzf "{self.iconsSourceArchive}" -C "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer/")}" 2>&1; PREV_RET=$?; rm -f "{self.iconsSourceArchive}" > /dev/null 2>&1; (exit $PREV_RET)'
        self.cmd = iptv_system(cmd, self.__unpackCmdFinished)

    def stepCheckFiles(self):
        code, msg = self.checkVersionFile(os_path.join(self.ExtensionTmpPath, 'IPTVPlayer'))
        self.stepFinished(code, msg)

    def stepRemoveUnnecessaryFiles(self):
        printDBG("stepRemoveUnnecessaryFiles")
        playerSelectorPath = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer/icons/PlayerSelector/')
        logosPath = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer/icons/logos/')
        hostsPath = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer/hosts/')
        webPath = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer/Web/')
        cmds = []
        iconSize = int(config.plugins.iptvplayer.IconsSize.value)
        if not config.plugins.iptvplayer.ListaGraficzna.value:
            iconSize = 0
        for size in [135, 120, 100]:
            if size != iconSize:
                cmds.append(f'rm -f {hostsPath}*{size}.png')
                cmds.append(f'rm -f {playerSelectorPath}marker{size + 45}.png')

        # remove Web iterface module if not needed
        if not config.plugins.iptvplayer.IPTVWebIterface.value:
            cmds.append(f'rm -rf {webPath}')

        # removing not needed hosts
        if config.plugins.iptvplayer.remove_diabled_hosts.value:
            enabledHostsList = GetEnabledHostsList()
            hostsFromList = GetHostsList(fromList=True, fromHostFolder=False)
            hostsFromFolder = GetHostsList(fromList=False, fromHostFolder=True)
            hostsToRemove = []
            for hostItem in hostsFromList:
                if hostItem not in enabledHostsList and hostItem in hostsFromFolder:
                    cmds.append(f'rm -f {playerSelectorPath}{hostItem}*.png')
                    cmds.append(f'rm -f {logosPath}{hostItem}logo.png')
                    cmds.append(f'rm -f {hostsPath}host{hostItem}.py*')

            # we need to prepare temporary file with removing cmds because cmd can be to long
            cmdFilePath = GetTmpDir('.iptv_remove_cmds.sh')
            cmds.insert(0, '#!/bin/sh')
            cmds.append('exit 0\n')
            text = '\n'.join(cmds)
            WriteTextFile(cmdFilePath, text, 'ascii')
            cmd = '/bin/sh "{0}" '.format(cmdFilePath)
            # cmd = '/bin/sh "{0}" && rm -rf "{1}" '.format(cmdFilePath, cmdFilePath)
        else:
            cmd = ' && '.join(cmds)
        printDBG(f"stepRemoveUnnecessaryFiles cmdp[{cmd}]")
        self.cmd = iptv_system(cmd, self.__removeUnnecessaryFilesCmdFinished)

    def stepCopyGraphicsWithoutIcons(self):
        # copy whole old icon directory
        # remove IPTVPlayer/icons/PlayerSelector dir, it will be replaced by new one
        oldIconsDir = os_path.join(self.ExtensionPath, 'IPTVPlayer', 'icons')
        newIconsDir = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer', 'icons')
        newPlayerSelectorDir = os_path.join(newIconsDir, 'PlayerSelector')
        cmd = f'mkdir -p "{newIconsDir}" && cp -rf "{oldIconsDir}"/* "{newIconsDir}"/ && rm -rf "{newPlayerSelectorDir}"'
        printDBG(f'UpdateMainAppImpl.stepCopyGraphicsWithoutIcons cmd[{cmd}]')
        self.cmd = iptv_system(cmd, self.__copyOldCmdFinished)

    def stepCopyAllGraphics(self):
        # copy whole old icon directory
        oldIconsDir = os_path.join(self.ExtensionPath, 'IPTVPlayer', 'icons')
        newIconsDir = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer', 'icons')
        cmd = f'mkdir -p "{newIconsDir}" && cp -rf "{oldIconsDir}"/* "{newIconsDir}"/'
        printDBG(f'UpdateMainAppImpl.stepCopyAllGraphics cmd[{cmd}]')
        self.cmd = iptv_system(cmd, self.__copyOldCmdFinished)

    def stepCopyOnlyIcons(self):
        # create subdir IPTVPlayer/icons/ and copy only PlayerSelector dir to it
        cmd = f'rm -rf "{os_path.join(self.ExtensionPath, "IPTVPlayer")}"/*'
        oldPlayerSelectorDir = os_path.join(self.ExtensionPath, 'IPTVPlayer', 'icons', 'PlayerSelector')
        newPlayerSelectorDir = os_path.join(self.ExtensionTmpPath, 'IPTVPlayer', 'icons', 'PlayerSelector')
        cmd = f'mkdir -p "{newPlayerSelectorDir}" && cp -rf "{oldPlayerSelectorDir}"/* "{newPlayerSelectorDir}"/'
        printDBG(f'UpdateMainAppImpl.stepCopyOnlyIcons cmd[{cmd}]')
        self.cmd = iptv_system(cmd, self.__copyOldCmdFinished)

    def stepCopyPostInatalledBinaries(self, init=True, code=0, msg=''):
        # get users scripts
        if init:
            self.copyBinariesCmdList = []

            if fileExists(f"{self.PluginPath}/libs/iptvsubparser/_subparser.so"):
                self.copyBinariesCmdList.append(f'cp -f "{self.PluginPath}/libs/iptvsubparser/_subparser.so" "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer")}/libs/iptvsubparser/_subparser.so"  2>&1 ')

            if fileExists(f"{self.PluginPath}/libs/e2icjson/e2icjson.so"):
                self.copyBinariesCmdList.append(f'cp -f "{self.PluginPath}/libs/e2icjson/e2icjson.so" "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer")}/libs/e2icjson/e2icjson.so"  2>&1 ')

            binPath = f"{self.PluginPath}/bin/"
            binariesTab = [
                ('exteplayer3', config.plugins.iptvplayer.exteplayer3path.value),
                ('gstplayer', config.plugins.iptvplayer.gstplayerpath.value),
                ('wget', config.plugins.iptvplayer.wgetpath.value),
                ('hlsdl', config.plugins.iptvplayer.hlsdlpath.value),
                ('cmdwrap', config.plugins.iptvplayer.cmdwrappath.value),
                ('duk', config.plugins.iptvplayer.dukpath.value),
                ('f4mdump', config.plugins.iptvplayer.f4mdumppath.value),
                ('uchardet', config.plugins.iptvplayer.uchardetpath.value)]
            for binItem in binariesTab:
                if binPath in binItem[1]:
                    self.copyBinariesCmdList.append(f'cp -f "{binPath}/{binItem[0]}" "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer")}/bin/"  2>&1 ')

            if 0 < len(self.copyBinariesCmdList):
                self.copyBinariesMsg = ''
            else:
                self.copyBinariesMsg = _("Nothing to do here.")

        self.copyBinariesMsg += msg
        if 0 != code:
            self.stepFinished(-1, _(f"Problem with copy binary.\n{self.copyBinariesMsg}"))
        elif 0 < len(self.copyBinariesCmdList):
            cmd = self.copyBinariesCmdList.pop()
            self.cmd = iptv_system(cmd, self.__copyBinariesCmdFinished)
        else:
            self.stepFinished(0, _("Completed.\n") + self.copyBinariesMsg)

    def stepExecuteUserScripts(self, init=True, code=0, msg=''):
        # get users scripts
        if init:
            self.customUserCmdList = self.__getUserCmdsList()
            if 0 < len(self.customUserCmdList):
                self.customUserMsg = ''
            else:
                self.customUserMsg = _("No user scripts.")

        self.customUserMsg += msg
        if 0 != code:
            self.stepFinished(-1, _("Problem with user script execution.\n") + self.customUserMsg)
        elif 0 < len(self.customUserCmdList):
            cmd = self.customUserCmdList.pop()
            self.cmd = iptv_system(cmd, self.__userCmdFinished)
        else:
            self.stepFinished(0, _("Completed.\n") + self.customUserMsg)

    def stepConfirmInstalation(self, confirmed=None):
        if None == confirmed:
            self.session.openWithCallback(self.stepConfirmInstalation, MessageBox, _(
                f"Version [{self.serversList[self.currServIdx]['version']}] is ready for installation. After installation, restart of the system will be done.\nDo you want to continue?"), type=MessageBox.TYPE_YESNO, timeout=-1)
        else:
            if confirmed:
                self.stepFinished(0, _("Installation has been confirmed."))
            else:
                self.stepFinished(-1, _("Installation has been aborted."))

    def stepRemoveOldVersion(self):
        cmd = f'rm -rf "{self.PluginPath}"/*'
        printDBG(f'UpdateMainAppImpl.stepRemoveOldVersion cmd[{cmd}]')
        self.cmd = iptv_system(cmd, self.__removeOldVersionCmdFinished)

    def stepInstallNewVersion(self):
        cmd = ''
        try:
            url = f"http://iptvplayer.vline.pl/check.php?ver={self.serversList[self.currServIdx]['version']}&type={self.serversList[self.currServIdx]['pyver']}"
            cmd = f'{config.plugins.iptvplayer.wgetpath.value} "{url}" -t 1 -T 10 -O - > /dev/null 2>&1; '
        except Exception:
            printExc()

        cmd += f'cp -rf "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer")}"/* "{self.PluginPath}"/ 2>&1'
        printDBG(f'UpdateMainAppImpl.stepInstallNewVersion cmd[{cmd}]')
        self.cmd = iptv_system(cmd, self.__installNewVersionCmdFinished)

    def downloadFinished(self, callBackFun, arg, status):
        if 0:
            # usage of 'file' in below line seems to be a mistake. file is a standard object and doesn't return anything valuable - just pointer to itself
            # Example:
            #  UpdateMainAppImpl.downloadFinished file[<type 'file'>], status[STS_DOWNLOADED]
            printDBG(f'UpdateMainAppImpl.downloadFinished file[{file}], status[{status}]')
        else:
            printDBG(f'UpdateMainAppImpl.downloadFinished, status[{status}]')
        self.downloader.subscribersFor_Finish = []
        if self.terminating:
            printDBG('UpdateMainAppImpl.downloadFinished closing')
            return
        callBackFun(arg, status)

    def clearTmpData(self):
        try:
            rmtree(self.tmpDir)
        except Exception:
            printExc('WARNING')

    ##############################################################################
    # SERWERS LISTS STEP'S PRIVATES METHODS
    ##############################################################################
    def __addLastVersion(self, servers):
        mainUrl = "https://gitlab.com/iptvplayer-for-e2/iptvplayer-for-e2"
        sts, data = self.cm.getPage(f'{mainUrl}/tree/main')
        if sts:
            crcSum = CParsingHelper.getSearchGroups(data, '''['"]/iptvplayer-for-e2/iptvplayer-for-e2/commit/([^"^']+?)['"]>''')[0]
            if 40 == len(crcSum):
                finalurl = f'{mainUrl}/blob/{crcSum}/IPTVPlayer/version.py'
                sts, data = self.cm.getPage(finalurl)
                if sts:
                    newVerNum = CParsingHelper.getSearchGroups(data, '&quot;([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)&quot;')[0]
                    sourceUrl = f"{mainUrl}/repository/archive.tar.gz?ref={crcSum}"
                    server = {'name': 'gitlab.com', 'version': newVerNum, 'url': sourceUrl, 'subdir': 'iptvplayer-for-e2.git/', 'pyver': 'X.X', 'packagetype': 'sourcecode'}
                    printDBG(f"UpdateMainAppImpl.__addLastVersion server: [{str(server)}]")
                    servers.append(server)
            else:
                printDBG(f"Wrong crcSum[{crcSum}]")

    def __serversListGitlabFinished(self, arg, status):
        url = self.downloader.getUrl()
        filePath = self.downloader.getFullFileName()
        self.downloader = None
        printDBG(f'UpdateMainAppImpl.__serversListGitlabFinished url[{url}], filePath[{filePath}] ')
        if DMHelper.STS.DOWNLOADED != status:
            msg = _(f"Problem with downloading the packet:\n[{url}].")
            self.stepFinished(-1, msg)
        else:
            newVerNum = ''
            newVerFile = os_path.join(self.tmpDir, 'lastversion.py')
            verPattern = self.VERSION_PATTERN
            if os_path.isfile(newVerFile):
                try:
                    with open(newVerFile, "r") as verFile:
                        data = verFile.read()
                    newVerNum = CParsingHelper.getSearchGroups(data, verPattern)[0]
                except Exception:
                    printExc()
                if 13 == len(newVerNum):
                    sourceUrl = f"https://gitlab.com/{config.plugins.iptvplayer.gitlab_repo.value}/e2iplayer/-/archive/main/e2iplayer-main.tar.gz"
                    self.gitlabList = {'name': 'gitlab.com', 'version': newVerNum, 'url': sourceUrl, 'subdir': 'e2iplayer-main/', 'pyver': 'X.X', 'packagetype': 'sourcecode'}
                    printDBG(f"__serversListGitlabFinished: [{str(self.gitlabList)}]")
                    self.stepFinished(0, _(f"GitLab version from {config.plugins.iptvplayer.gitlab_repo.value} was downloaded successfully."))
                else:
                    msg = _(f"Wrong version: [{str(self.gitlabList)}].")
                    self.stepFinished(-1, msg)
            else:
                msg = _(f"File not found:\n[{filePath}].")
                self.stepFinished(-1, msg)
        return

    def __serversListDownloadFinished(self, arg, status):
        def ServerComparator(x, y):
            try:
                val1 = int(x['version'].replace('.', ''))
            except Exception:
                val1 = 0
            try:
                val2 = int(y['version'].replace('.', ''))
            except Exception:
                val2 = 0
            printDBG(f"ServerComparator val1[{val1}], val2[{val2}]")
            return cmp(val1, val2)
        try:
            currVerNum = int(GetIPTVPlayerVerstion().replace('.', ''))
        except Exception:
            printDBG(f'Version of the current instalation [{GetIPTVPlayerVerstion()}]')
            currVerNum = 0
        pythonVer = GetShortPythonVersion()

        url = self.downloader.getUrl()
        filePath = self.downloader.getFullFileName()
        self.downloader = None
        if DMHelper.STS.DOWNLOADED != status:
            self.serverIdx += 1
            if self.serverIdx < len(self.SERVERS_LIST_URLS):
                self.stepGetServerLists()
            else:
                urls = ""
                for idx in range(self.serverIdx):
                    urls += f"{self.SERVERS_LIST_URLS[idx]}, "
                if 1 < len(urls):
                    urls = urls[:-2]
                self.stepFinished(-1, _(f"Problem with downloading the server list from [{urls}]."))
        else:
            # process servers list
            serverGraphicsHash = ''
            serverIconsHash = ''
            serversList = []
            try:
                with open(filePath) as fileHandle:
                    jsonData = json.load(fileHandle)
                for server in jsonData['servers']:
                    serverOK = True
                    extServer = {}
                    for key in ['name', 'version', 'url', 'subdir', 'pyver', 'packagetype', 'graphics_url', 'icons_url']:
                        if key not in iterDictKeys(server):
                            serverOK = False
                            break
                        else:
                            extServer[key] = server[key].encode('utf8')

                    # printDBG("")
                    if not serverOK:
                        continue
                    enc = server.get('enc')
                    encUrlTmp = server.get('enc_url_tmp')
                    if enc:
                        if (not self.user or not self.password):
                            continue
                        else:
                            extServer['enc'] = enc.encode('utf8')
                            extServer['enc_url_tmp'] = encUrlTmp.encode('utf8')

                    newServer = dict(extServer)
                    serversList.append(newServer)
                serverGraphicsHash = str(jsonData.get('graphics_hash', ''))
                serverIconsHash = str(jsonData.get('icons_hash', ''))
            except Exception:
                printExc()
                self.serverIdx += 1
                if self.serverIdx < len(self.SERVERS_LIST_URLS):
                    self.stepGetServerLists()
                else:
                    self.stepFinished(-1, _("Problem with parsing the server list."))
                return
            if config.plugins.iptvplayer.hiddenAllVersionInUpdate.value:
                self.__addLastVersion(serversList)  # get last version from gitlab.com only for developers

            if config.plugins.iptvplayer.gitlab_repo.value and config.plugins.iptvplayer.preferredupdateserver.value == '2':
                serversList.append(self.gitlabList)

            self.serversList = serversList
            self.serverGraphicsHash = serverGraphicsHash
            self.serverIconsHash = serverIconsHash
            if 0 < len(serversList):
                options = []
                for idx in range(len(serversList)):
                    server = serversList[idx]
                    if not config.plugins.iptvplayer.hiddenAllVersionInUpdate.value:
                        try:
                            newVerNum = int(server['version'].replace('.', ''))
                        except Exception:
                            continue
                        printDBG(f"newVerNum[{newVerNum}], currVerNum[{currVerNum}]")
                        if newVerNum < currVerNum and not config.plugins.iptvplayer.downgradePossible.value:
                            continue
                        if newVerNum == currVerNum and not self.allowTheSameVersion:
                            continue
                        if 'X.X' != server['pyver'] and pythonVer != server['pyver']:
                            continue
                        if config.plugins.iptvplayer.possibleUpdateType.value not in [server['packagetype'], 'all']:  # "sourcecode", "precompiled"
                            continue

                    name = f"| {server['version']} | python {server['pyver']} | {server['packagetype']} | {server['name']} |"
                    printDBG(f"server list: {name}")
                    options.append((name, idx))
                if 1 == len(options) and not config.plugins.iptvplayer.downgradePossible.value:
                    self.__selServerCallBack(options[0])
                elif 0 < len(options):
                    self.session.openWithCallback(self.__selServerCallBack, ChoiceBox, title=_("Select update server"), list=options)
                else:
                    self.stepFinished(-1, _("There is no update for the current configuration."))
            else:
                self.stepFinished(-1, _("Update not available."))

    def __selServerCallBack(self, retArg):
        if retArg and len(retArg) == 2:
            self.localIconsHash = GetIconsHash()
            self.localGraphicsHash = GetGraphicsHash()

            self.currServIdx = retArg[1]
            list = []
            if 'graphics_url' in self.serversList[self.currServIdx]:
                if self.localGraphicsHash == '' or self.serverGraphicsHash == '' or \
                        self.localGraphicsHash != self.serverGraphicsHash:
                    list.append(self.__getStepDesc(title=_("Downloading graphics package."), execFunction=self.stepGetGraphicsArchive))
                    list.append(self.__getStepDesc(title=_("Extracting graphics package."), execFunction=self.stepUnpackGraphicsArchive))
                    oldGraphics = False
                else:
                    oldGraphics = True

                if self.localIconsHash == '' or self.serverIconsHash == '' or self.localIconsHash != self.serverIconsHash:
                    if oldGraphics:
                        list.append(self.__getStepDesc(title=_("Copy graphics without icons."), execFunction=self.stepCopyGraphicsWithoutIcons))

                    if config.plugins.iptvplayer.ListaGraficzna.value:
                        list.append(self.__getStepDesc(title=_("Downloading icons package."), execFunction=self.stepGetIconsArchive))
                        list.append(self.__getStepDesc(title=_("Extracting icons package."), execFunction=self.stepUnpackIconsArchive))
                else:
                    if oldGraphics:
                        if config.plugins.iptvplayer.ListaGraficzna.value:
                            list.append(self.__getStepDesc(title=_("Copy all graphics."), execFunction=self.stepCopyAllGraphics))
                        else:
                            list.append(self.__getStepDesc(title=_("Copy graphics without icons."), execFunction=self.stepCopyGraphicsWithoutIcons))
                    elif config.plugins.iptvplayer.ListaGraficzna.value:
                        list.append(self.__getStepDesc(title=_("Copy icons."), execFunction=self.stepCopyOnlyIcons))

            if config.plugins.iptvplayer.gitlab_repo.value and config.plugins.iptvplayer.preferredupdateserver.value == '2':
                self.list[4:4] = list
            else:
                self.list[3:3] = list
            if 'enc' in self.serversList[self.currServIdx]:
                self.list.insert(1, self.__getStepDesc(title=_("Get decryption key."), execFunction=self.stepGetEncKey))
                self.list.insert(3, self.__getStepDesc(title=_("Decrypt archive."), execFunction=self.stepDecryptArchive))

            self.stepFinished(0, _(f"Selected version [{retArg[0]}]."))
        else:
            self.stepFinished(-1, _("Update server not selected."))

    ##############################################################################
    # GET ARCHIVE STEP'S PRIVATES METHODS
    ##############################################################################
    def __archiveDownloadFinished(self, arg, status):
        url = self.downloader.getUrl()
        filePath = self.downloader.getFullFileName()
        remoteFileSize = self.downloader.getRemoteFileSize()
        localFileSize = self.downloader.getLocalFileSize(True)
        self.downloader = None
        printDBG(f'UpdateMainAppImpl.__archiveDownloadFinished url[{url}], filePath[{filePath}], remoteFileSize[{remoteFileSize}], localFileSize[{localFileSize}] ')
        if DMHelper.STS.DOWNLOADED != status and remoteFileSize > localFileSize:
            sts, msg = self.checkForFreeSpace(self.tmpDir, remoteFileSize - localFileSize)
            if sts:
                msg = _(f"Problem with downloading the packet:\n[{url}].")
            self.stepFinished(-1, msg)
        else:
            self.stepFinished(0, _("Update packet was downloaded successfully."))
        return

    ##############################################################################
    # GET ARCHIVE STEP'S PRIVATES METHODS
    ##############################################################################
    def __encKeyDownloadFinished(self, arg, status):
        url = self.downloader.getUrl()
        filePath = self.downloader.getFullFileName()
        remoteFileSize = self.downloader.getRemoteFileSize()
        localFileSize = self.downloader.getLocalFileSize(True)
        self.downloader = None
        printDBG(f'UpdateMainAppImpl.__encKeyDownloadFinished url[{url}], filePath[{filePath}], remoteFileSize[{remoteFileSize}], localFileSize[{localFileSize}] ')
        if DMHelper.STS.DOWNLOADED != status and remoteFileSize > localFileSize:
            self.stepFinished(-1, _(f"Problem with downloading the encryption key:\n[{url}]."))
        elif localFileSize != 16:
            self.stepFinished(-1, _(f"Wrong the encryption key size: {localFileSize}\n"))
        else:
            try:
                from hashlib import md5, sha256

                from Plugins.Extensions.IPTVPlayer.libs.crypto.cipher.aes import \
                    AES
                from Plugins.Extensions.IPTVPlayer.libs.crypto.cipher.base import \
                    noPadding

                with open(filePath, "rb") as f:
                    data = f.read()

                userKey = sha256(self.password).digest()[:16]
                cipher = AES(userKey, keySize=len(userKey), padding=noPadding())
                data = cipher.decrypt(data)
                check = md5(data).hexdigest()[:16]
                if check != self.serversList[self.currServIdx]['enc']:
                    self.stepFinished(-1, _("Problem with decryption the key."))
                    return
                self.decKey = data
            except Exception:
                printExc()
                self.stepFinished(-1, _("Problem with decryption the key."))
                return

            self.stepFinished(0, _("Encryption key was downloaded successfully."))
        return

    ##############################################################################
    # UNPACK ARCHIVE STEP'S PRIVATES METHODS
    ##############################################################################
    def __unpackCmdFinished(self, status, outData):
        code = -1
        msg = ""
        self.cmd = None
        if 0 != status:
            code = -1
            msg = _(f"Problem with extracting the archive. Return code [{status}]\n{outData}.")
        else:
            try:
                os_remove
            except Exception:
                printExc()
            code = 0
            msg = _("Unpacking the archive completed successfully.")
        self.stepFinished(code, msg)

    ##############################################################################
    # DECRYPTION ARCHIVE STEP'S PRIVATES METHODS
    ##############################################################################
    def __decryptionCmdFinished(self, status, outData):
        if self.decKeyFilePath:
            rm(self.decKeyFilePath)
        code = -1
        msg = ""
        self.cmd = None
        if 0 != status:
            code = -1
            msg = _(f"Problem with decryption the archive. Return code [{status}]\n{outData}.")
        else:
            try:
                os_remove
            except Exception:
                printExc()
            code = 0
            msg = _("Decryption the archive completed successfully.")
        self.stepFinished(code, msg)

    ##############################################################################
    # REMOVE UNNECESSARY FILES STEP'S PRIVATES METHODS
    ##############################################################################
    def __removeUnnecessaryFilesCmdFinished(self, status, outData):
        code = -1
        msg = ""
        self.cmd = None
        if 0 != status:
            code = -1
            msg = _(f"Error. Return code [{status}]\n{outData}.")
        else:
            code = 0
            msg = _("Success.")
        self.stepFinished(code, msg)

    ##############################################################################
    # ExecuteUserScripts STEP'S PRIVATES METHODS
    ##############################################################################
    def __getUserCmdsList(self):
        printDBG('UpdateMainAppImpl.__getScriptsList begin')
        cmdList = []
        try:
            pathWithUserScripts = os_path.join(self.ExtensionPath, 'IPTVPlayer/iptvupdate/custom/')
            fileList = os_listdir(pathWithUserScripts)
            for wholeFileName in fileList:
                # separate file name and file extension
                fileName, fileExt = os_path.splitext(wholeFileName)
                filePath = os_path.join(pathWithUserScripts, wholeFileName)
                if os_path.isfile(filePath):
                    if fileExt in ['.pyo', '.pyc', '.py']:
                        interpreterBinName = 'python'
                    elif '.sh' == fileExt:
                        interpreterBinName = 'sh'
                    else:
                        continue
                    cmdList.append(f'{interpreterBinName} "{filePath}" "{os_path.join(self.ExtensionPath, "IPTVPlayer/")}" "{os_path.join(self.ExtensionTmpPath, "IPTVPlayer/")}" 2>&1 ')
            cmdList.sort()
        except Exception:
            printExc()
        printDBG(f'UpdateMainAppImpl.__getScriptsList [{cmdList}]')
        return cmdList

    def __copyBinariesCmdFinished(self, status, outData):
        self.cmd = None
        if 0 != status:
            code = -1
        else:
            code = 0
        msg = f'------------\nstatus[{status}]\n[{outData}]\n------------\n'
        self.stepCopyPostInatalledBinaries(init=False, code=code, msg=msg)

    def __userCmdFinished(self, status, outData):
        self.cmd = None
        if 0 != status:
            code = -1
        else:
            code = 0
        msg = f'------------\nstatus[{status}]\n[{outData}]\n------------\n'
        self.stepExecuteUserScripts(init=False, code=code, msg=msg)

    ##############################################################################
    # Instalation new version STEP'S PRIVATES METHODS
    ##############################################################################
    def __copyOldCmdFinished(self, status, outData):
        self.cmd = None
        if 0 != status:
            code = -1
            msg = self.messages['problem_copy'] % (status, outData)
        else:
            code = 0
            msg = self.messages['completed']
        self.stepFinished(code, msg)

    def __removeOldVersionCmdFinished(self, status, outData):
        self.cmd = None
        if 0 != status:
            code = -1
            msg = self.messages['problem_removal'] % (status, outData)
        else:
            code = 0
            msg = self.messages['completed']
        self.stepFinished(code, msg)

    def __installNewVersionCmdFinished(self, status, outData):
        self.cmd = None
        if 0 != status:
            msg = self.messages['problem_install'] % (status, outData)
            self.stepFinished(-1, msg)
        else:
            if self.localIconsHash != self.serverIconsHash:
                SetIconsHash(self.serverIconsHash)

            if self.localGraphicsHash != self.serverGraphicsHash:
                SetGraphicsHash(self.serverGraphicsHash)

            self.cmd = iptv_system(f'rm -rf {self.tmpDir} && sync', self.__doSyncCallBack)
        return

    def __doSyncCallBack(self, status, outData):
        self.cmd = None
        code, msg = self.checkVersionFile(self.PluginPath)
        self.stepFinished(code, msg)
