# -*- coding: utf-8 -*-


from Plugins.Extensions.IPTVPlayer.tools.iptvtools import byteify, printDBG

try:
    import json
except Exception:
    import simplejson as json

e2icjson = False


def loads(inputString, noneReplacement=None, baseTypesAsString=False, utf8=True):
    global e2icjson

    utf8 = False

    if e2icjson == None:
        try:
            import e2icjson  # p3 should have it installed in site-packages through opkg
            e2icjson = e2icjson
        except Exception:
            e2icjson = False

    if e2icjson:
        printDBG(f">> cjson ACELERATION noneReplacement[{noneReplacement}] baseTypesAsString[{baseTypesAsString}]")
        try:
            outDict = e2icjson.decode(inputString, 2 if utf8 else 1)
        except Exception as e:
            # TEMPORARY: SHOULD BE DEEPLY INVESTIGATED why cjson sometimes fails but json not
            printDBG(f">> cjson FAILED with EXCEPTION: {str(e)}")
            printDBG(f"\t Problematic inputString = '{inputString}'")
            printDBG(">> Trying with regular json module")
            outDict = json.loads(inputString)
    else:
        outDict = json.loads(inputString)

    if utf8 or noneReplacement != None or baseTypesAsString != False:
        return byteify(outDict, noneReplacement, baseTypesAsString)
    else:
        return outDict


def dumps(inputString, *args, **kwargs):
    return json.dumps(inputString, *args, **kwargs)
