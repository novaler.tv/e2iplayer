# -*- coding: utf-8 -*-
# Adopted from ResolveURL https://github.com/Gujal00/ResolveURL

import binascii
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler

UA = random_ua.get_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, '1cloud', 'OneCloud')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        oRequestHandler = cRequestHandler(self._url)
        oRequestHandler.addHeaderEntry('User-Agent', UA)
        oRequestHandler.addHeaderEntry('Origin', self._url.rsplit('/', 1)[0])
        oRequestHandler.addHeaderEntry('Referer', self._url)
        sHtmlContent = oRequestHandler.request()

        api_call = ''
        aResult = re.search(r'getNextDownloadPageLink\("([^"]+)', sHtmlContent)
        if aResult:
            api_call = ''.join([chr((x if isinstance(x, int) else ord(x)) ^ 117) for x in binascii.unhexlify(aResult.group(1))])

        if api_call:
            return True, f'{api_call}|User-Agent={UA}&Referer={self._url}'

        return False, False
