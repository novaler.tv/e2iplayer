# -*- coding: utf-8 -*-
# vStream https://github.com/Kodi-vStream/venom-xbmc-addons
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

# meme code que gorillavid


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'daclips', 'Daclips')

    def __getIdFromUrl(self):
        oParser = cParser()

        sPattern = 'http://daclips.in/embed-([^<]+)-'
        aResult = oParser.parse(self._url, sPattern)
        if aResult[0]:
            return aResult[1][0]
        return ''

    def _getMediaLinkForGuest(self, api_call=None):
        VSlog(self._url)
        oParser = cParser()

        url = f'http://daclips.in/{self.__getIdFromUrl()}'
        oRequest = cRequestHandler(url)
        sHtmlContent = oRequest.request()
        sPattern = '<input type="hidden" name="([^"]+)" value="([^"]+)"'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            oRequest.setRequestType(cRequestHandler.REQUEST_TYPE_POST)
            for aEntry in aResult[1]:
                oRequest.addParameters(aEntry[0], aEntry[1])
            oRequest.addParameters('referer', url)
            sHtmlContent = oRequest.request()
            r2 = re.search('file: "([^"]+)",', sHtmlContent)
            if r2:
                api_call = r2.group(1)

        if api_call:
            return True, api_call

        return False, False
