# -*- coding: utf-8 -*-

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser


class cHoster(iHoster):
    def __init__(self):
        iHoster.__init__(self, 'downloadgg', 'Download.gg')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        self._url = self._url.replace('/file-', '/iframe-').replace('/play-', '/iframe-')
        api_call = ''
        oParser = cParser()

        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        sPattern = '<video src="([^"]+)".*?data-title'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            api_call = aResult[1][0]

        if api_call:
            return True, api_call

        return False, False
