# -*- coding: utf-8 -*-
# Vstream https://github.com/Kodi-vStream/venom-xbmc-addons

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = random_ua.get_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'lien_direct', 'Direct Link', 'gold')

    def setUrl(self, url):
        self._url = str(url).replace('+', '%20')

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        api_call = self._url
        VSlog(self._url)

        SubTitle = ''
        if ('sub.info' in self._url):
            SubTitle = self._url.split('sub.info=')[1]
            self._url = self._url.replace('+', '%2B').split('?sub.info=')[0]

        api_call = self._url.replace("rrsrr", "cimanow").replace('rrsrrsn', 'newcima')

        if 'ffsff' in api_call:
            api_call = self._url.replace("ffsff", "moshahda")
            sReferer = self._url.split('|Referer=')[1]
            api_call += f'|User-Agent={UA}&Referer={sReferer}'

        if 'panet' in api_call:
            api_call += f'|AUTH=TLS&verifypeer=false'
        if 'scorarab' in api_call:
            api_call += f'|&User-Agent={UA}&Referer=https://live.scorarab.com/'
        if 'beintube' in api_call:
            api_call += f'|AUTH=TLS&verifypeer=false&Referer=https://bein-match.net/'
        if 'cimanow' in api_call:
            api_call += f'|AUTH=TLS&verifypeer=false&User-Agent={UA}&Referer=https://cimanow.cc'

        if 'hadara' in api_call:
            api_call += f'|AUTH=TLS&verifypeer=false&User-Agent={UA}'

        if '?src=' in api_call:
            api_call = api_call.split('?src=')[1]

        if '+' in api_call:
            api_call = api_call.replace("[", "%5B").replace("]", "%5D").replace("+", "%20")

        if 'goal4live.com' in api_call:
            api_call += f'|User-Agent={UA}'

        if 'fushaar' in api_call:
            api_call += f'|User-Agent={UA}&Referer={self._url}'

        if api_call:
            if ('http' in SubTitle):
                return True, api_call, SubTitle
            else:
                return True, api_call

        return False, False
