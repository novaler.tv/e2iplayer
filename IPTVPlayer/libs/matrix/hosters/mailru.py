# -*- coding: utf-8 -*-
# Adopted from ResolveURL

import json
import re

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import helpers
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'mailru', 'MailRu')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        media_id = self.get_host_and_id(self._url)

        location, user, media_id = media_id.split('|')
        if user == 'None':
            web_url = f'http://my.mail.ru/+/video/meta/{media_id}'
        else:
            if media_id is not False:
                location, user, media_id = media_id.split('|')
                if user == 'None':
                    web_url = f'http://my.mail.ru/+/video/meta/{media_id}'
                else:
                    web_url = f'http://my.mail.ru/+/video/meta/{location}/{user}/{media_id}?ver=0.2.60'

                s = requests.session()
                response = s.get(web_url)
                html = response.content

                if html:
                    js_data = json.loads(html)
                    sources = [(video['key'], video['url']) for video in js_data['videos']]
                    sorted(sources)
                    source = helpers.pick_source(sources)

                    if source.startswith("//"):
                        source = f'http:{source}'

                    return True, source + helpers.append_headers({'Cookie': response.headers.get('Set-Cookie', '')})
            else:
                s = requests.session()
                response = s.get(self._url)
                html = response.text
                match = re.findall(r'"weblink_get":.*?,"url":"(https?://[^\s"]+/public/[^"]+)"', html)
                if match:
                    stream_url = match[0] + self._url.split("public")[1]
                    return True, stream_url

            return False, False

    def get_host_and_id(self, url):
        pattern = r'(?://|\.)(mail\.ru)/(?:\w+/)?(?:videos/embed/)?(inbox|mail|embed|mailua|list|bk|v)/(?:([^/]+)/[^.]+/)?(\d+)'
        r = re.search(pattern, url)
        if r:
            return (f'{r.group(2)}|{r.group(3)}|{r.group(4)}')
        else:
            return False
