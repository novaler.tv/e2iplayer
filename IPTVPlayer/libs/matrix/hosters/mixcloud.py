# -*- coding: utf-8 -*-
# Vstream https://github.com/Kodi-vStream/venom-xbmc-addons
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'mixcloud', 'Mixcloud')

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)
        api_call = False

        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        sPattern = 'https://audiocdn.+?mixcloud.com/previews/(.+?).mp3'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            api_call = f'https://audio4.mixcloud.com/secure/hls/{aResult[1][0]}.m4a/index.m3u8'

        if api_call:
            return True, api_call

        return False, False
