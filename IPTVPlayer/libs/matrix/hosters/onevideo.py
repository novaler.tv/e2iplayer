# -*- coding: utf-8 -*-
# vStream https://github.com/Kodi-vStream/venom-xbmc-addons
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.util import Unquote


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'onevideo', 'Onevideo')

    def __getIdFromUrl(self):
        sPattern = "id=([^<]+)"
        oParser = cParser()
        aResult = oParser.parse(self._url, sPattern)
        if aResult[0]:
            return aResult[1][0]

        return ''

    def __getKey(self):
        oRequestHandler = cRequestHandler(self._url)
        sHtmlContent = oRequestHandler.request()
        sPattern = 'key: "(.+?)";'
        oParser = cParser()
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            aResult = aResult[1][0].replace('.', '%2E')
            return aResult
        return ''

    def setUrl(self, url):
        self._url = str(url)
        self._url = self._url.replace('http://www.onevideo.to/', '')
        self._url = self._url.replace('embed.php?id=', '')
        self._url = f'http://www.onevideo.to/embed.php?id={str(self._url)}'

    def _getMediaLinkForGuest(self):
        VSlog(self._url)
        api_call = (f'http://www.onevideo.to/api/player.api.php?user=undefined&codes=1&file={self.__getIdFromUrl()}&pass=undefined&key={self.__getKey()}')

        oRequest = cRequestHandler(api_call)
        sHtmlContent = oRequest.request()

        sPattern = 'url=(.+?)&title'
        oParser = cParser()
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            stream_url = Unquote(aResult[1][0])
            return True, stream_url
        else:
            return False, False

        return False, False
