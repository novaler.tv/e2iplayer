﻿# -*- coding: utf-8 -*-

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = random_ua.get_pc_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'shoffree', 'Shoffree/EgyBest', 'gold')

    def isDownloadable(self):
        return False

    def setUrl(self, url):
        self._url = str(url)

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)
        sReferer = ""
        url = self._url
        if '|Referer=' in self._url:
            url = self._url.split('|Referer=')[0]
            sReferer = self._url.split('|Referer=')[1]
        sHost = self._url.rsplit("/")[2]
        api_call = False

        oRequest = cRequestHandler(url)
        oRequest.addHeaderEntry('user-agent', UA)
        oRequest.addHeaderEntry('Referer', sReferer.encode('utf-8'))
        oRequest.addHeaderEntry('Host', sHost.encode('utf-8'))
        sHtmlContent = oRequest.request()

        sPattern = '(eval\(function\(p,a,c,k,e(?:.|\s)+?\))<\/script>'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            sHtmlContent = cPacker().unpack(aResult[1][0])

            sPattern = 'file:"(.+?)"'
            aResult = oParser.parse(sHtmlContent, sPattern)
            if aResult[0]:
                api_call = aResult[1][0]

                if api_call:
                    return True, f'{api_call.replace(" ", "%20")}|User-Agent={UA}&Referer=https://{sHost}/'
        url = []
        qua = []
        sPattern = 'file:\s*"([^"]+)",\s*label:\s*"([^"]+)'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0] is True:
            for aEntry in aResult[1]:

                url.append(aEntry[0])
                qua.append(aEntry[1])
            if url:
                api_call = dialog().VSselectqual(qua, url)

        if api_call:
            return True, api_call.replace(' ', '%20')

        sPattern = '<source src="(.+?)" type='
        aResult = oParser.parse(sHtmlContent, sPattern)

        api_call = False

        if aResult[0]:
            api_call = aResult[1][0]

            if api_call:
                return True, f'{api_call}|User-Agent={UA}&Referer={sReferer}'

        sPattern = 'file:.+?"(.+?)",.+?label: "(.+?)",'
        aResult = oParser.parse(sHtmlContent, sPattern)

        api_call = False

        if aResult[0]:

            # initialisation des tableaux
            url = []
            qua = []

            # Replissage des tableaux
            for i in aResult[1]:
                url.append(str(i[0]))
                qua.append(str(i[1]))
            api_call = dialog().VSselectqual(qua, url)

            if api_call:
                return True, f'{api_call}|User-Agent={UA}&Referer={sReferer}'

        return False, False
