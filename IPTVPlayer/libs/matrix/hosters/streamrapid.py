# -*- coding: utf-8 -*-
# https://github.com/Kodi-vStream/venom-xbmc-addons
# Plugin from ResolveURL
# Yonn1981 https://github.com/Yonn1981/Repo
#

import base64
import json
import re

import requests
from Plugins.Extensions.IPTVPlayer.compat import urljoin
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser
from resolveurl.lib.pyaes import openssl_aes

AU = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'streamrapid', 'Rabbitstream/Dokicloud')

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)
        mainurl = self._url

        referer = urljoin(self._url, '/')
        headers = {
            'User-Agent': AU,
            'Referer': referer}

        s = requests.Session()

        headers = {
            'User-Agent': AU,
            'Referer': referer}
        r = s.get(self._url, headers=headers)
        sHtmlContent = r.content.decode('utf8')
        oParser = cParser()

        sPattern = '<script type="text/javascript" src="(.+?)"></script>'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            surl = aResult[1][0]
            surl = referer + surl

        host = mainurl.split("/e")[0]
        host = host.split("//")[1]
        mid = mainurl.split('?')[0]
        mid = mid.split("embed-4")[1]
        mid = mid.replace('/', '/getSources?id=')
        aurl = f'https://{host}/ajax/embed-4{mid}'

        headers = {
            'User-Agent': AU,
            'Referer': referer, 'X-Requested-With': 'XMLHttpRequest'}
        r = s.get(aurl, headers=headers)
        sHtmlContent = r.content.decode('utf8')
        oParser = cParser()

        sPattern = '"sources":"([^"]+)"'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            sources = aResult[1][0]

            response = requests.get('https://keys4.fun')
            keys = response.json()["rabbitstream"]["keys"]

            key_bytes = bytes(keys)
            key_string = base64.b64encode(key_bytes).decode('utf-8')

            strippedSources = sources

            OpenSSL_AES = openssl_aes.AESCipher()
            sources = json.loads(OpenSSL_AES.decrypt(strippedSources, key_string))

            sPattern = "'file': '(.+?)',"
            aResult = oParser.parse(sources, sPattern)
            if aResult[0]:
                source = aResult[1][0]

        sPattern = '{"file":"([^"]+)'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            source = aResult[1][0]

        url_stream = source

        SubTitle = ''
        sPattern = '"file":"([^"]+)".+?"label":"(.+?)"'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            url = []
            qua = []
            for i in aResult[1]:
                url.append(str(i[0]))
                qua.append(str(i[1]))
            SubTitle = dialog().VSselectsub(qua, url)

        if url_stream:
            if ('http' in SubTitle):
                return True, url_stream, SubTitle
            else:
                return True, url_stream

        return False, False


def extractKey(script):
    startOfSwitch = script.rfind("switch")
    endOfCases = script.find("partKeyStartPosition")
    switchBody = script[startOfSwitch:endOfCases]
    nums = []
    matches = re.findall(r":[a-zA-Z0-9]+=([a-zA-Z0-9]+),[a-zA-Z0-9]+=([a-zA-Z0-9]+);", switchBody)
    for match in matches:
        innerNumbers = []
        for varMatch in match:
            regex = re.compile("{}=0x([a-zA-Z0-9]+)".format(varMatch))
            varMatches = re.findall(regex, script)
            lastMatch = varMatches[-1]
            if not lastMatch:
                return None
            number = int(lastMatch, 16)
            innerNumbers.append(number)
        nums.append([innerNumbers[0], innerNumbers[1]])
    return nums
