﻿# -*- coding: utf-8 -*-
# Vstream https://github.com/Kodi-vStream/venom-xbmc-addons
# https://www.vidbm.com/emb.html?xxx=img.vidbm.com/xxx
# https://www.vidbm.com/embed-xxx.html?auto=1

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = random_ua.get_pc_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vidshare', 'Vidshare')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        oParser = cParser()
        sReferer = self._url

        oRequest = cRequestHandler(self._url)
        oRequest.addHeaderEntry('user-agent', UA)
        oRequest.addHeaderEntry('Referer', sReferer)
        oRequest.addHeaderEntry('x-requested-with', 'XMLHttpRequest')
        oRequest.addHeaderEntry('accept', '*/*')
        sHtmlContent = oRequest.request()

        sPattern = '(eval\(function\(p,a,c,k,e(?:.|\s)+?\))<\/script>'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if (aResult[0] == True):
            sHtmlContent = cPacker().unpack(aResult[1][0])
            sPattern = 'file:"(.+?)",label:".+?"}'
            aResult = oParser.parse(sHtmlContent, sPattern)
            if (aResult[0] == True):
                api_call = aResult[1][0]

        sPattern = 'file:"(.+?)"}'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if (aResult[0] == True):
            api_call = f'{aResult[1][0]}|User-Agent={UA}&Referer={sReferer}'

        if (api_call):
            return True, api_call

        return False, False
