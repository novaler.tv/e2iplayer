﻿
import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vidtube', 'VidTube')

    def isDownloadable(self):
        return True

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)

        sReferer = ''
        if '/d/' in self._url or 'embed-' in self._url:
            self._url = self._url.replace('/d/', '/embed-')
            if '.html' not in self._url:
                self._url += '.html'
        if '|Referer=' in self._url:
            sReferer = self._url.split('|Referer=')[1]
            self._url = self._url.split('|Referer=')[0]

        VSlog(self._url)
        api_call = ''

        headers = {
            'User-Agent': UA,
            'Referer': sReferer}
        s = requests.session()
        sHtmlContent = s.get(self._url, headers=headers).text

        sPattern = "(\s*eval\s*\(\s*function(?:.|\s)+?)<\/script>"
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            sHtmlContent = cPacker().unpack(aResult[1][0])

        sPattern = 'file:"([^"]+)".+?label:"([^"]+)"'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            url = []
            qua = []
            for i in aResult[1]:
                url.append(str(i[0]))
                qua.append(str(i[1]))

            api_call = f'{dialog().VSselectqual(qua, url)}|User-Agent={UA}&Referer={self._url}&Origin={self._url.rsplit("/", 1)[0]}'

        sPattern = 'sources:*\[{file:"([^"]+)"'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            for aEntry in aResult[1]:
                api_call = f'{aEntry}|User-Agent={UA}&Referer={self._url}&Origin={self._url.rsplit("/", 1)[0]}'

        if api_call:
            return True, api_call

        return False, False
