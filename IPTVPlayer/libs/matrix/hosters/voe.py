﻿# coding: utf-8

import base64
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

try:
    import json
except Exception:
    import simplejson as json

UA = random_ua.get_random_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'voe', 'Voe')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)
        oParser = cParser()
        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        api_call = ''

        r = re.search(r"let\s*(?:wc0|[0-9a-f]+)\s*=\s*'([^']+)", sHtmlContent)
        if r:
            r = json.loads(base64.b64decode(r.group(1))[::-1].decode('utf8', errors='ignore'))
            url = r.get('file')
            return True, f'{url}|User-Agent={UA}&Referer={self._url}&Origin={self._url.rsplit("/", 2)[0]}'

        sPattern = '["\']hls["\']:\s*["\']([^"\']+)["\']'
        aResult = oParser.parse(sHtmlContent, sPattern)
        aResult1 = base64.b64decode(aResult[1][0])
        if aResult[0]:
            api_call = aResult1.decode("utf-8")

        if api_call:
            return True, api_call

        return False, False
