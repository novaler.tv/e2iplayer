# -*- coding: utf-8 -*-
# Vstream https://github.com/Kodi-vStream/venom-xbmc-addons
# test sur http://vshare.eu/embed-wuqinr62cpn6-703x405.html
#         http://vshare.eu/embed-cxmr4o8l2waa-703x405.html
#         http://vshare.eu/embed-cxmr4o8l2waa703x405.html erreur code streambb
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vshare', 'Vshare')

    def isDownloadable(self):
        return False

    def setUrl(self, url):
        self._url = str(url)
        self._url = re.sub('-*\d{3,4}x\d{3,4}', '', self._url)
        self._url = self._url.replace('https', 'http')

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)
        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        if '<div id="deleted">' in sHtmlContent:
            return False, False

        sPattern = '<source src="([^"]+)"'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            api_call = aResult[1][0]
        else:
            sPattern = '(eval\(function\(p,a,c,k,e(?:.|\s)+?\))<\/script>'
            aResult = oParser.parse(sHtmlContent, sPattern)
            if aResult[0]:
                sHtmlContent = cPacker().unpack(aResult[1][0])

                sPattern = '{file:"(http.+?vid.mp4)"'
                aResult = oParser.parse(sHtmlContent, sPattern)
                if aResult[0]:
                    api_call = aResult[1][0]

        if api_call:
            return True, api_call

        return False, False
