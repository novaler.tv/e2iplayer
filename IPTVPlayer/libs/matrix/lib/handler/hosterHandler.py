# -*- coding: utf-8 -*-
# vStream https://github.com/Kodi-vStream/venom-xbmc-addons
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser


class cHosterHandler:

    def getUrl(self, oHoster):
        sUrl = oHoster.getUrl()
        VSlog(f"hosterhandler {sUrl}")
        oRequest = cRequestHandler(sUrl)
        sContent = oRequest.request()

        aMediaLink = cParser().parse(sContent, oHoster.getPattern())
        if (aMediaLink[0] == True):
            return True, aMediaLink[1][0]
        return False, ''

    def getHoster(self, sHosterFileName):
        exec(f"from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.{sHosterFileName} import cHoster")

        return cHoster()
