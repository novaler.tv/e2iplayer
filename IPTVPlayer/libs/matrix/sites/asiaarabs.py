﻿# -*- coding: utf-8 -*-
# zombi https://github.com/zombiB/zombi-addons/

import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (
    addon, siteManager)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.gui import cGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.hoster import cHosterGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.inputParameterHandler import \
    cInputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.outputParameterHandler import \
    cOutputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.util import cUtil

SITE_IDENTIFIER = 'asiaarabs'
SITE_NAME = 'Asia4arabs'
SITE_DESC = 'arabic vod'

URL_MAIN = siteManager().getUrlMain(SITE_IDENTIFIER)

MOVIE_ASIAN = (f'{URL_MAIN}search/label/%D8%A3%D9%81%D9%84%D8%A7%D9%85', 'showMovies')
SERIE_ASIA = (f'{URL_MAIN}search/label/%D9%85%D8%B3%D9%84%D8%B3%D9%84%D8%A7%D8%AA', 'showSeries')


URL_SEARCH = (f'{URL_MAIN}search?q=', 'showMovies')
URL_SEARCH_SERIES = (f'{URL_MAIN}search?q=', 'showSeries')
URL_SEARCH_MOVIES = (f'{URL_MAIN}search?q=', 'showMovies')
FUNCTION_SEARCH = 'showMovies'


def load():
    oGui = cGui()
    addons = addon()
    oOutputParameterHandler = cOutputParameterHandler()

    oGui.addText(SITE_IDENTIFIER, f'[COLOR olive]-----●★| {addons.VSlang(30076)} |★●-----[/COLOR]')

    oGui.addDir(SITE_IDENTIFIER, 'showSearch', addons.VSlang(30078), 'search.png', oOutputParameterHandler)

    oGui.addDir(SITE_IDENTIFIER, 'showSearchSeries', addons.VSlang(30079), 'search.png', oOutputParameterHandler)

    oGui.addText(SITE_IDENTIFIER, f'[COLOR olive]-----●★| {addons.VSlang(30120)} |★●-----[/COLOR]')

    oOutputParameterHandler.addParameter('siteUrl', MOVIE_ASIAN[0])
    oGui.addDir(SITE_IDENTIFIER, 'showMovies', 'أفلام', 'film.png', oOutputParameterHandler)

    oGui.addText(SITE_IDENTIFIER, f'[COLOR olive]-----●★| {addons.VSlang(30121)} |★●-----[/COLOR]')

    oOutputParameterHandler.addParameter('siteUrl', SERIE_ASIA[0])
    oGui.addDir(SITE_IDENTIFIER, 'showSeries', 'مسلسلات', 'mslsl.png', oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showSearch():
    oGui = cGui()

    sSearchText = oGui.showKeyBoard()
    if sSearchText:
        sUrl = f'{URL_MAIN}search?q={sSearchText}'
        showMovies(sUrl)
        oGui.setEndOfDirectory()
        return


def showSearchSeries():
    oGui = cGui()

    sSearchText = oGui.showKeyBoard()
    if sSearchText:
        sUrl = f'{URL_MAIN}search?q={sSearchText}'
        showSeries(sUrl)
        oGui.setEndOfDirectory()
        return


def showMovies(sSearch=''):
    oGui = cGui()
    oParser = cParser()

    if sSearch:
        sUrl = sSearch
    else:
        oInputParameterHandler = cInputParameterHandler()
        sUrl = oInputParameterHandler.getValue('siteUrl')

    sUrl = sUrl.replace("+", "%2B")
    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sPattern = "<a class='Img-Holder thumb' href='([^<]+)' title='([^<]+)'>.+?rel='tag'>(.+?)</span>.+?class='post-thumb' data-src='([^<]+)' height"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:
            if "مترجم" not in aEntry[1]:
                continue
            if "مسلسل" in aEntry[1]:
                continue

            sTitle = cUtil().CleanMovieName(aEntry[1])
            siteUrl = aEntry[0]
            sThumb = aEntry[3]
            sDesc = aEntry[2]
            sYear = ''
            m = re.search('([0-9]{4})', sTitle)
            if m:
                sYear = str(m.group(0))

            oOutputParameterHandler.addParameter('siteUrl', siteUrl)
            oOutputParameterHandler.addParameter('sMovieTitle', sTitle)
            oOutputParameterHandler.addParameter('sThumb', sThumb)
            oOutputParameterHandler.addParameter('sYear', sYear)

            oGui.addMovie(SITE_IDENTIFIER, 'showHosters', sTitle, '', sThumb, sDesc, oOutputParameterHandler)

    sPattern = "href='([^<]+)' id='.+?' title='(.+?)'>"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:
            if "youtube" in aEntry[1]:
                continue

            sTitle = aEntry[1]

            sTitle = f"PAGE {sTitle}"
            sTitle = f'[COLOR red]{sTitle}[/COLOR]'
            siteUrl = aEntry[0]

            oOutputParameterHandler.addParameter('siteUrl', siteUrl)

            oGui.addDir(SITE_IDENTIFIER, 'showMovies', sTitle, 'next.png', oOutputParameterHandler)

    if not sSearch:
        oGui.setEndOfDirectory()


def showSeries(sSearch=''):
    oGui = cGui()
    oParser = cParser()

    if sSearch:
        sUrl = sSearch
    else:
        oInputParameterHandler = cInputParameterHandler()
        sUrl = oInputParameterHandler.getValue('siteUrl')

    sUrl = sUrl.replace("+", "%2B")
    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sPattern = "<a class='Img-Holder thumb' href='([^<]+)' title='([^<]+)'>.+?rel='tag'>(.+?)</span>.+?class='post-thumb' data-src='([^<]+)' height"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:
            if "مترجم" not in aEntry[1]:
                continue
            if "فيلم" in aEntry[1]:
                continue

            sTitle = cUtil().CleanSeriesName(aEntry[1])
            siteUrl = aEntry[0]
            sThumb = aEntry[3]
            sDesc = aEntry[2]

            oOutputParameterHandler.addParameter('siteUrl', siteUrl)
            oOutputParameterHandler.addParameter('sMovieTitle', sTitle)
            oOutputParameterHandler.addParameter('sThumb', sThumb)

            oGui.addTV(SITE_IDENTIFIER, 'showEpisodes', sTitle, '', sThumb, sDesc, oOutputParameterHandler)

    sPattern = "href='([^<]+)' id='.+?' title='(.+?)'>"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:
            if "youtube" in aEntry[1]:
                continue

            sTitle = aEntry[1]

            sTitle = f"PAGE {sTitle}"
            sTitle = f'[COLOR red]{sTitle}[/COLOR]'
            siteUrl = aEntry[0]

            oOutputParameterHandler.addParameter('siteUrl', siteUrl)

            oGui.addDir(SITE_IDENTIFIER, 'showSeries', sTitle, 'next.png', oOutputParameterHandler)

    if not sSearch:
        oGui.setEndOfDirectory()


def showEpisodes():
    oGui = cGui()
    oParser = cParser()
    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')
    sMovieTitle = oInputParameterHandler.getValue('sMovieTitle')
    sThumb = oInputParameterHandler.getValue('sThumb')

    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sPattern = '<a href="(https://asia4arabs-fs.+?)"'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        m3url = aResult[1][0]
        oRequest = cRequestHandler(m3url)
        sHtmlContent = oRequest.request()
    sPattern = '<a href="(https://www.asia4arabs.co.+?)" target'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        m3url = aResult[1][0]
        oRequest = cRequestHandler(m3url)
        sHtmlContent = oRequest.request()

    sPattern = '<a class="button" href="([^<]+)" id=".+?">(.+?)</a>'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = aEntry[1]
            sTitle = aEntry[0]
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                continue
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sTitle = sTitle + sMovieTitle
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = 'iframes([^<]+)=.+?width="100%" height="400" src="(.+?)" frameborder='
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = aEntry[1]
            sTitle = 'E' + aEntry[0].replace(" ", "").replace("]", "").replace("[", "").replace(" = {};", "").replace(" iframes", "").replace("iframes", "").replace("={};", "")
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sTitle = sTitle + sMovieTitle
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = '<a href="([^<]+)" target="_blank">(.+?)</a>'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = aEntry[0]
            sTitle = aEntry[1].replace("الحلقة ", " E")
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sTitle = sTitle + sMovieTitle
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = '<td><a href=["\']([^"\']+)["\']\s*target="iframe_a">(.+?)</a>'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = aEntry[0]
            sTitle = aEntry[1].replace("Episode ", " E").replace("Episod ", " E")
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                continue
            if "google" in sHosterUrl:
                continue
            if "LINK0" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'moshahda' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sTitle = sTitle + sMovieTitle
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = '>الحلقة([^<]+)</span></span></h4><iframe allowfullscreen.+?src="(.+?)" width'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:
            url = aEntry[1]
            sTitle = "E" + aEntry[0].replace(" ", "").replace("]", "").replace("[", "").replace(" = {};", "").replace(" iframes", "").replace("iframes", "").replace("={};", "")
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sTitle = sTitle + sMovieTitle
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    oGui.setEndOfDirectory()


def __checkForNextPage(sHtmlContent):
    oParser = cParser()

    sPattern = "href='([^<]+)' id='.+?' title='NextUrl'>"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        return aResult[1][0]

    return False


def showHosters():
    oGui = cGui()
    oParser = cParser()
    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')
    sMovieTitle = oInputParameterHandler.getValue('sMovieTitle')
    sThumb = oInputParameterHandler.getValue('sThumb')

    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sPattern = '<iframe.+?src="([^"]+)'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = str(aEntry)
            sTitle = " "
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                sTitle = "-trailer"

            if "blogger" in sHosterUrl:
                continue
            if ".jpg" in sHosterUrl:
                continue
            if ".jpeg" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sDisplayTitle = sMovieTitle + sTitle
                oHoster.setDisplayName(sDisplayTitle)
                oHoster.setFileName(sMovieTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = '<a href="([^<]+)" target="'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = str(aEntry)
            sTitle = " "
            if url.startswith('//'):
                url = f'http:{url}'

            sHosterUrl = url
            if "youtube" in sHosterUrl:
                sTitle = "-trailer"

            if "blogger" in sHosterUrl:
                continue
            if ".jpg" in sHosterUrl:
                continue
            if ".jpeg" in sHosterUrl:
                continue
            if 'userload' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            if 'mystream' in sHosterUrl:
                sHosterUrl += f"|Referer={URL_MAIN}"
            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                sDisplayTitle = sMovieTitle + sTitle
                oHoster.setDisplayName(sDisplayTitle)
                oHoster.setFileName(sMovieTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    oGui.setEndOfDirectory()
