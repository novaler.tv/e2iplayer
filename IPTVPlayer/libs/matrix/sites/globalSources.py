# -*- coding: utf-8 -*-
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import addon
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.gui import cGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.outputParameterHandler import \
    cOutputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.pluginHandler import \
    cPluginHandler


def globalSources():
    oGui = cGui()
    oPluginHandler = cPluginHandler()
    aPlugins = oPluginHandler.getAvailablePlugins(force=True)

    if len(aPlugins) == 0:
        addons = addon()
        addons.openSettings()
        oGui.updateDirectory()
    else:
        for aPlugin in aPlugins:
            oOutputParameterHandler = cOutputParameterHandler()
            oOutputParameterHandler.addParameter('siteUrl', 'http://venom')
            icon = f'sites/{aPlugin[1]}.png'
            oGui.addDir(aPlugin[1], 'load', aPlugin[0], icon, oOutputParameterHandler)
    oGui.setEndOfDirectory()
    return
