﻿# -*- coding: utf-8 -*-
# Yonn1981 https://github.com/Yonn1981/Repo

import base64
import json
import time

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (addon,
                                                                    isMatrix)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.gui import cGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.hoster import cHosterGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.inputParameterHandler import \
    cInputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.outputParameterHandler import \
    cOutputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.tmdb import cTMDb

UA = random_ua.get_ua()

SITE_IDENTIFIER = 'tma'
SITE_NAME = 'The Movie Archive'
SITE_DESC = 'english vod'

URL_MAIN = 'https://www.themoviedb.org/'

API_VERS = '3'
API_URL = URL_MAIN + API_VERS

tmdb_session = ''
tmdb_account = ''

MOVIE_EN = ('movie/now_playing', 'showMovies')
MOVIE_TOP = ('movie/top_rated', 'showMovies')
MOVIE_POP = ('movie/popular', 'showMovies')
MOVIE_GENRES = ('genre/movie/list', 'showGenreMovie')

URL_SEARCH_MOVIES = ('https://api.themoviedb.org/3/search/movie?include_adult=false&query=', 'showMoviesSearch')
FUNCTION_SEARCH = 'showMovies'


def load():
    oGui = cGui()
    addons = addon()
    oOutputParameterHandler = cOutputParameterHandler()

    oGui.addText(SITE_IDENTIFIER, f'[COLOR olive]-----●★| {addons.VSlang(30076)} |★●-----[/COLOR]')

    oOutputParameterHandler.addParameter('siteUrl', 'http://')
    oGui.addDir(SITE_IDENTIFIER, 'showSearchMovie', addons.VSlang(30330), 'search.png', oOutputParameterHandler)

    oGui.addText(SITE_IDENTIFIER, f'[COLOR olive]-----●★| {addons.VSlang(30120)} |★●-----[/COLOR]')

    oOutputParameterHandler.addParameter('siteUrl', MOVIE_POP[0])
    oGui.addDir(SITE_IDENTIFIER, 'showMovies', addons.VSlang(30425), 'pop.png', oOutputParameterHandler)

    oOutputParameterHandler.addParameter('siteUrl', MOVIE_EN[0])
    oGui.addDir(SITE_IDENTIFIER, 'showMovies', addons.VSlang(30426), 'agnab.png', oOutputParameterHandler)

    oOutputParameterHandler.addParameter('siteUrl', MOVIE_TOP[0])
    oGui.addDir(SITE_IDENTIFIER, 'showMovies', addons.VSlang(30427), 'top.png', oOutputParameterHandler)

    oOutputParameterHandler.addParameter('siteUrl', MOVIE_GENRES[0])
    oGui.addDir(SITE_IDENTIFIER, 'showGenreMovie', addons.VSlang(30428), 'genres.png', oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showSearchMovie():
    oGui = cGui()

    sSearchText = oGui.showKeyBoard()
    if sSearchText:
        showMovies(sSearchText.replace(' ', '+'))
        return


def showGenreMovie():
    oGui = cGui()
    grab = cTMDb()

    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')

    result = grab.getUrl(sUrl)
    oOutputParameterHandler = cOutputParameterHandler()
    for i in result['genres']:
        sId, sTitle = i['id'], i['name']

        if not isMatrix():
            sTitle = sTitle.encode("utf-8")
        sUrl = f'genre/{str(sId)}/movies'
        oOutputParameterHandler.addParameter('siteUrl', sUrl)
        oGui.addDir(SITE_IDENTIFIER, 'showMovies', str(sTitle), 'genres.png', oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showFolderList():
    oGui = cGui()

    liste = []
    liste.append(['Top 50 Greatest Movies', '10'])
    liste.append(['Oscar winners', '31670'])
    liste.append(['Fascinating movies ', '43'])
    liste.append(['Science-Fiction', '3945'])
    liste.append(['Adaptations', '9883'])
    liste.append(['Disney Classic', '338'])
    liste.append(['Pixar', '3700'])
    liste.append(['Marvel', '1'])
    liste.append(['DC Comics Universe', '3'])
    liste.append(['Top Manga', '31665'])
    liste.append(['Top Manga 2', '31695'])
    liste.append(['Best Series', '36788'])

    oOutputParameterHandler = cOutputParameterHandler()
    for sTitle, sUrl in liste:
        oOutputParameterHandler.addParameter('siteUrl', sUrl)
        oGui.addDir(SITE_IDENTIFIER, 'showLists', sTitle, 'listes.png', oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showMoviesSearch(sSearch=''):
    oGui = cGui()
    addons = addon()
    API_Key = addons.getSetting('api_tmdb')

    sUrl = f'{sSearch}&api_key={API_Key}'
    simdb_id = ''
    data = requests.get(sUrl)
    sHtmlContent = data.text

    sPattern = '"id":(.+?),.+?"original_title":"([^"]+)".+?"overview":"([^"]+)".+?"poster_path":(.+?),'

    oParser = cParser()
    aResult = oParser.parse(sHtmlContent, sPattern)

    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:
            if 'null' in aEntry[3] or 'none' in aEntry[3]:
                continue

            sId = aEntry[0]
            sTitle = aEntry[1]
            sDisplayTitle = sTitle.replace(' ', '%2520').replace('%20', '%2520')
            siteUrl = base64.b64decode('aHR0cHM6Ly9hcGkuYnJhZmxpeC52aWRlby9mZWJib3gvc291cmNlcy13aXRoLXRpdGxlPw==').decode('utf8', errors='ignore')
            siteUrl = f'{siteUrl}title={sDisplayTitle}&mediaType=movie&episodeId=1&seasonId=1&tmdbId={sId}&imdbId={simdb_id}'
            sThumb = "https://image.tmdb.org/t/p/w500{}".format(aEntry[3].replace('"', ''))
            sDesc = aEntry[2]

            oOutputParameterHandler = cOutputParameterHandler()
            oOutputParameterHandler.addParameter('siteUrl', siteUrl)
            oOutputParameterHandler.addParameter('sMovieTitle', sTitle)
            oOutputParameterHandler.addParameter('sThumb', sThumb)
            oOutputParameterHandler.addParameter('sId', sId)
            oGui.addMovie(SITE_IDENTIFIER, 'showHosters', sTitle, '', sThumb, sDesc, oOutputParameterHandler)
    if not sSearch:
        oGui.setEndOfDirectory()


def showMovies(sSearch=''):
    oGui = cGui()
    grab = cTMDb()

    oInputParameterHandler = cInputParameterHandler()

    iPage = 1
    term = ''
    if oInputParameterHandler.exist('page'):
        iPage = oInputParameterHandler.getValue('page')

    if oInputParameterHandler.exist('sSearch'):
        sSearch = oInputParameterHandler.getValue('sSearch')

    if sSearch:
        result = grab.getUrl('search/movie', iPage, f'query={sSearch}')
        sUrl = ''

    else:
        if oInputParameterHandler.exist('session_id'):
            term += f"session_id={oInputParameterHandler.getValue('session_id')}"

        sUrl = oInputParameterHandler.getValue('siteUrl')
        result = grab.getUrl(sUrl, iPage, term)

    try:

        for i in result['results']:
            i = grab._format(i, '', "movie")

            sId, sTitle, simdb_id, sThumb, sDesc, sYear = i['tmdb_id'], i['title'], i['imdb_id'], i['poster_path'], i['plot'], i['year']

            if not isMatrix():
                sTitle = sTitle.encode("utf-8")
            sDisplayTitle = sTitle.replace(' ', '%2520').replace('%20', '%2520')
            siteUrl = base64.b64decode('aHR0cHM6Ly9hcGkuYnJhZmxpeC52aWRlby9mZWJib3gvc291cmNlcy13aXRoLXRpdGxlPw==').decode('utf8', errors='ignore')
            siteUrl = f'{siteUrl}title={sDisplayTitle}&year={sYear}&mediaType=movie&episodeId=1&seasonId=1&tmdbId={sId}&imdbId={simdb_id}'
            oOutputParameterHandler = cOutputParameterHandler()
            oOutputParameterHandler.addParameter('siteUrl', siteUrl)
            oOutputParameterHandler.addParameter('sMovieTitle', sTitle)
            oOutputParameterHandler.addParameter('sThumb', sThumb)
            oOutputParameterHandler.addParameter('sId', sId)
            oGui.addMovie(SITE_IDENTIFIER, 'showHosters', sTitle, '', sThumb, sDesc, oOutputParameterHandler)

        if int(iPage) > 0:
            iNextPage = int(iPage) + 1
            oOutputParameterHandler = cOutputParameterHandler()
            if sSearch:
                oOutputParameterHandler.addParameter('sSearch', sSearch)

            oOutputParameterHandler.addParameter('siteUrl', sUrl)
            oOutputParameterHandler.addParameter('page', iNextPage)
            oGui.addNext(SITE_IDENTIFIER, 'showMovies', f'Page {str(iNextPage)}', oOutputParameterHandler)

    except TypeError as e:
        oGui.addText(SITE_IDENTIFIER, '[COLOR red]No result n\'was found.[/COLOR]')

    oGui.setEndOfDirectory()


def showHosters():
    oGui = cGui()

    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')
    sMovieTitle = oInputParameterHandler.getValue('sMovieTitle')
    sThumb = oInputParameterHandler.getValue('sThumb')
    sId = oInputParameterHandler.getValue('sId')

    simdb_id = sUrl.split('imdbId=')[1]
    if simdb_id == '':
        addons = addon()
        API_Key = addons.getSetting('api_tmdb')
        sApi = f'https://api.themoviedb.org/3/movie/{sId}?api_key={API_Key}'
        sResponse = requests.request("GET", sApi, headers=None, data=None)
        data = json.loads(sResponse.text)
        simdb_id = data["imdb_id"]
        sUrl = sUrl + simdb_id

    from resources.lib.multihost import cVidsrcto
    try:
        sHosterUrl = f'https://vidsrc.to/embed/movie/{simdb_id}'
        aResult = cVidsrcto().GetUrls(sHosterUrl)
        if (aResult):
            for aEntry in aResult:
                sHosterUrl = aEntry
                if '.site' in sHosterUrl:
                    oHoster = cHosterGui().getHoster('mcloud')
                else:
                    oHoster = cHosterGui().checkHoster(sHosterUrl)
                sDisplayTitle = sMovieTitle
                if oHoster != False:
                    oHoster.setDisplayName(sDisplayTitle)
                    oHoster.setFileName(sMovieTitle)
                    cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)
    except:
        oGui.addText(SITE_IDENTIFIER, '[COLOR red]فشل الاتصال بالموقع ، حاول مرة أخرى[/COLOR]')
        time.sleep(5)

    oGui.setEndOfDirectory()
