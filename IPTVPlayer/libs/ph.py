# -*- coding: utf-8 -*-

import re

from Plugins.Extensions.IPTVPlayer.libs.youtube_dl.utils import \
    clean_html as yt_clean_html
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import E2ColoR, printExc
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import (
    ensure_binary, ensure_str)

basestring = str

# flags:
NONE = 0
START_E = 1
START_S = 2
END_E = 4
END_S = 8
IGNORECASE = 16
I = 16

# pre-compiled regular expressions
IFRAME_SRC_URI_RE = re.compile(r'''<iframe[^>]+?src=(['"])([^>]*?)(?:\1)''', re.I)
IMAGE_SRC_URI_RE = re.compile(r'''<img[^>]+?src=(['"])([^>]*?\.(?:jpe?g|png)(?:\?[^\1]*?)?)(?:\1)''', re.I)
A_HREF_URI_RE = re.compile(r'''<a[^>]+?href=(['"])([^>]*?)(?:\1)''', re.I)
STRIP_HTML_COMMENT_RE = re.compile("<!--[\s\S]*?-->")

# add short aliases
IFRAME = IFRAME_SRC_URI_RE
IMG = IMAGE_SRC_URI_RE
A = A_HREF_URI_RE


def getattr(data, attrmame, flags=0):
    if flags & IGNORECASE:
        sData = data.lower()
        m = f'{attrmame.lower()}='
    else:
        sData = data
        m = f'{attrmame}='
    sidx = 0
    while True:
        sidx = sData.find(m, sidx)
        if sidx == -1:
            return ''
        if data[sidx - 1] in ('\t', ' ', '\n', '\r'):
            break
        sidx += len(m)
    sidx += len(m)
    z = data[sidx]
    if z not in ('"', "'"):
        return ''
    eidx = sidx + 1
    while eidx < len(data):
        if data[eidx] == z:
            return data[sidx + 1:eidx]
        eidx += 1
    return ''


def search(data, pattern, flags=0, limits=-1):
    tab = []
    if isinstance(pattern, basestring):
        reObj = re.compile(pattern, re.IGNORECASE if flags & IGNORECASE else 0)
    else:
        reObj = pattern
    if limits == -1:
        limits = reObj.groups
    if type(pattern) == type(data):
        match = reObj.search(data)
    elif isinstance(pattern, basestring):
        match = reObj.search(ensure_str(data))
    elif isinstance(pattern, bytes):
        match = reObj.search(ensure_binary(data))
    else:
        try:  # just blind try
            match = reObj.search(data)
        except Exception:
            printExc(f'EXCEPTION: unknown types: type(pattern)={str(type(pattern))} vs type(data)={str(type(data))}')

    for idx in range(limits):
        try:
            value = match.group(idx + 1)
        except Exception:
            value = ''
        tab.append(value)
    return tab


def all(tab, data, start, end):
    for it in tab:
        if data.find(it, start, end) == -1:
            return False
    return True


def any(tab, data, start, end):
    for it in tab:
        if data.find(it, start, end) != -1:
            return True
    return False


def none(tab, data, start, end):
    return not any(tab, data, start, end)

# example: ph.findall(data, ('<a', '>', ph.check(ph.any, ('articles.php', 'readarticle.php'))), '</a>')


def check(arg1, arg2=None):
    if arg2 == None and isinstance(arg1, basestring):
        return lambda data, ldata, s, e: ldata.find(arg1, s, e) != -1

    return lambda data, ldata, s, e: arg1(arg2, ldata, s, e)


def findall(data, start, end=('',), flags=START_E | END_E, limits=-1):
    start = start if isinstance(start, tuple) or isinstance(start, list) else (start,)
    end = end if isinstance(end, tuple) or isinstance(end, list) else (end,)

    if len(start) < 1 or len(end) < 1:
        return []

    itemsTab = []

    n1S = start[0]
    n1E = start[1] if len(start) > 1 else ''
    match1P = start[2] if len(start) > 2 else None
    match1P = check(match1P) if isinstance(match1P, basestring) else match1P

    n2S = end[0]
    n2E = end[1] if len(end) > 1 else ''
    match2P = end[2] if len(end) > 2 else None
    match2P = check(match2P) if isinstance(match2P, basestring) else match2P

    lastIdx = 0
    search = 1

    if not (flags & IGNORECASE):
        sData = data
    else:
        sData = data.lower()
        n1S = n1S.lower()
        n1E = n1E.lower()
        n2S = n2S.lower()
        n2E = n2E.lower()

    while True:
        if search == 1:
            # node 1 - start
            idx1 = sData.find(n1S, lastIdx)
            if -1 == idx1:
                return itemsTab
            lastIdx = idx1 + len(n1S)
            idx2 = sData.find(n1E, lastIdx)
            if -1 == idx2:
                return itemsTab
            lastIdx = idx2 + len(n1E)

            if match1P and not match1P(data, sData, idx1 + len(n1S), idx2):
                continue

            search = 2
        else:
            # node 2 - end
            tIdx1 = sData.find(n2S, lastIdx)
            if -1 == tIdx1:
                return itemsTab
            lastIdx = tIdx1 + len(n2S)
            tIdx2 = sData.find(n2E, lastIdx)
            if -1 == tIdx2:
                return itemsTab
            lastIdx = tIdx2 + len(n2E)

            if match2P and not match2P(data, sData, tIdx1 + len(n2S), tIdx2):
                continue

            if flags & START_S:
                itemsTab.append(data[idx1:idx2 + len(n1E)])

            idx1 = idx1 if flags & START_E else idx2 + len(n1E)
            idx2 = tIdx2 + len(n2E) if flags & END_E else tIdx1

            itemsTab.append(data[idx1:idx2])

            if flags & END_S:
                itemsTab.append(data[tIdx1:tIdx2 + len(n2E)])

            search = 1

        if limits > 0 and len(itemsTab) >= limits:
            break
    return itemsTab


def rfindall(data, start, end=('',), flags=START_E | END_E, limits=-1):
    start = start if isinstance(start, tuple) or isinstance(start, list) else (start,)
    end = end if isinstance(end, tuple) or isinstance(end, list) else (end,)

    if len(start) < 1 or len(end) < 1:
        return []

    itemsTab = []

    n1S = start[0]
    n1E = start[1] if len(start) > 1 else ''
    match1P = start[2] if len(start) > 2 else None
    match1P = check(match1P) if isinstance(match1P, basestring) else match1P

    n2S = end[0]
    n2E = end[1] if len(end) > 1 else ''
    match2P = end[2] if len(end) > 2 else None
    match2P = check(match2P) if isinstance(match2P, basestring) else match2P

    lastIdx = len(data)
    search = 1

    if not (flags & IGNORECASE):
        sData = data
    else:
        sData = data.lower()
        n1S = n1S.lower()
        n1E = n1E.lower()
        n2S = n2S.lower()
        n2E = n2E.lower()

    while True:
        if search == 1:
            # node 1 - end
            idx1 = sData.rfind(n1S, 0, lastIdx)
            if -1 == idx1:
                return itemsTab
            lastIdx = idx1
            idx2 = sData.find(n1E, idx1 + len(n1S))
            if -1 == idx2:
                return itemsTab

            if match1P and not match1P(data, sData, idx1 + len(n1S), idx2):
                continue

            search = 2
        else:
            # node 2 - start
            tIdx1 = sData.rfind(n2S, 0, lastIdx)
            if -1 == tIdx1:
                return itemsTab
            lastIdx = tIdx1
            tIdx2 = sData.find(n2E, tIdx1 + len(n2S), idx1)
            if -1 == tIdx2:
                return itemsTab

            if match2P and not match2P(data, sData, tIdx1 + len(n2S), tIdx2):
                continue

            if flags & START_S:
                itemsTab.insert(0, data[idx1:idx2 + len(n1E)])

            s1 = tIdx1 if flags & START_E else tIdx2 + len(n2E)
            s2 = idx2 + len(n1E) if flags & END_E else idx1

            itemsTab.insert(0, data[s1:s2])

            if flags & END_S:
                itemsTab.insert(0, data[tIdx1:tIdx2 + len(n2E)])

            search = 1

        if limits > 0 and len(itemsTab) >= limits:
            break
    return itemsTab


def find(data, start, end=('',), flags=START_E | END_E):
    ret = findall(data, start, end, flags, 1)
    if len(ret):
        return True, ret[0]
    else:
        return False, ''


def rfind(data, start, end=('',), flags=START_E | END_E):
    ret = rfindall(data, start, end, flags, 1)
    if len(ret):
        return True, ret[0]
    else:
        return False, ''


def getParse(sHtmlContent, sPattern, iMinFoundValue=1):
    sHtmlContent = decodeHtml(str(sHtmlContent))
    aMatches = re.compile(sPattern, re.IGNORECASE).findall(sHtmlContent)

    if (len(aMatches) >= iMinFoundValue):
        return True, aMatches
    return False, aMatches


def strip_doubles(data, pattern):
    while -1 < data.find(pattern + pattern) and '' != pattern:
        data = data.replace(pattern + pattern, pattern)
    return data


STRIP_HTML_TAGS_C = None


def clean_html(string):  # str is a keyword in python and should not be used, so changed to string
    string = ensure_str(string)
    global STRIP_HTML_TAGS_C
    if None == STRIP_HTML_TAGS_C:
        STRIP_HTML_TAGS_C = False
        try:
            from Plugins.Extensions.IPTVPlayer.libs.iptvsubparser import \
                _subparser as p
            if 'strip_html_tags' in dir(p):
                STRIP_HTML_TAGS_C = p
        except Exception:
            printExc('WARNING')

    if STRIP_HTML_TAGS_C:
        return STRIP_HTML_TAGS_C.strip_html_tags(string)

    string = string.replace('<', ' <')
    string = string.replace('&nbsp;', ' ')
    string = string.replace('&nbsp', ' ')
    string = yt_clean_html(string)
    string = string.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ')
    return strip_doubles(string, ' ').strip()


def std_title(titre, with_type=False, desc='', with_ep=False):
    info = {}
    info.clear()
    if desc == '':
        desc = {}
        desc.clear()
    titre = titre.replace('مشاهدة وتحميل مباشر', '').replace('مشاهدة', '').replace('اون لاين', '').replace('اونلاين', '').replace('كامل', '').replace('حلقات', '').replace('  ', ' ').replace('[', '').replace(']', '').replace('(', '').replace(')', '')
    tag_film = [('مسلسل الكرتون', 'CARTOON'), ('مسلسل الانمي', 'ANIM'), ('مسلسل انمي', 'ANIM'), ('مسلسل كرتون', 'CARTOON'),
                ('فيلم الكرتون', 'CARTOON FILM'), ('فلم الكرتون', 'CARTOON FILM'), ('فيلم الانمي', 'ANIM FILM'), ('فيلم كرتون', 'CARTOON FILM'),
                ('فيلم الانيميشن', 'ANIM FILM'), ('كرتون', 'CARTOON'), ('فيلم', 'FILM'), ('فلم', 'FILM'), ('مسلسل', 'SERIE'), ('عرض', 'TV SHOW'), ('انمي', 'ANIM'),
                ('برنامج', 'TV SHOW')]
    tag_type = [('مدبلج للعربية', 'مدبلج'), ('مترجمة للعربية', 'مترجم'), ('مترجم للعربية', 'مترجم'), ('مدبلجة', 'مدبلج'), ('مترجمة', 'مترجم'), ('مترجم', 'مترجم'), ('مدبلج', 'مدبلج'),
                ('باللغة العربية - كامل', 'Arabic Full'), ('باللغة العربية', 'Arabic')]

    for elm in tag_film:
        tag_type.append(elm)
    tag_qual = ['1080p', '720p', 'WEB-DL', 'BluRay', 'DVDRip', 'HDCAM', 'HDTC', 'HDRip', 'HD', '1080P', '720P', 'DVBRip', 'TVRip', 'DVD', 'SD']
    tag_season = [
        ('الموسم الثاني', '2'), ('الموسم الاول', '1'), ('الموسم الأول', '1'), ('الموسم الثالث', '3'), ('الموسم الرابع', '4'), ('الموسم الخامس', '5'), ('الموسم السادس', '6'), ('الموسم السابع', '7'), ('الموسم الثامن', '8'),
        ('الموسم التاسع', '9'), ('الموسم العاشر', '10'), ('الموسم 1', '1'), ('الموسم 2', '2'), ('الموسم 3', '3'), ('الموسم 4', '4'), ('الموسم 5', '5'), ('الموسم 6', '6'), ('الموسم 7', '7'), ('الموسم 8', '8'), ('الموسم 9', '9'), ('الموسم 10', '10'),
        ('موسم 1', '1'), ('موسم 2', '2'), ('موسم 3', '3'), ('موسم 4', '4'), ('موسم 5', '5'), ('موسم 6', '6'), ('موسم 7', '7'), ('موسم 8', '8'), ('موسم 9', '9'), ('موسم 10', '10')]
    tags_special = [('رمضان 2023', '2023'), ('رمضان 2022', '2023'), ('رمضان 2021', '2023'), ('رمضان 2020', '2023'), ('رمضان 2019', '2023'), ('رمضان 2018', '2023'), ('رمضان 2017', '2023'), ('رمضان 2016', '2023'),]
    cars = [':', '-', '_', '|']
    tag_part = []
    for elm in tag_season:
        tag_part.append(elm)
        tag_part.append((elm[0].replace('الموسم', 'الجزء'), elm[1]))
    tag_season = tag_season + [('S01', '1'),]

    # Tags
    tags_ = []
    type_ = ''
    for elm in tag_type:
        if elm[0] in titre:
            titre = titre.replace(elm[0], '').strip()
            for elm_ in tag_film:
                if elm[0] == elm_[0]:
                    type_ = elm_[1]
            tags_.append(elm[1])

    for elm in tags_special:
        if elm[0] in titre:
            titre = titre.replace(elm[0], elm[1]).strip()
            tags_.append(elm[0])

    info['type'] = type_
    info['tags'] = tags_

    for tg in desc.get('tags', []):
        info['tags'].append(tg)

    # Quality
    qual_ = []
    for elm in tag_qual:
        if elm in titre:
            titre = titre.replace(elm, '').strip()
            qual_.append(elm)
    info['qual'] = qual_

    # Season
    season = ''
    for elm in tag_season:
        if elm[0] in titre:
            season = elm[1]
            titre = titre.replace(elm[0], '').strip()
            break
    info['season'] = season

    # Part
    part = ''
    for elm in tag_part:
        if elm[0] in titre:
            part = elm[1]
            titre = titre.replace(elm[0], '').strip()
            break
    info['part'] = part

    # Year
    year = ''
    data = re.findall('(20[0-2][0-9])', titre, re.S)
    if data:
        year = data[0].strip()
        titre = titre.replace(year, '').strip()
    else:
        data = re.findall('(19[0-9]{2})', titre, re.S)
        if data:
            year = data[0].strip()
            titre = titre.replace(year, '').strip()
    if year == '':
        year = desc.get('year', '')
    info['year'] = year

    # extract episode:
    episode = ''
    titre = titre.replace('الحلقة', '_EP_').replace('حلقة', '_EP_')
    data_list = re.findall('EP_([ 0-9]{1,5})', titre.replace('الحلقة', '_EP_').replace('حلقة', '_EP_'), re.S)
    if data_list:
        episode = data_list[0]
        titre = titre.replace(episode, '').replace('_EP_', '').strip()
    if episode.strip() != '':
        desc['episode'] = episode.strip()

    # Titre en
    titre_en = ''
    titre_ = ''
    data_list = re.findall("[a-zA-Z0-9 :_\-\.&,!'’]+", titre, re.S)
    if data_list:

        for elm in data_list:
            if elm.strip() != '':
                if len(elm.strip()) > len(titre_):
                    titre_ = elm.strip()
    if len(titre_) > 2:
        titre_en = titre_

    for car in cars+cars:
        if titre_en.startswith(car):
            titre_en = titre_en[1:].strip()
        if titre_en.endswith(car):
            titre_en = titre_en[:-2].strip()
    info['title_en'] = titre_en

    if titre_en.endswith('-') or titre_en.endswith(':'):
        titre_en = titre_en[:-2].strip()

    # Titre ar
    titre_ar = ''
    titre_ = ''
    titre_ = titre.replace(titre_en, '').replace('  ', ' ').replace('  ', ' ').strip()
    if len(titre_.strip()) > 2:
        titre_ar = titre_

    for car in cars+cars:
        if titre_ar.startswith(car):
            titre_ar = titre_ar[1:].strip()
        if titre_ar.endswith(car):
            titre_ar = titre_ar[:-2].strip()
    info['title_ar'] = titre_ar

    # Titre
    if len(titre_en) < 3:
        titre = titre_ar
    else:
        titre = titre_en
    info['title'] = titre

    # display titre

    genre = desc.get('genre', '')
    rating = desc.get('rating', '')
    quality = desc.get('quality', '')
    plot = desc.get('plot', '')
    episode = desc.get('episode', '')
    info_ = desc.get('info', '')

    tag = ''
    if with_type and type_ != '':
        tag = f"{tag}{E2ColoR('lime')}{type_} - "
    if season != '':
        tag = f"{tag}{E2ColoR('yellow')}Season {season} - "
    elif part != '':
        tag = f"{tag}{E2ColoR('yellow')}Part {part} - "
    if (with_ep) and (episode != ''):
        tag = f"{tag}{E2ColoR('yellow')}E{episode} - "
    if year != '':
        tag = f"{tag}{E2ColoR('cyan')}{year} - "
    if tag.endswith(' - '):
        tag = tag[:-3]
    if tag != '':
        tag = f"{E2ColoR('white')}[{tag}{E2ColoR('white')}] "
    info['title_display'] = tag + titre.strip()

    tags = ''
    for tg in info['tags']:
        tags = f'{tags}{tg} | '

    desc = ''
    if episode:
        desc = f"{desc}\n{E2ColoR('aqua')}Episode:{E2ColoR('white')} {episode}"
    if season:
        desc = f"{desc}\n{E2ColoR('aqua')}Season:{E2ColoR('white')} {season}"
    if (rating and rating != '0'):
        desc = f"{desc}\n{E2ColoR('aqua')}Rating:{E2ColoR('white')} {rating}"
    if quality:
        desc = f"{desc}\n{E2ColoR('aqua')}Quality:{E2ColoR('white')} {quality}"
    if info_:
        desc = f"{desc}\n{E2ColoR('aqua')}Info:{E2ColoR('white')} {info_}"
    if plot:
        desc = f"{desc}\n{E2ColoR('aqua')}Plot:{E2ColoR('white')} {plot}"
    if tags:
        desc = f"{desc}\n{E2ColoR('aqua')}Tags:{E2ColoR('white')} {tags}"
    if genre:
        desc = f"{desc}\n{E2ColoR('aqua')}Genre:{E2ColoR('white')} {genre}"
    info['desc'] = desc.strip()
    return info


def std_episode(titre, cItem):
    sTitle = cItem.get('sTitle', cItem.get('info', {}).get('title', ''))
    season = cItem.get('season', cItem.get('info', {}).get('season', ''))
    part = cItem.get('part', '')
    tag = ''

    if 'اعلان' in titre:
        if season != '':
            tag = f"{tag}{E2ColoR('yellow')}S{season}{E2ColoR('white')} | {E2ColoR('aqua')}Trailer{E2ColoR('white')}"
        elif part != '':
            tag = f"{tag}{E2ColoR('yellow')}Part{part}{E2ColoR('white')} | {E2ColoR('aqua')}Trailer{E2ColoR('white')}"
        else:
            tag = f"{E2ColoR('aqua')}Trailer{E2ColoR('white')}"
    elif 'حلقة' in titre:
        episode = titre.replace('الحلقة', '').replace('والأخيرة', '').replace('والاخيرة', '').replace('الاخيرة', '').replace('الأخيرة', '').replace('حلقة', '').strip()

        if len(episode) < 4:
            if len(episode) < 2:
                episode = f'0{episode}'
            if season != '':
                tag = f'S{season}E{episode}'
            elif part != '':
                tag = f'S{part}E{episode}'
            else:
                tag = f'E{episode}'
        else:
            if (season != ''):
                tag = f'S{season} | {episode}'
            elif part != '':
                tag = f'S{part} | {episode}'
    elif ('مشاهدة' in titre) and ('تحميل' in titre):
        tag = ''
    else:
        tag = titre
    if tag != '':
        tag = f"{E2ColoR('white')}[{tag}{E2ColoR('white')}] "
    return (tag + sTitle)


def std_extract(titre):
    target = "الحلقة الأخيرة"
    sTitel = clean_html(titre)
    if target in sTitel:
        episode = target
    else:
        match = re.search(r'الحلقة\s\d+', sTitel)
        if match:
            episode = match.group()
        else:
            titre_ = sTitel.split()
            episode = " ".join(titre_[-2:])
    return episode


def extract_desc(data, Desc):
    info = {}
    for (tag, pattern) in Desc:
        desc = clean_html(search(data, pattern)[0])
        if desc.strip() != '':
            info[tag] = desc
    return info


def uniform_titre(titre, year_op=0):
    titre = titre.replace('مشاهدة وتحميل مباشر', '').replace('مشاهدة', '').replace('اون لاين', '')
    tag_type = ['مدبلج للعربية', 'مترجمة للعربية', 'مترجم للعربية', 'مدبلجة', 'مترجمة', 'مترجم', 'مدبلج', 'مسلسل', 'عرض', 'انمي', 'فيلم']
    tag_qual = ['1080p', '720p', 'WEB-DL', 'BluRay', 'DVDRip', 'HDCAM', 'HDTC', 'HDRip', 'HD', '1080P', '720P', 'DVBRip', 'TVRip', 'DVD', 'SD']
    tag_season = [
        ('الموسم الثاني', '02'), ('الموسم الاول', '01'), ('الموسم الثالث', '03'), ('الموسم الرابع', '04'), ('الموسم الخامس', '05'),
        ('الموسم السادس', '06'), ('الموسم السابع', '07'), ('الموسم الثامن', '08'), ('الموسم التاسع', '09'), ('الموسم العاشر', '10')]
    type_ = f"{E2ColoR('yellow')}Type: {E2ColoR('white')}"
    qual = f"{E2ColoR('yellow')}Quality: {E2ColoR('white')}"
    sais = f"{E2ColoR('yellow')}Season: {E2ColoR('white')}"
    desc = ''

    for elm in tag_season:
        if elm[0] in titre:
            sais = sais+elm[1]
            titre = titre.replace(elm[0], '')
            break

    for elm in tag_type:
        if elm in titre:
            titre = titre.replace(elm, '')
            type_ = f'{type_}{elm} | '
    for elm in tag_qual:
        if elm in titre:
            titre = titre.replace(elm, '')
            qual = f'{qual}{elm} | '

    data = re.findall('((?:19|20)\d{2})', titre, re.S)
    if data:
        year_ = data[-1]
        year_out = E2ColoR('aqua')+data[-1]+E2ColoR('white')
        if year_op == 0:
            titre = f"{year_out}  {titre.replace(year_, '')}"
            desc = f"{E2ColoR('yellow')}Year: {E2ColoR('white')}{year_}\n"
        elif year_op == -1:
            titre = f"{year_out}  {titre.replace(year_, '')}"
            desc = ''
        elif year_op == 1:
            titre = titre.replace(year_, '')
            desc = f"{E2ColoR('yellow')}Year: {E2ColoR('white')}{year_}\n"
        elif year_op == 2:
            titre = titre.replace(year_, '')
            desc = year_

    if year_op < 2:
        if sais != f"{E2ColoR('yellow')}Season: {E2ColoR('white')}":
            desc = f'{desc}{sais}\n'
        if type_ != f"{E2ColoR('yellow')}Type: {E2ColoR('white')}":
            desc = f'{desc}{type_[:-3]}\n'
        if qual != f"{E2ColoR('yellow')}Quality: {E2ColoR('white')}":
            desc = desc+qual[:-3]+'\n'

    pat = 'موسم.*?([0-9]{1,2}).*?حلقة.*?([0-9]{1,2})'

    data = re.findall(pat, titre, re.S)
    if data:
        sa = data[0][0]
        ep = data[0][1]
        if len(sa) == 1:
            sa = f'0{sa}'
        if len(ep) == 1:
            ep = f'0{ep}'
        ep_out = f"{E2ColoR('aqua')}S{sa}{E2ColoR('aqua')}E{ep}{E2ColoR('white')}"
        titre = f"{ep_out} {re.sub(pat, '', titre)}"
        titre = titre.replace('ال ', '')

    return desc, clean_html(titre).replace('()', '').strip()


def decodeHtml(text):
    text = text.replace('  ...  ', '')
    text = text.replace('  by', '')
    text = text.replace('-&gt;', 'and')
    text = text.replace('…  ...    …', '')
    text = text.replace('／', '/')
    text = text.replace('\/', '/')
    text = text.replace('\\x22', '"')
    text = text.replace('\u00a0', ' ')
    text = text.replace('\u00b2', '²')
    text = text.replace('\u00b3', '³')
    text = text.replace('\u00c4', 'Ä')
    text = text.replace('\u00c9', 'É')
    text = text.replace('\u00d6', 'Ö')
    text = text.replace('\u00dc', 'Ü')
    text = text.replace('\u00df', 'ß')
    text = text.replace('\u00e1', 'á')
    text = text.replace('\u00e4', 'ä')
    text = text.replace('\u00e8', 'è')
    text = text.replace('\u00e9', 'é')
    text = text.replace('\u00f3', 'ó')
    text = text.replace('\u00f6', 'ö')
    text = text.replace('\u00f8', 'ø')
    text = text.replace('\u00fc', 'ü')
    text = text.replace('\u0105', 'ą')
    text = text.replace('\u0107', 'ć')
    text = text.replace('\u0119', '_')
    text = text.replace('\u0142', 'ł')
    text = text.replace('\u0142a', '_')
    text = text.replace('\u0144', 'ń')
    text = text.replace('\u0144', 'ń')
    text = text.replace('\u015a', '_')
    text = text.replace('\u015b', '_')
    text = text.replace('\u017c', 'ż')
    text = text.replace('\u0308', '̈')
    text = text.replace('\u2013', "-")
    text = text.replace('\u2018', '‘')
    text = text.replace('\u2019', '’')
    text = text.replace('\u201c', '“')
    text = text.replace('\u201e', '„')
    text = text.replace('\u2026', '...')
    text = text.replace('\u202fh', 'h')
    text = text.replace('&#034;', '\'')
    text = text.replace('&#038;', '&')
    text = text.replace('&#039;', "'")
    text = text.replace('&#133;', '')
    text = text.replace('&#160;', ' ')
    text = text.replace('&#174;', '')
    text = text.replace('&#196;', 'Ä')
    text = text.replace('&#214;', 'Ö')
    text = text.replace('&#220;', 'Ü')
    text = text.replace('&#223;', 'ß')
    text = text.replace('&#225;', 'a')
    text = text.replace('&#228;', 'ä')
    text = text.replace('&#233;', 'e')
    text = text.replace('&#243;', 'o')
    text = text.replace('&#246;', 'ö')
    text = text.replace('&#252;', 'ü')
    text = text.replace('&#34;', "'")
    text = text.replace('&#39;', '\'')
    text = text.replace('&#4', '')
    text = text.replace('&#40;', '')
    text = text.replace('&#41;', ')')
    text = text.replace('&#58;', ':')
    text = text.replace('&#8211;', '-')
    text = text.replace('&#8216;', "'")
    text = text.replace('&#8217;', "'")
    text = text.replace('&#8220;', '“')
    text = text.replace('&#8221;', '"')
    text = text.replace('&#8222;', '„')
    text = text.replace('&#8230;', '…')
    text = text.replace('&#x27;', "'")
    text = text.replace('&aacute;', 'a')
    text = text.replace('&acute;', '\'')
    text = text.replace('&amp;', '&')
    text = text.replace('&amp;#39;', '\'')
    text = text.replace('&amp;quot;', '"')
    text = text.replace('&apos;', "'")
    text = text.replace('&atilde;', "'")
    text = text.replace('&auml;', 'ä')
    text = text.replace('&Auml;', 'Ä')
    text = text.replace('&bdquo;', '"')
    text = text.replace('&colon;', ':')
    text = text.replace('&comma;', ',')
    text = text.replace('&commmat;', ' ')
    text = text.replace('&eacute;', 'e')
    text = text.replace('&excl;', '!')
    text = text.replace('&gt;', '>')
    text = text.replace('&lbrack;', '[')
    text = text.replace('&ldquo;', '"')
    text = text.replace('&lowbar;', '_')
    text = text.replace('&lpar;', '(')
    text = text.replace('&lsquo;', '\'')
    text = text.replace('&middot;', '')
    text = text.replace('&nbsp;', '')
    text = text.replace('&ndash;', '-')
    text = text.replace('&ntilde;', 'n')
    text = text.replace('&num;', '#')
    text = text.replace('&oacute;', 'ó')
    text = text.replace('&ouml;', 'ö')
    text = text.replace('&percnt;', '%')
    text = text.replace('&period;', '.')
    text = text.replace('&plus;', '+')
    text = text.replace('&quot_', '\"')
    text = text.replace('&quot;', '\"')
    text = text.replace('&rdquo;', '"')
    text = text.replace('&rpar;', ')')
    text = text.replace('&rsqb;', ']')
    text = text.replace('&rsquo;', '\'')
    text = text.replace('&rsquo;', '\'')
    text = text.replace('&semi;', '')
    text = text.replace('&sol;', '/')
    text = text.replace('&szlig;', 'ß')
    text = text.replace('&uuml;', 'ü')
    text = text.replace('&Uuml;', 'Ü')
    text = text.replace('#038;', '')
    text = text.replace('#8217;', "'")

    return text
