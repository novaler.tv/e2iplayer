# -*- coding: utf-8 -*-

from Plugins.Extensions.IPTVPlayer.compat import urllib_urlencode
from Plugins.Extensions.IPTVPlayer.components.asynccall import \
    MainSessionWrapper
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.recaptcha_v2widget import \
    UnCaptchaReCaptchaWidget
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetCookieDir,
                                                           GetTmpDir, printDBG)


class UnCaptchaReCaptcha:
    def __init__(self, lang='en'):
        self.cm = common()
        self.HTTP_HEADER = {'Accept-Language': lang, 'Referer': 'https://www.google.com/recaptcha/api2/demo', 'User-Agent': self.cm.getDefaultUserAgent()}
        self.sessionEx = MainSessionWrapper()
        self.COOKIE_FILE = GetCookieDir('google.cookie')

    def processCaptcha(self, key, referer=None, captchaType=''):
        post_data = None
        token = ''
        iteration = 0
        if referer != None:
            self.HTTP_HEADER['Referer'] = referer
        reCaptchaUrl = f'http://www.google.com/recaptcha/api/fallback?k={key}'
        while iteration < 20:
            #,'cookiefile':self.COOKIE_FILE, 'use_cookie': True, 'load_cookie': True, 'save_cookie':True
            sts, data = self.cm.getPage(reCaptchaUrl, {'header': self.HTTP_HEADER, 'raw_post_data': True}, post_data=post_data)
            if not sts:
                SetIPTVPlayerLastHostError(_(f'Fail to get "{reCaptchaUrl}".'))
                return ''

            printDBG("+++++++++++++++++++++++++++++++++++++++++")
            printDBG(data)
            printDBG("+++++++++++++++++++++++++++++++++++++++++")
            imgUrl = ph.search(data, '"(/recaptcha/api2/payload[^"]+?)"')[0]
            iteration += 1

            message = ph.clean_html(ph.find(data, ('<div', '>', 'imageselect-desc'), '</div>', flags=0)[1])
            if not message:
                message = ph.clean_html(ph.find(data, ('<label', '>', 'fbc-imageselect-message-text'), '</label>', flags=0)[1])
            if not message:
                message = ph.clean_html(ph.find(data, ('<div', '>', 'imageselect-message'), '</div>', flags=0)[1])
            if '' == message:
                token = ph.find(data, ('<div', '>', 'verification-token'), '</div>', flags=0)[1]
                token = ph.find(data, ('<textarea', '>'), '</textarea>', flags=0)[1].strip()
                if token == '':
                    token = ph.search(data, '"this\.select\(\)">(.*?)</textarea>')[0]
                if token == '':
                    token = ph.find(data, ('<textarea', '>'), '</textarea>', flags=0)[1].strip()
                if '' != token:
                    printDBG(f'>>>>>>>> Captcha token[{token}]')
                else:
                    printDBG(f'>>>>>>>> Captcha Failed\n\n{data}\n\n')
                break

            cval = ph.search(data, 'name="c"\s+value="([^"]+)')[0]
            imgUrl = f"https://www.google.com{imgUrl.replace('&amp;', '&')}"
            message = ph.clean_html(message)
            accepLabel = ph.clean_html(ph.search(data, 'type="submit"\s+value="([^"]+)')[0])

            filePath = GetTmpDir('.iptvplayer_captcha.jpg')
            printDBG(f">>>>>>>> Captcha message[{message}]")
            printDBG(f">>>>>>>> Captcha accep label[{accepLabel}]")
            printDBG(f">>>>>>>> Captcha imgUrl[{imgUrl}] filePath[{filePath}]")

            params = {'maintype': 'image', 'subtypes': ['jpeg'], 'check_first_bytes': [b'\xFF\xD8', b'\xFF\xD9']}
            ret = self.cm.saveWebFile(filePath, imgUrl, params)
            if not ret.get('sts'):
                SetIPTVPlayerLastHostError(_(f'Fail to get "{imgUrl}".'))
                break

            retArg = self.sessionEx.waitForFinishOpen(UnCaptchaReCaptchaWidget, imgFilePath=filePath, message=message, title="reCAPTCHA v2", additionalParams={'accep_label': accepLabel})
            printDBG(f'>>>>>>>> Captcha response[{retArg}]')
            if retArg is not None and len(retArg) and retArg[0]:
                answer = retArg[0]
                printDBG(f'>>>>>>>> Captcha answer[{answer}]')
                post_data = urllib_urlencode({'c': cval, 'response': answer}, doseq=True)
            else:
                break

        return token
