# -*- coding: utf-8 -*-

import time

from Components.config import config
from Plugins.Extensions.IPTVPlayer.compat import urllib_quote
from Plugins.Extensions.IPTVPlayer.components.asynccall import \
    MainSessionWrapper
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    GetIPTVSleep
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Screens.MessageBox import MessageBox


class UnCaptchaReCaptcha:
    def __init__(self, lang='en'):
        self.cm = common()
        self.sessionEx = MainSessionWrapper()
        self.MAIN_URL = 'https://www.9kw.eu/'

    def getMainUrl(self):
        return self.MAIN_URL

    def getFullUrl(self, url, mainUrl=None):
        if mainUrl == None:
            mainUrl = self.getMainUrl()
        return self.cm.getFullUrl(url, mainUrl)

    def processCaptcha(self, sitekey, referer='', captchaType=''):
        sleepObj = None
        token = ''
        errorMsgTab = []
        apiKey = config.plugins.iptvplayer.api_key_9kweu.value
        apiUrl = f"{self.getFullUrl('/index.cgi?apikey=')}{apiKey}&action=usercaptchaupload&interactive=1&json=1&file-upload-01={sitekey}&oldsource=recaptchav2&pageurl={urllib_quote(referer)}"
        try:
            token = ''
            sts, data = self.cm.getPage(apiUrl)
            if sts:
                printDBG(f'API DATA:\n{data}\n')
                data = json_loads(data)
                if 'captchaid' in data:
                    captchaid = data['captchaid']
                    sleepObj = GetIPTVSleep()
                    sleepObj.Sleep(300, False)
                    tries = 0
                    while True:
                        tries += 1
                        timeout = sleepObj.getTimeout()
                        if tries == 1:
                            timeout = 10
                        elif timeout > 10:
                            timeout = 5
                        time.sleep(timeout)

                        apiUrl = f"{self.getFullUrl('/index.cgi?apikey=')}{apiKey}&action=usercaptchacorrectdata&json=1&id={captchaid}"
                        sts, data = self.cm.getPage(apiUrl)
                        if not sts:
                            continue
                            # maybe simple continue here ?
                            errorMsgTab.append(_('Network failed 2.'))
                            break
                        else:
                            printDBG(f'API DATA:\n{data}\n')
                            data = json_loads(data)
                            token = data['answer']
                            if token != '':
                                break
                        if sleepObj.getTimeout() == 0:
                            errorMsgTab.append(_(f'{self.getMainUrl()} timeout.'))
                            break
                else:
                    errorMsgTab.append(data['error'])
            else:
                errorMsgTab.append(_('Network failed 1.'))
        except Exception as e:
            errorMsgTab.append(str(e))
            printExc()

        if sleepObj != None:
            sleepObj.Reset()

        if token == '':
            self.sessionEx.waitForFinishOpen(MessageBox, (_(f'Resolving reCaptcha with {self.getMainUrl()} failed!\n\n')) + '\n'.join(errorMsgTab), type=MessageBox.TYPE_ERROR, timeout=10)
        return token
