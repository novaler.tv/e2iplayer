
from os.path import isfile as os_isfile
from time import sleep as _sleep

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import GetCacheSubDir

LOGINFO = 1

def getInfoLabel(txt):
    if txt == 'system.buildversion': return '18'
    else: return '0'

def getCondVisibility(txt):
    return True


def sleep(int_):
    _sleep(int_/float(1000))

def executebuiltin(int_):
    pass

class Keyboard():
    def __init__(self, sDefaultText):
        pass

    def setHeading(self,heading):
        pass

    def doModal(self):
        pass

    def isConfirmed(self):
        return True

    def getText(self):
        txt_def = ''
        tmpPath = f"{GetCacheSubDir('Tsiplayer')}searchSTR"
        if os_isfile(tmpPath):
            with open(tmpPath, 'r') as f:
                txt_def = f.read().strip()
        return txt_def