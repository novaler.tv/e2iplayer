# -*- coding: utf-8 -*-
from json import dump

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetCacheSubDir,
                                                           printDBG)

SORT_METHOD_NONE = 0
SORT_METHOD_EPISODE = 1

def addDirectoryItems(iHandler, listing, count=0):
    MyPath = f"{GetCacheSubDir('Tsiplayer')}TsiPlayer_listing.json"
    data = []
    tab = {}
    if (listing[-1][1].getItems().get('sId', '') == 'globalSearch') and (listing[-1][1].getItems().get('sFav', '') == 'DoNothing'):
        titre_search = listing[-2][1].getItems()
        for elm in listing:
            sId   = elm[1].getItems().get('sId','')
            title = elm[1].getItems().get('title','')
            if 'Aucun élément' not in title:
                name = sId.title()
                count = tab.get(sId,{}).get('count',0) + 1
                tab.update( {sId : {'name':name,'count':count}} )
        data.append(titre_search)
        id_ = 1
        for sId in tab.keys():
            if sId != 'globalSearch':
                count = str(tab[sId]['count'])
                name = tab[sId]['name']
                titre = f'[COLOR yellow]{id_}[/COLOR]. [COLOR lightcoral]{name}[/COLOR] ([COLOR violet]{count}[/COLOR] items found)'
                icon = f'special://home/addons/plugin.video.xxxx/resources/art/sites/{sId}.png'
                data.append({'title':titre,'sId':sId,'sFav':'DoNothing','icon':icon})
                for elm in listing:
                    if elm[1].getItems().get('sId','')==sId:
                        data.append(elm[1].getItems())
                id_ = id_ +1
        if id_ == 1: data.append({'title':'[COLOR redl]No information[/COLOR]','sId':'','sFav':'DoNothing','icon':''})
    else:
        for elm in listing:
            data.append(elm[1].getItems())

    with open(MyPath, "w") as f:
        dump(data, f, ensure_ascii=False)

def setPluginCategory(iHandler, txt=''):
    printDBG('setPluginCategory')

def setContent(iHandler, CONTENT):
    printDBG(f'setContent [{str(CONTENT)}]')

def addSortMethod(iHandler, SORT_METHOD):
    printDBG('addSortMethod')

def endOfDirectory(iHandler, succeeded=True, cacheToDisc=True):
    printDBG('endOfDirectory')