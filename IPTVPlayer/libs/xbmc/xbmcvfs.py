from os import listdir as os_listdir
from os.path import exists as os_exists
from os.path import isdir as os_isdir
from os.path import join as os_join

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import GetCacheSubDir


def translatePath(path):
    PathPlugin = '/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/'
    PathSpecial = 'special://home/addons/plugin.video'
    path = path.replace('special://home/userdata/addon_data/plugin.video.matrix',GetCacheSubDir('Tsiplayer'))
    path = path.replace(f'{PathSpecial}.matrix/resources/sites', f'{PathPlugin}matrix/sites')
    path = path.replace(f'{PathSpecial}.matrix/resources/extra', f'{PathPlugin}matrix/extra')
    path = path.replace('special://home/userdata/addon_data/plugin.video.vstream',GetCacheSubDir('Tsiplayer'))
    path = path.replace(f'{PathSpecial}.vstream/resources/sites', f'{PathPlugin}vstream/sites')
    path = path.replace(f'{PathSpecial}.vstream/resources/extra', f'{PathPlugin}vstream/extra')
    return path

def File(sFile,tp):
    sFile = translatePath(sFile)
    f =  open(sFile, tp)
    return f

def exists(path):
    return os_exists(translatePath(path))

def listdir(basepath):
    basepath = translatePath(basepath)
    list_dirs  = []
    list_files = []
    for fname in os_listdir(basepath):
        path = os_join(basepath, fname)
        if os_isdir(path):
            list_dirs.append(fname)
        else:
            list_files.append(fname)
    return list_dirs, list_files

