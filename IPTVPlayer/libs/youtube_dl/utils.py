#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import traceback
from html.entities import name2codepoint

from Plugins.Extensions.IPTVPlayer.compat import urllib2_Request, urlparse
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc

compat_str = str
compat_chr = chr

def compat_ord(c):
    if type(c) is int:
        return c
    else:
        return ord(c)


def preferredencoding():
    """Get preferred encoding."""
    pref = 'UTF-8'
    return pref


def compat_print(s):
    assert type(s) == type(u'')
    printDBG(s)


def htmlentity_transform(entity):
    """Transforms an HTML entity to a character."""
    # Known non-numeric HTML entity
    try:
        if entity in name2codepoint:
            return compat_chr(name2codepoint[entity])
    except Exception:
        pass

    mobj = re.match(r'#(x?[0-9A-Fa-f]+)', entity)
    if mobj is not None:
        numstr = mobj.group(1)
        if numstr.startswith(u'x'):
            base = 16
            numstr = u'0{}'.format(numstr)
        else:
            base = 10
        try:
            ret = compat_chr(int(numstr, base))
            return ret
        except Exception:
            printExc()
    # Unknown entity in name, return its literal representation
    return (u'&{};'.format(entity))


def clean_html(html):
    """Clean an HTML snippet into a readable string"""
    if type(html) == type(u''):
        strType = 'unicode'
    elif type(html) == type(''):
        strType = 'utf-8'
        html = html.decode("utf-8", 'ignore')

    # Newline vs <br />
    html = html.replace('\n', ' ')
    html = re.sub(r'\s*<\s*br\s*/?\s*>\s*', '\n', html)
    html = re.sub(r'<\s*/\s*p\s*>\s*<\s*p[^>]*>', '\n', html)
    # Strip html tags
    html = re.sub('<.*?>', '', html)
    # Replace html entities
    html = unescapeHTML(html)

    if strType == 'utf-8':
        html = html.encode("utf-8")

    return html.strip()


def unescapeHTML(s):
    if s is None:
        return None
    assert type(s) == compat_str

    return re.sub(r'&([^;]+);', lambda m: htmlentity_transform(m.group(1)), s)


class ExtractorError(Exception):
    """Error during info extraction."""

    def __init__(self, msg, tb=None):
        """ tb, if given, is the original traceback (so that it can be printed out). """
        printDBG(msg)
        super(ExtractorError, self).__init__(msg)
        self.traceback = tb
        self.exc_info = sys.exc_info()  # preserve original exception

    def format_traceback(self):
        if self.traceback is None:
            return None
        return u''.join(traceback.format_tb(self.traceback))


def url_basename(url):
    path = urlparse(url).path
    return path.strip('/').split('/')[-1]


class RegexNotFoundError(ExtractorError):
    """Error when a regex didn't match"""
    pass


class HEADRequest(urllib2_Request):
    def get_method(self):
        return 'HEAD'
