# -*- coding: utf-8 -*-
#
from Plugins.Extensions.IPTVPlayer.compat import urllib_quote

try:
    import json
except Exception:
    import simplejson as json

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_str


class SuggestionsProvider:

    def __init__(self):
        self.cm = common()

    def getName(self):
        return _("Filmweb Suggestions")

    def getSuggestions(self, text, locale):
        url = f'https://www.filmweb.pl/api/v1/live/search?query={urllib_quote(text).lower()}'
        sts, data = self.cm.getPage(url)
        if sts:
            retList = []
            data = json.loads(data)['searchHits']
            for item in data:
                if item.get('matchedName', '') == '':
                    retList.append(ensure_str(item['matchedTitle']))
                else:
                    retList.append(ensure_str(item['matchedName']))
            return retList
        return None
