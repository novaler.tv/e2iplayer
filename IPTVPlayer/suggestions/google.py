# -*- coding: utf-8 -*-
#
import json

from Plugins.Extensions.IPTVPlayer.compat import urllib_quote
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_str


class SuggestionsProvider:

    def __init__(self, forYouyube=False):
        self.cm = common()
        self.forYouyube = forYouyube

    def getName(self):
        return _("Youtube Suggestions") if self.forYouyube else _("Google Suggestions")

    def getSuggestions(self, text, locale):
        lang = locale.split('-', 1)[0]
        tmp = '&ds=yt' if self.forYouyube else ''
        url = f'http://suggestqueries.google.com/complete/search?output=firefox&hl={lang}&gl={lang}{tmp}&q={urllib_quote(text)}'
        sts, data = self.cm.getPage(url)
        if sts:
            retList = []
            for item in json.loads(data)[1]:
                retList.append(ensure_str(item))

            return retList
        return None
