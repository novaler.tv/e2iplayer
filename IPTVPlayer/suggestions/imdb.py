# -*- coding: utf-8 -*-
#
# seems not used  to DELETE import urllib
import json

from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.pCommon import common
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_str


class SuggestionsProvider:

    def __init__(self):
        self.cm = common()

    def getName(self):
        return _("IMDb Suggestions")

    def getSuggestions(self, text, locale):
        text = text.decode('ascii', 'ignore').encode('ascii').lower()
        if len(text) > 2:
            text = text.replace(' ', '_')
            url = f'http://v2.sg.media-imdb.com/suggests/titles/{text[0]}/{text}.json'
            sts, data = self.cm.getPage(url)
            if sts:
                retList = []
                data = data[data.find('(') + 1:data.rfind(')')]
                printDBG(data)
                data = json.loads(data)['d']
                for item in data:
                    retList.append(ensure_str(item['l']))
                return retList
        return None
