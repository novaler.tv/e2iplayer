# -*- coding: utf-8 -*-

import _thread as thread
from binascii import hexlify
from hashlib import md5

from Plugins.Extensions.IPTVPlayer.components.asynccall import iptv_execute
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    GetIPTVNotify
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (CreateTmpFile,
                                                           GetDukPath,
                                                           GetJSCacheDir,
                                                           ReadTextFile,
                                                           WriteTextFile,
                                                           getDebugMode,
                                                           printDBG, printExc,
                                                           rm)
from Plugins.Extensions.IPTVPlayer.tools.manipulateStrings import ensure_binary
from Tools.Directories import fileExists

DUKTAPE_VER = '260'


def duktape_execute(cmd_params):
    ret = {'sts': False, 'code': -12, 'data': ''}
    noDuk = False
    cmd = GetDukPath()
    if cmd != '':
        cmd += ' -b '  # allow bytecode inputs
        cmd += ' ' + cmd_params + ' 2> /dev/null'
        printDBG(f"duktape_execute cmd[{cmd}]")
        ret = iptv_execute()(cmd)

        if ret['code'] == 127:
            noDuk = True
    else:
        noDuk = True
        ret['code'] = 127

    if noDuk:
        messages = [_('The duktape utility is necessary here but it was not detected.')]
        messages.append(_('Please consider restart your Engima2 and agree to install the duktape utlity when the E2iPlayer will propose this.'))
        GetIPTVNotify().push('\n'.join(messages), 'error', 40, 'no_duktape', 40)

    printDBG(f'duktape_execute cmd ret[{ret}]')
    return ret


def js_execute(jscode, params={}):
    ret = {'sts': False, 'code': -12, 'data': ''}
    sts, tmpPath = CreateTmpFile('.iptv_js.js', jscode)
    if sts:
        param = f'{params.get("timeout_secf", 20)} {tmpPath}'
        ret = duktape_execute(f'-t {param} ')

    # leave last script for debug purpose
    if getDebugMode() == '':
        rm(tmpPath)

    printDBG(f'js_execute cmd ret[{ret}]')
    return ret


def js_execute_ext(items, params={}):
    fileList = []
    tmpFiles = []

    tid = thread.get_ident()
    uniqueId = 0
    ret = {'sts': False, 'code': -13, 'data': ''}
    try:
        for item in items:
            # we can have source file or source code
            path = item.get('path', '')
            code = item.get('code', '')

            name = item.get('name', '')
            if name: # cache enabled
                hash = item.get('hash', '')
                if not hash:
                    # we will need to calc hash by our self
                    if path:
                        sts, code = ReadTextFile(path)
                        if not sts:
                            raise Exception(f'Faile to read file "{path}"!')
                    hash = hexlify(md5(ensure_binary(code)).digest())
                byteFileName = GetJSCacheDir(name + '.byte')
                metaFileName = GetJSCacheDir(name + '.meta')
                if fileExists(byteFileName):
                    sts, tmp = ReadTextFile(metaFileName)
                    if sts:
                        tmp = tmp.split('|') # DUKTAPE_VER|hash
                        if DUKTAPE_VER != tmp[0] or hash != tmp[-1].strip():
                            sts = False
                    if not sts:
                        rm(byteFileName)
                        rm(metaFileName)
                else:
                    sts = False

                if not sts:
                    # we need compile here
                    if not path:
                        path = f'.{name}.js'
                        sts, path = CreateTmpFile(path, code)
                        if not sts:
                            raise Exception(f'Faile to create file "{path}" "{code}"')
                        tmpFiles.append(path)

                    # remove old meta
                    rm(metaFileName)

                    # compile
                    if 0 != duktape_execute(f'-c "{byteFileName}" "{path}" ')['code']:
                        raise Exception(f'Compile to bytecode file "{path}" > "{byteFileName}" failed!')

                    # update meta
                    if not WriteTextFile(metaFileName, f'{DUKTAPE_VER}|{hash}'):
                        raise Exception(f'Faile to write "{metaFileName}" file!')

                fileList.append(byteFileName)
            else:
                if path:
                    fileList.append(path)
                else:
                    path = f'e2i_js_exe_{uniqueId}_{tid}.js'
                    uniqueId += 1
                    sts, path = CreateTmpFile(path, code)
                    if not sts:
                        raise Exception(f'Faile to create file "{path}"')
                    tmpFiles.append(path)
                    fileList.append(path)
        ret = duktape_execute(' '.join([f'"{file}"' for file in fileList]))
    except Exception:
        printExc()

    # leave last script for debug purpose
    if getDebugMode() == '':
        for file in tmpFiles:
            rm(file)
    return ret


def is_js_cached(name, hash):
    ret = False
    byteFileName = GetJSCacheDir(name + '.byte')
    metaFileName = GetJSCacheDir(name + '.meta')
    if fileExists(byteFileName):
        sts, tmp = ReadTextFile(metaFileName)
        if sts:
            tmp = tmp.split('|')
            if DUKTAPE_VER == tmp[0] and hash == tmp[-1].strip():
                ret = True
    return ret
