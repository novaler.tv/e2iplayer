# -*- coding: utf-8 -*-
#


import codecs
from os import path as os_path

from Plugins.Extensions.IPTVPlayer.components.ihost import CHostsGroupItem
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import dumps as json_dumps
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetConfigDir,
                                                           GetHostsList,
                                                           IsHostEnabled,
                                                           printDBG, printExc)


class IPTVHostsGroups:
    def __init__(self):
        printDBG("IPTVHostsGroups.__init__")
        self.lastError = ''
        self.GROUPS_FILE = GetConfigDir('iptvplayerhostsgroups.json')

        # groups
        self.PREDEFINED_GROUPS = [
            "userdefined", "arabic", "kodi", "moviesandseries", "cartoonsandanime", "islamic", "music", "sport", "live", "documentary", "science", "polish", "english", "german", "french", "hungarian", "greek", "latino", "italian", "swedish", "balkans", "others"]
        self.PREDEFINED_GROUPS_TITLES = {
            "userdefined": "User defined",
            "moviesandseries": "Movies and series",
            "cartoonsandanime": "Cartoons and anime",
            "islamic": "Islamic",
            "music": "Music",
            "sport": "Sport",
            "live": "Live",
            "documentary": "Documentary",
            "science": "Science",
            "polish": "Polish",
            "english": "English",
            "german": "German",
            "french": "French",
            "hungarian": "Hungarian",
            "arabic": "Arabic",
            "kodi": "Kodi Addon",
            "greek": "Greek",
            "latino": "Latino",
            "italian": "Italian",
            'swedish': "Swedish",
            "balkans": "Balkans",
            "others": "Others", }

        self.LOADED_GROUPS = []
        self.LOADED_GROUPS_TITLES = {}
        self.LOADED_DISABLED_GROUPS = []

        self.CACHE_GROUPS = None

        # hosts
        self.PREDEFINED_HOSTS = {}
        self.PREDEFINED_HOSTS['userdefined'] = ['favourites', 'localmedia', 'xtreamlive', 'xtreamvod', 'youtube', 'webstream', 'dailymotion', 'vimeo', 'twitchtv', 'iptvplayerinfo']
        self.PREDEFINED_HOSTS['arabic'] = [
            'aflaam', 'akwam', 'akoam', 'alfajertv', 'arablionz', 'arabseed', 'bahyfilm', 'brstej', 'cima4u', 'cimaclub', 'cimalek', 'cimalina', 'cimanow',
            'cinemana', 'cinematy', 'coolcima', 'eceeq', 'egybest', 'egydead', 'extra3sk', 'faselhd', 'halacima', 'hwnaturkya', 'lodynet', 'moviztime', 'shahid4u',
            'taxielcima', 'telebox', 'topcinema', 'tuktukcinema', 'tvfun', 'wecima']
        self.PREDEFINED_HOSTS['kodi'] = ['matrix', 'vstream']
        self.PREDEFINED_HOSTS['moviesandseries'] = ['seezsu', 'efilmytv', 'ekinotv', 'cdapl', 'vizjerpl', 'zaluknijcc', 'filman', 'hdseanspl', 'filmowoclub', 'freediscpl', 'zerioncc'
                                                    'movienightws', 'hdpopcornscom', 'losmovies', 'kinomoc', 'dmdamedia',
                                                    'solarmovie', 'thewatchseriesto', 'classiccinemaonline', 'seriesonline', 'vumooch',
                                                    'cinemay', 'librestream', 'streamcomplet', 'skstream', 'filmstreamvkcom',
                                                    'filmpalast', 'hdstreams', 'serienstreamto', 'bsto', 'hdfilmetv', 'cineto', 'mozicsillag', 'filmezz', 'rtlmost', 'gamatocom', 'xrysoise', 'gamatotvme', 'mooviecc', 'mrpiracy',
                                                    'filmativa', 'filmovizijastudio', 'filma24hdcom', 'serijeonline', 'kinox', 'szenestreamz', 'cartoonhd', 'tantifilmorg', 'forjatn', 'serialeco', 'planetstreamingcom', 'filmeonlineto', 'tainieskaiseirestv', 'gledalica',
                                                    'obejrzyjto', 'tfarjocom', 'filmaoncom', 'streaminghdfun', 'putlockertvto', 'filmehdnet', 'andrijaiandjelka',
                                                    'altadefinizione01', '123movieshd', 'filma24io', 'ddl', 'hdfull', 'dixmax', 'fenixsite', 'kkiste']
        self.PREDEFINED_HOSTS['cartoonsandanime'] = ['9anime', 'animeblkom', 'animelek', 'animeni', 'animeodcinki', 'animeup', 'bajeczkiorg', 'cartoonrbi', 'kisscartoonme', 'otakufr', 'shahiidanimenet', 'stardima', 'watchcartoononline', 'xsanime']
        self.PREDEFINED_HOSTS['sport'] = [
            'basketballvideo', 'meczykipl', 'ekstraklasatv', 'laola1tv', 'bbcsport', 'ourmatchnet', 'hoofootcom', 'okgoals', 'ngolos', 'watchwrestlinguno', 'watchwrestling', 'fighttube', 'pinkbike', 'sportdeutschland', 'eurosportplayer', 'del', 'redbull']
        self.PREDEFINED_HOSTS['live'] = ['streamliveto', 'ustreamtv', 'eskago', 'eurosportplayer', 'ustvgo']
        self.PREDEFINED_HOSTS['documentary'] = ['asharq', 'arabsciences', 'aljazeera', 'alarabiya', 'fokustv', 'dokumentalnenet', 'greekdocumentaries3', 'orthobulletscom', 'vumedicom']
        self.PREDEFINED_HOSTS['science'] = ['questtvcouk', 'ustreamtv', 'dokumentalnenet', 'orthobulletscom', 'vumedicom']
        self.PREDEFINED_HOSTS['polish'] = [
            'efilmytv', 'ekinotv', 'cdapl', 'vizjerpl', 'zaluknijcc', 'filman', 'hdseanspl', 'obejrzyjto', 'zerioncc', 'tvpvod', 'ipla',
            'kinomoc', 'filmowoclub', 'freediscpl', 'ekstraklasatv', 'bajeczkiorg', 'animeodcinki', 'playpuls', 'meczykipl', 'eskago', 'vodpl',
            'tvjworg', 'artetv', 'kabarety', 'tvgrypl', 'chomikuj', 'fighttube', 'spryciarze', 'wgrane', 'wolnelekturypl', 'tvn24', 'ninateka',
            'maxtvgo', 'wpolscepl', 'wrealu24tv', 'wptv', 'interiatv', 'dokumentalnenet', 'serialeco', 'radiostacja', 'nuteczki', 'luxveritatis', 'tvproart', 'tvrepublika', 'christusvincit', 'joemonsterorg']
        self.PREDEFINED_HOSTS['english'] = ['bbciplayer', 'bbcsport', 'tvplayercom', 'itvcom', 'uktvplay', 'seezsu', 'classiccinemaonline', 'seriesonline',
                                            'thewatchseriesto', 'movienightws', 'artetv', 'hdpopcornscom', 'losmovies', 'solarmovie', 'putlockertvto', 'vumooch', 'cineto', 'cartoonhd', '9anime', 'kisscartoonme', 'watchcartoononline',
                                            'ourmatchnet', 'watchwrestlinguno', 'watchwrestling', 'laola1tv', 'hoofootcom', 'ted', 'ororotv', 'pinkbike', 'dancetrippin',
                                            'ustreamtv', 'rteieplayer', '3player', 'questtvcouk', 'filmeonlineto', 'playrtsiw', '123movieshd', 'orthobulletscom', 'vumedicom', 'ddl']
        self.PREDEFINED_HOSTS['german'] = [
            'ardmediathek', 'zdfmediathek', 'artetv', 'tvnowde', 'spiegeltv', 'ddl', 'serienstreamto', 'bsto', 'hdfilmetv', 'cineto', 'filmpalast', 'kinox', 'szenestreamz', 'tata', 'laola1tv', 'sportdeutschland', 'playrtsiw', 'del', 'kkiste']
        self.PREDEFINED_HOSTS['french'] = ['tfarjocom', 'skstream', 'filmstreamvkcom', 'streamcomplet', 'librestream', 'cinemay', 'otakufr', 'rtbfbe', 'artetv', 'planetstreamingcom', 'playrtsiw']
        self.PREDEFINED_HOSTS['hungarian'] = ['dmdamedia', 'mooviecc', 'filmezz', 'mozicsillag', 'rtlmost']
        self.PREDEFINED_HOSTS['greek'] = ['gamatotvme', 'xrysoise', 'tainieskaiseirestv', 'gamatocom', 'greekdocumentaries3']
        self.PREDEFINED_HOSTS['latino'] = ['mrpiracy', 'solarmovie', 'artetv', 'hdfull', 'dixmax']
        self.PREDEFINED_HOSTS['italian'] = ['cb01uno', 'mediasetplay', 'altadefinizione01', 'tantifilmorg', 'playrtsiw', 'streaminghdfun', 'raiplay']
        self.PREDEFINED_HOSTS['swedish'] = ['svtplayse']
        self.PREDEFINED_HOSTS['balkans'] = ['andrijaiandjelka', 'filmehdnet', 'gledalica', 'filmativa', 'filmovizijastudio', 'filma24hdcom', 'filma24io', 'filmaoncom', 'serijeonline', 'filmeonlineto', 'fenixsite']
        self.PREDEFINED_HOSTS['islamic'] = ['mp3quran', 'assabile']
        self.PREDEFINED_HOSTS['music'] = ['vevo', 'musicmp3ru', 'dancetrippin', 'musicbox', 'shoutcast', 'eskago', 'radiostacja', 'nuteczki', 'mediayou']

        self.PREDEFINED_HOSTS['others'] = ['iptvplayerinfo', 'localmedia', 'urllist', 'cdapl', 'wolnelekturypl', 'chomikuj', 'freediscpl', 'kabarety', 'spryciarze', 'wgrane', 'ted', 'ororotv', 'tvjworg', 'drdk', 'pinkbike', 'kijknl', 'rtbfbe', 'playrtsiw']

        self.LOADED_HOSTS = {}
        self.LOADED_DISABLED_HOSTS = {}
        self.CACHE_HOSTS = {}

        self.ADDED_HOSTS = {}

        self.hostListFromFolder = None
        self.hostListFromList = None

    def _getGroupFile(self, groupName):
        printDBG("IPTVHostsGroups._getGroupFile")
        return GetConfigDir(f"iptvplayer{groupName}group.json")

    def getLastError(self):
        return self.lastError

    def addHostToGroup(self, groupName, hostName):
        printDBG("IPTVHostsGroups.addHostToGroup")
        hostsList = self.getHostsList(groupName)
        self.ADDED_HOSTS[groupName] = []
        if hostName in hostsList or hostName in self.ADDED_HOSTS[groupName]:
            self.lastError = _('This host has been added already to this group.')
            return False
        self.ADDED_HOSTS[groupName].append(hostName)
        return True

    def flushAddedHosts(self):
        printDBG("IPTVHostsGroups.flushAddedHosts")
        for groupName in self.ADDED_HOSTS:
            if 0 == len(self.ADDED_HOSTS[groupName]):
                continue
            newList = list(self.CACHE_HOSTS[groupName])
            newList.extend(self.ADDED_HOSTS[groupName])
            self.setHostsList(groupName, newList)
        self.ADDED_HOSTS = {}

    def getGroupsWithoutHost(self, hostName):
        groupList = self.getGroupsList()
        retList = []
        for groupItem in groupList:
            hostsList = self.getHostsList(groupItem.name)
            if hostName not in hostsList and hostName not in self.ADDED_HOSTS.get(groupItem.name, []):
                retList.append(groupItem)
        return retList

    def getHostsList(self, groupName):
        printDBG("IPTVHostsGroups.getHostsList")
        if groupName in self.CACHE_HOSTS:
            return self.CACHE_HOSTS[groupName]

        if self.hostListFromFolder == None:
            self.hostListFromFolder = GetHostsList(fromList=False, fromHostFolder=True)
        if self.hostListFromList == None:
            self.hostListFromList = GetHostsList(fromList=True, fromHostFolder=False)

        groupFile = self._getGroupFile(groupName)
        self._loadHosts(groupFile, groupName, self.hostListFromFolder, self.hostListFromFolder)

        hosts = []
        for host in self.LOADED_HOSTS[groupName]:
            if IsHostEnabled(host):
                hosts.append(host)

        for host in self.PREDEFINED_HOSTS.get(groupName, []):
            if host not in hosts and host not in self.LOADED_DISABLED_HOSTS[groupName] and host in self.hostListFromFolder and IsHostEnabled(host):
                hosts.append(host)

        self.CACHE_HOSTS[groupName] = hosts
        return hosts

    def setHostsList(self, groupName, hostsList):
        printDBG(f"IPTVHostsGroups.setHostsList groupName[{groupName}], hostsList[{hostsList}]")
        # hostsList - must be updated with host which were not disabled in this group but they are not
        # available or they are disabled globally
        outObj = {"version": 0, "hosts": hostsList, "disabled_hosts": []}

        # check if some host from diabled one has been enabled
        disabledHosts = []
        for host in self.LOADED_DISABLED_HOSTS[groupName]:
            if host not in hostsList:
                disabledHosts.append(host)

        # check if some host has been disabled
        for host in self.CACHE_HOSTS[groupName]:
            if host not in hostsList and host in self.PREDEFINED_HOSTS.get(groupName, []):
                disabledHosts.append(host)

        outObj['disabled_hosts'] = disabledHosts

        self.LOADED_DISABLED_HOSTS[groupName] = disabledHosts
        self.CACHE_HOSTS[groupName] = hostsList

        groupFile = self._getGroupFile(groupName)
        return self._saveHosts(outObj, groupFile)

    def _saveHosts(self, outObj, groupFile):
        printDBG("IPTVHostsGroups._saveHosts")
        ret = True
        try:
            data = json_dumps(outObj)
            self._saveToFile(groupFile, data)
        except Exception:
            printExc()
            self.lastError = _(f"Error writing file \"{self.GROUPS_FILE}\".\n")
            ret = False
        return ret

    def _loadHosts(self, groupFile, groupName, hostListFromFolder, hostListFromList):
        printDBG(f"IPTVHostsGroups._loadHosts groupName[{groupName}]")
        predefinedHosts = self.PREDEFINED_HOSTS.get(groupName, [])
        hosts = []
        disabledHosts = []

        ret = True
        if os_path.isfile(groupFile):
            try:
                data = self._loadFromFile(groupFile)
                data = json_loads(data)
                for item in data.get('disabled_hosts', []):
                    # we need only information about predefined hosts which were disabled
                    if item in predefinedHosts and item in hostListFromList:
                        disabledHosts.append(str(item))

                for item in data.get('hosts', []):
                    if item in hostListFromFolder:
                        hosts.append(item)
            except Exception:
                printExc()

        self.LOADED_HOSTS[groupName] = hosts
        self.LOADED_DISABLED_HOSTS[groupName] = disabledHosts

    def getGroupsList(self):
        printDBG("IPTVHostsGroups.getGroupsList")
        if self.CACHE_GROUPS != None:
            return self.CACHE_GROUPS
        self._loadGroups()
        groups = list(self.LOADED_GROUPS)

        for group in self.PREDEFINED_GROUPS:
            if group not in self.LOADED_GROUPS and group not in self.LOADED_DISABLED_GROUPS:
                groups.append(group)

        groupList = []
        for group in groups:
            title = self.PREDEFINED_GROUPS_TITLES.get(group, '')
            if title == '':
                title = self.LOADED_GROUPS_TITLES.get(group, '')
            if title == '':
                title = group.title()
            item = CHostsGroupItem(group, _(title))
            groupList.append(item)
        self.CACHE_GROUPS = groupList
        return groupList

    def getPredefinedGroupsList(self):
        printDBG("IPTVHostsGroups.getPredefinedGroupsList")
        groupList = []
        for group in self.PREDEFINED_GROUPS:
            title = self.PREDEFINED_GROUPS_TITLES[group]
            item = CHostsGroupItem(group, title)
            groupList.append(item)
        return groupList

    def setGroupList(self, groupList):
        printDBG(f"IPTVHostsGroups.setGroupList groupList[{groupList}]")
        # update disabled groups
        outObj = {"version": 0, "groups": [], "disabled_groups": []}

        for group in self.PREDEFINED_GROUPS:
            if group not in groupList:
                outObj['disabled_groups'].append(group)

        for group in groupList:
            outObj['groups'].append({'name': group})
            if group in self.LOADED_GROUPS_TITLES:
                outObj['groups']['title'] = self.LOADED_GROUPS_TITLES[group]

        return self._saveGroups(outObj)

    def _saveGroups(self, outObj):
        printDBG("IPTVHostsGroups._saveGroups")
        ret = True
        try:
            data = json_dumps(outObj)
            self._saveToFile(self.GROUPS_FILE, data)
        except Exception:
            printExc()
            self.lastError = _(f"Error writing file \"{self.GROUPS_FILE}\".\n")
            ret = False
        return ret

    def _loadGroups(self):
        printDBG("IPTVHostsGroups._loadGroups")
        self.LOADED_GROUPS = []
        self.LOADED_DISABLED_GROUPS = []
        self.LOADED_GROUPS_TITLES = {}

        groups = []
        titles = {}
        disabledGroups = []

        ret = True
        if os_path.isfile(self.GROUPS_FILE):
            try:
                data = self._loadFromFile(self.GROUPS_FILE)
                data = json_loads(data)
                for item in data.get('disabled_groups', []):
                    # we need only information about predefined groups which were disabled
                    if item in self.PREDEFINED_GROUPS:
                        disabledGroups.append(str(item))

                for item in data.get('groups', []):
                    name = str(item['name'])
                    groups.append(name)
                    if 'title' in item:
                        titles[name] = str(item['title'])
            except Exception:
                printExc()

        self.LOADED_GROUPS = groups
        self.LOADED_DISABLED_GROUPS = disabledGroups
        self.LOADED_GROUPS_TITLES = titles

    def _saveToFile(self, filePath, data, encoding='utf-8'):
        printDBG(f"IPTVHostsGroups._saveToFile filePath[{filePath}]")
        with codecs.open(filePath, 'w', encoding, 'replace') as fp:
            fp.write(data)

    def _loadFromFile(self, filePath, encoding='utf-8'):
        printDBG(f"IPTVHostsGroups._loadFromFile filePath[{filePath}]")
        with codecs.open(filePath, 'r', encoding, 'replace') as fp:
            return fp.read()
