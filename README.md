# E2iPlayer for E2

E2iPlayer Enigma2 Plugin. Includes Movies, TV series, Catoons, Anime, Music, Sport, Live Streams, Documentries, Science and Content for various languages.


### ✔️ NOTE Installation

** To install run the following command in telnet 💕**

```fish
wget -qO- --no-check-certificate https://gitlab.com/MOHAMED_OS/e2iplayer/-/raw/main/install-e2iplayer.sh?inline=false -qO - | bash
```
        OR:

```fish
curl -LJso- https://gitlab.com/MOHAMED_OS/e2iplayer/-/raw/main/install-e2iplayer.sh?inline=false | bash
```
