# Language extension for distutils Python scripts. Based on this concept:
# http://wiki.maemo.org/Internationalize_a_Python_application

from os import listdir as os_listdir
from os import system as os_system
from os.path import isdir as os_isdir
from os.path import join as os_join

from setuptools import Command
from setuptools.command.build import build as _build


class build_trans(Command):
	description = 'Compile .po files into .mo files'

	def initialize_options(self):
		pass

	def finalize_options(self):
		pass

	def run(self):
		s = os_join('IPTVPlayer', 'locale')
		for lang in os_listdir(s):
			lc = os_join(s, lang, 'LC_MESSAGES')
			if os_isdir(lc):
				for f in os_listdir(lc):
					if f.endswith('.po'):
						src = os_join(lc, f)
						dest = os_join(lc, f'{f[:-2]}mo')
						print(f"Language compile {src} -> {dest}")
						if os_system(f"msgfmt '{src}' -o '{dest}'") != 0:
							raise Exception(f"Failed to compile: {src}")


class build(_build):
	sub_commands = _build.sub_commands + [('build_trans', None)]

	def run(self):
		_build.run(self)


cmdclass = {
	'build': build,
	'build_trans': build_trans,
}
